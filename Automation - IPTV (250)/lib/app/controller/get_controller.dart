// ignore_for_file: non_constant_identifier_names

import 'dart:io';
import 'package:btc_purchase/btc_purchase.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';
import '../constant/text_config.dart';

class Controller extends GetxController {
  static Controller to = Controller();

  /// first time guide
  RxBool isFirst = true.obs;

  /// APi data fetch controller
  RxBool isLoadComplete = false.obs;

  /// for handle file dialog and qrcode dialog
  RxBool isUpdateDialogFirstTime = false.obs;

  /// video guide controller
  RxBool fileVideo_Status = false.obs;
  RxBool qrcodeVideo_Status = false.obs;
  RxBool linkVideo_Status = false.obs;

  /// is video end or not controller
  RxBool isFileVideoEnd = false.obs;
  RxBool isLinkVideoEnd = false.obs;
  RxBool isQRCodeVideoEnd = false.obs;

  /// in app purchase param
  RxString purchaseKey = 'remove_ads'.obs;
  final btcPurchasePlugin = BtcPurchase();
  RxBool isPurchased = false.obs;

  /// TV channels List save in local storage
  String FileSaveName = TextConfig.FILE_SAVE_NAME;
  final Directory pathhh = Directory(
      "/storage/emulated/0/Download/${TextConfig.APP_TITLE}");

  /// video guide controller
  late VideoPlayerController fileVideoGuide;
  late VideoPlayerController linkVideoGuide;
  late VideoPlayerController qrCodeVideoGuide;

  ///download status
  RxInt fileDownloadStatus = 0.obs;
  RxInt qrCodeDownloadStatus = 0.obs;

  // dynamic name
  RxString dynamicFileName = ''.obs;
  RxString dynamicQrCodeName = ''.obs;

  // file and qrcode download status
  RxBool isFileDownloading = false.obs;
  RxBool isQRCodeDownloading = false.obs;

  // first time guide show controller
  RxBool isDrawerGuideFirstTime = false.obs;
  RxBool isRateAppGuideFirstTime = false.obs;
  RxBool isInAppPurchaseGuideFirstTime = false.obs;

  // privacy policy Controller
  RxBool privacyPolicy = false.obs;
  RxBool isFirstTimeCheck = false.obs;


  /// ads Controller.
  RxBool smallNativeAds1 = false.obs;
  RxBool smallNativeAds2 = false.obs;
  RxBool smallNativeAds3 = false.obs;
  RxBool bannerAds = false.obs;

  // inAppPurchase tap Controller
  RxBool inAppPurchaseTap = false.obs;

  // todo Intra ads Controller
  RxBool isAdsLoaded = false.obs;


}
