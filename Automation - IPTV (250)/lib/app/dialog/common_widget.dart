// ignore_for_file: non_constant_identifier_names
import 'dart:ui';
import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/constant/text_config.dart';
import 'package:automation_iptv/app/model/provider.dart';
import 'package:external_app_launcher/external_app_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import '../constant/color_config.dart';
import '../constant/font_config.dart';

Widget ButtonWithTitle(
    {required GestureTapCallback onTap,
    required Color button_color,
    required String title}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      margin: EdgeInsets.only(bottom: 10.r, right: 25.r, left: 25.r),
      padding: EdgeInsets.all(15.r),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: button_color, borderRadius: BorderRadius.circular(10.r)),
      child: Text(title,
          style: TextStyle(
              fontFamily: FontConfig.ROBOTO_REGULAR,
              fontSize: 18.sp,
              fontWeight: FontWeight.w500,
              color: ColorConfig.WHITE)),
    ),
  );
}

Widget BUTTON_RIGHT_ICON_LEFT_TITLE(
    {required GestureTapCallback onButtonTap,
    required String title,
    required String logoPath,
    required Color buttonColor}) {
  return GestureDetector(
    onTap: onButtonTap,
    child: Container(
      decoration: BoxDecoration(
          color: buttonColor, borderRadius: BorderRadius.circular(18.r)),
      child: Row(
        children: [
          const Spacer(),
          Text(title,
              style: TextStyle(
                  color: ColorConfig.WHITE,
                  fontSize: 24.sp,
                  fontFamily: FontConfig.ROBOTO_REGULAR,
                  fontWeight: FontWeight.bold)),
          const Spacer(),
          Image.asset(
            logoPath,
            height: 75.h,
          ).marginOnly(right: 10.r, top: 5.r, bottom: 5.r)
        ],
      ),
    ),
  );
}

Widget BUTTON_LEFT_ICON_RIGHT_TITLE(
    {required GestureTapCallback onButtonTap,
    required String title,
    required String logoPath,
    required Color buttonColor}) {
  return GestureDetector(
    onTap: onButtonTap,
    child: Container(
      decoration: BoxDecoration(
          color: buttonColor, borderRadius: BorderRadius.circular(18.r)),
      child: Row(
        children: [
          Image.asset(
            logoPath,
            height: 75.h,
          ).marginOnly(left: 10.r, top: 5.r, bottom: 5.r),
          const Spacer(),
          Text(title,
              style: TextStyle(
                  color: ColorConfig.WHITE,
                  fontSize: 24.sp,
                  fontFamily: FontConfig.ROBOTO_REGULAR,
                  fontWeight: FontWeight.bold)),
          const Spacer(),
        ],
      ),
    ),
  );
}

afterRewardDialog(BuildContext context,
    {required String title,
    required String smallNote,
    required Widget widget,
    required GestureTapCallback videoGuide,
    required double marginLeft}) {
  showDialog(
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        top: 22.h,
                      ),
                      margin: EdgeInsets.only(
                        top: 35.h,
                      ),
                      decoration: BoxDecoration(
                          color: ColorConfig.BACKGROUND,
                          shape: BoxShape.rectangle,
                          border: Border.all(
                              color: ColorConfig.WHITE.withOpacity(0.5),
                              width: 1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: const <BoxShadow>[
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 0.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Center(
                            child: Text(title,
                                    style: TextStyle(
                                        fontSize: 18.w,
                                        color: ColorConfig.WHITE,
                                        fontWeight: FontWeight.bold))
                                .marginOnly(top: 25.h),
                          ),
                          widget.marginOnly(
                              bottom: 20.h, top: 20.h, left: marginLeft.w),
                          Padding(
                            padding: EdgeInsets.only(
                                right: 5.w, left: 5.w, bottom: 5.h),
                            child: Text(
                              smallNote,
                              style: TextStyle(
                                  color: ColorConfig.GREY, fontSize: 14.w),
                              textAlign: TextAlign.center,
                            ),
                          ).marginOnly(bottom: 10.h),
                          ButtonWithTitle(
                            title: TextConfig.IPTV_PLAYER,
                            button_color: ColorConfig.BLUE,
                            onTap: () async {
                              await LaunchApp.openApp(
                                androidPackageName: modelApi?.iptvPlayerUrl,
                              );
                            },
                          ).marginOnly(bottom: 10.h),
                        ],
                      ),
                    ),
                    Positioned(
                      right: 0,
                      left: 0,
                      top: 0,
                      child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            padding: EdgeInsets.all(20.r),
                            decoration: BoxDecoration(
                                color: ColorConfig.GREEN,
                                shape: BoxShape.circle),
                            child: Image.asset(
                              AssetsConfig.DONE,
                              height: 30.h,
                            ),
                          )),
                    ),
                    Positioned(
                        top: 30,
                        right: 0,
                        child: GestureDetector(
                          onTap: videoGuide,
                          child: Container(
                            padding: EdgeInsets.all(5.r),
                            height: 46.1.h,
                            width: 50.h,
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(AssetsConfig.GUIDE_LOGO,
                                        height: 20.h, color: ColorConfig.GREY)
                                    .marginOnly(
                                  bottom: 2.h,
                                ),
                                Text(
                                  TextConfig.GUIDE,
                                  style: TextStyle(
                                      fontSize: 12.sp, color: ColorConfig.GREY),
                                )
                              ],
                            ),
                          ).paddingOnly(top: 15.r),
                        ))
                  ],
                ),
              )),
        );
      },
      context: context);
}

Widget GuideButton({required GestureTapCallback onTap}) {
  return GestureDetector(
    onTap: onTap,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(AssetsConfig.GUIDE_LOGO, height: 0.03.sh).marginOnly(bottom: 0.005.sh),
        Text(
          TextConfig.GUIDE,
          style: TextStyle(fontSize: 11.sp),
        )
      ],
    ),
  );
}

OpenSettingDialog(BuildContext context) {
  showDialog(
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 15.h,
                  ),
                  decoration: BoxDecoration(
                      color: ColorConfig.BACKGROUND,
                      shape: BoxShape.rectangle,
                      border: Border.all(
                          color: ColorConfig.WHITE.withOpacity(0.5), width: 1),
                      borderRadius: BorderRadius.circular(16.0),
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 0.0,
                          offset: Offset(0.0, 0.0),
                        ),
                      ]),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Center(
                        child: Text(TextConfig.PERMISSION_DENIED_1,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18.w,
                                    color: ColorConfig.WHITE,
                                    fontWeight: FontWeight.bold))
                            .marginOnly(top: 25.h),
                      ),
                      Center(
                        child: Text(TextConfig.PERMISSION_DENIED_2,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18.w,
                                    color: ColorConfig.WHITE,
                                    fontWeight: FontWeight.bold))
                            .marginOnly(top: 5.h),
                      ),

                      ButtonWithTitle(
                        title: TextConfig.APP_SETTINGS,
                        button_color: ColorConfig.BLUE,
                        onTap: () async {
                          Navigator.pop(context);
                          openAppSettings();
                        },
                      ).marginOnly(bottom: 10.h, top: 20.h),
                    ],
                  ),
                ),
              )),
        );
      },
      context: context);
}
