import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:flutter/cupertino.dart';
import '../model/provider.dart';

updateDialog(BuildContext context) {
  PPDialog.updateDialog(context,
      isUpdateRequired: modelApi!.appUpdate!.isUpdateRequire!,
      isPopup: modelApi!.appUpdate!.isPopupDialog!,
      apiVersionCode: modelApi!.appUpdate!.updatedVersionCode!,
      shuffle: modelApi!.appUpdate!.isShuffle!,
      adSettingAppVersionCode: modelApi!.adSetting!.appVersionCode!,
      clickCount: modelApi!.appUpdate!.changeDialogdataClickCount!);
}