// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

LoaderDialog(BuildContext context) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return WillPopScope(
        onWillPop: () {
          return Future.value(false);
        },
        child: Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          insetPadding: EdgeInsets.symmetric(horizontal: 0.4.sw),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Center(
                child: const CircularProgressIndicator().marginAll(15.r),
              )
            ],
          ),
        ),
      );
    },
  );
}