// import 'package:automation_iptv/app/ads/ads.dart';
// import 'package:automation_iptv/app/constant/common_widget.dart';
// import 'package:automation_iptv/app/constant/font_config.dart';
// import 'package:automation_iptv/app/constant/text_config.dart';
// import 'package:automation_iptv/app/modules/first_time_guide.dart';
// import 'package:automation_iptv/app/modules/open_channels_module.dart';
// import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:get/get.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import '../constant/assets_config.dart';
// import '../constant/color_config.dart';
// import '../controller/get_controller.dart';
//
// class StartScreen extends StatefulWidget {
//   const StartScreen({Key? key}) : super(key: key);
//
//   @override
//   State<StartScreen> createState() => _StartScreenState();
// }
//
// class _StartScreenState extends AdsWidget<StartScreen> {
//   @override
//   void initState() {
//     getPreferenceValue();
//     /*Future.delayed(Duration.zero,() {
//       modelApi?.adSetting?.appVersionCode = int.parse(AppDetailsPP.to.buildNumber.value);
//       moreLiveApp!.record.clear();
//       print('appVersionCode ---> ${modelApi!.adSetting!.appVersionCode!}');
//     },);*/
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         PPDialog.exitDialog(
//             appLogo: AssetsConfig.APP_LOGO, mediumNative: smallNativeAds1());
//         return Future.value(false);
//       },
//       child: Scaffold(
//         backgroundColor: ColorConfig.BACKGROUND,
//         appBar: AppBar(
//           backgroundColor: Colors.transparent,
//           elevation: 0,
//           centerTitle: true,
//           title: Text(
//             "${TextConfig.APP_TITLE} IPTV".tr,
//             style: TextStyle(
//                 color: ColorConfig.WHITE,
//                 fontSize: 22.sp,
//                 fontWeight: FontWeight.w500,
//                 fontFamily: FontConfig.ROBOTO_REGULAR),
//           ),
//         ),
//         body: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: [
//             const Spacer(),
//             Image.asset(AssetsConfig.START_LOGO),
//             const Spacer(),
//             ButtonWithTitle(
//                     onTap: () async {
//                       if (Controller.to.isFirst.value == true) {
//                         Get.off(const IsFirstGuide());
//                       } else {
//                         Get.off(const LinkFileQRCode());
//                       }
//                     },
//                     button_color: ColorConfig.COLOR_1,
//                     title: TextConfig.START)
//                 .marginOnly(bottom: 20.r)
//           ],
//         ),
//       ),
//     );
//   }
//
//   getPreferenceValue() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     Controller.to.isFirst.value = prefs.getBool('isFirstTime') ?? true;
//   }
// }
