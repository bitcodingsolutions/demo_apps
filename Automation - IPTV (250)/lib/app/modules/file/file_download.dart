// ignore_for_file: non_constant_identifier_names, unused_local_variable, prefer_final_fields, avoid_print, prefer_typing_uninitialized_variables

import 'dart:io';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/constant/color_config.dart';
import 'package:automation_iptv/app/dialog/common_widget.dart';
import 'package:automation_iptv/app/dialog/load_Dialog.dart';
import 'package:automation_iptv/app/dialog/reward_dialog.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import '../../constant/font_config.dart';
import '../../constant/text_config.dart';
import '../../controller/get_controller.dart';
import '../../model/provider.dart';

class FileDownload extends StatefulWidget {
  const FileDownload({Key? key}) : super(key: key);

  @override
  State<FileDownload> createState() => _FileDownloadState();
}

class _FileDownloadState extends State<FileDownload>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget smallNativeShow1 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  var link = modelApi!.extraUrl;
  String dataPath = "";

  // todo load video from assets
  loadFileVideoInit() {
    Controller.to.fileVideoGuide =
        VideoPlayerController.asset('assets/video_guide/file_guide.mp4')
          ..initialize();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    initPlatformState();
    updateBuyRemoveAdsButton();
    fetchOldPurchaseDetails();
    loadFileVideoInit();
    Future.delayed(
      Duration.zero,
      () {
        showBannerAds(
            size: AdSize.banner,
            onAdLoadedCallback: (p0) {
              setState(() {
                _bannerShow = p0;
              });
            });
        showMediumNativeAds(onAdLoadedCallback: (a01) {
          setState(() {
            smallNativeShow1 = a01;
          });
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Controller.to.fileVideoGuide.seekTo(Duration.zero);
        Controller.to.fileVideoGuide.pause();
        // ads controller
        Controller.to.isAdsLoaded.value = false;
        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: ColorConfig.BACKGROUND,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Get.back();
              Controller.to.fileVideoGuide.seekTo(Duration.zero);
              Controller.to.fileVideoGuide.pause();
              // ads controller
              Controller.to.isAdsLoaded.value = false;
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            TextConfig.FILE,
            style: TextStyle(
                color: ColorConfig.WHITE,
                fontWeight: FontWeight.bold,
                fontSize: 22.sp,
                fontFamily: FontConfig.ROBOTO_REGULAR),
          ),
          actions: [
            GuideButton(
              onTap: () {
                Controller.to.isFileVideoEnd.value = false;
                FileVideoGuide(onCloseGuide: () {
                  Get.back();
                  Controller.to.fileVideoGuide.seekTo(Duration.zero);
                  Controller.to.fileVideoGuide.pause();
                });
              },
            ).marginSymmetric(horizontal: 15.r),
            modelApi!.adsSequence!.isNotEmpty
                ? modelApi!.adSetting!.appVersionCode !=
                        int.parse(AppDetailsPP.to.buildNumber.value)
                    ? buyButton ?? Container(height: 0)
                    : Container(height: 0)
                : Container(height: 0)
          ],
        ),
        body: Column(
          children: [
            smallNativeShow1.marginSymmetric(
              horizontal: 15.r,
            ),
            const Spacer(),
            Image.asset(AssetsConfig.FILE_LOGO,
                    height: modelApi!.adSetting!.appVersionCode !=
                            int.parse(AppDetailsPP.to.buildNumber.value)
                        ? 265.h
                        : 320.h)
                .marginSymmetric(vertical: 10.r),
            const Spacer(),
            ButtonWithTitle(
                    onTap: () async {
                      Controller.to.isFileVideoEnd.value = false;
                      if (Platform.isAndroid) {
                        final androidInfo =
                            await DeviceInfoPlugin().androidInfo;
                        final sdkInt = androidInfo.version.sdkInt;
                        if (33 <= sdkInt) {
                          print('API-level above');
                          if (modelApi!.adSetting!.isFullAds == true) {
                            if(Controller.to.isAdsLoaded.value == false){
                              Controller.to.isAdsLoaded.value = true;
                              showIntraAds(callback: () {
                                Controller.to.isAdsLoaded.value = false;
                                rewardPopupFile(context);
                              });
                            }

                          } else {
                            Future.delayed(Duration.zero, () {
                              rewardPopupFile(context);
                            });
                          }
                        } else {
                          print('API-level below');
                          await Permission.storage
                              .request()
                              .then((value) async {
                            if (value.isGranted) {
                              if(modelApi!.adSetting!.isFullAds == true){
                                if(Controller.to.isAdsLoaded.value == false){
                                  Controller.to.isAdsLoaded.value = true;
                                  showIntraAds(callback: () {
                                    Controller.to.isAdsLoaded.value = false;
                                    rewardPopupFile(context);
                                  });
                                }
                              }else{
                                Future.delayed(Duration.zero, () {
                                  rewardPopupFile(context);
                                });
                              }

                            } else if (value.isPermanentlyDenied) {
                              OpenSettingDialog(context);
                            }
                          });
                        }
                      }
                    },
                    button_color: ColorConfig.COLOR_1,
                    title: TextConfig.DOWNLOAD)
                .marginOnly(bottom: 15.r),
          ],
        ),
        bottomNavigationBar: BottomAppBar(child: _bannerShow),
      ),
    );
  }

  Future downloadFile() async {
    Controller.to.pathhh.create();
    dataPath = Controller.to.pathhh.path;
    Controller.to.dynamicFileName.value =
        '${Controller.to.FileSaveName}(${DateTime.now().millisecondsSinceEpoch}).m3u';
    Dio dio = Dio();
    String filename = "$dataPath/${Controller.to.dynamicFileName.value}";
    await dio.download(link!, filename).then((value) {
      Controller.to.isFileDownloading.value = true;
      Controller.to.fileDownloadStatus.value = value.statusCode ?? 0;
    });
  }

  fileDownloaderDialog() {
    LoaderDialog(context);
    Future.delayed(
      const Duration(milliseconds: 300),
      () {
        downloadFile().then((value) async {
          if (Controller.to.isFileDownloading.value == true) {
            Get.back();
            Controller.to.isFileDownloading.value = false;
          }

          afterRewardDialog(context,
              title: TextConfig.DOWNLOAD_COMPLETE,
              widget: Image.asset(AssetsConfig.FILE_ONLY, height: 120.h),
              marginLeft: 0,
              smallNote: TextConfig.DOWNLOAD_FILE_COMPLETE_LABEL,
              videoGuide: () {
            Controller.to.isFileVideoEnd.value = false;
            FileVideoGuide(onCloseGuide: () {
              Get.back();
              Controller.to.fileVideoGuide.seekTo(Duration.zero);
              Controller.to.fileVideoGuide.pause();
            });
          });

          // todo local notification
          if (Controller.to.fileDownloadStatus.value == 200) {
            bool isAllowed =
                await AwesomeNotifications().isNotificationAllowed();
            if (!isAllowed) {
              AwesomeNotifications().requestPermissionToSendNotifications();
            } else {
              AwesomeNotifications().createNotification(
                content: NotificationContent(
                    payload: {
                      "name": "Clickable Data",
                    },
                    color: const Color(0xff046689),
                    id: 12,
                    channelKey: 'basic_file',
                    groupKey: 'basic_test',
                    title: '${TextConfig.APP_TITLE}.m3u',
                    body: 'Download complete',
                    largeIcon: 'asset://assets/app_logo.png'),
                actionButtons: [
                  NotificationActionButton(
                    key: "accept",
                    label: "Open",
                  ),
                ],
              );
            }
          }
        });
      },
    );
  }

  rewardPopupFile(BuildContext context) {
    if (modelApi!.adsSequence!.isNotEmpty) {
      if (modelApi!.adSetting!.appVersionCode !=
          int.tryParse(AppDetailsPP.to.buildNumber.value)) {
       RewardDialog.show(context,
            rewardMessage: modelApi!.rewardDialog!.rewardMessage!, onTap: () {
          Get.back();
          if(Controller.to.isAdsLoaded.value == false){
            Controller.to.isAdsLoaded.value = true;
            // ads Controller
            Future.delayed(const Duration(milliseconds: 50),() {
              Controller.to.isAdsLoaded.value = false;
            },);
            showRewardAds(callback: () {

              Controller.to.isAdsLoaded.value = false;
              Controller.to.fileVideo_Status.value == false
                  ? FileVideoGuide(
                onCloseGuide: () {
                  Get.back();
                  Controller.to.fileVideoGuide.seekTo(Duration.zero);
                  Controller.to.fileVideoGuide.pause();
                  fileDownloaderDialog();
                },
              ).then((value) async {
                Future.delayed(
                  const Duration(milliseconds: 10),
                      () {
                    Controller.to.fileVideoGuide.seekTo(Duration.zero);
                    Controller.to.fileVideoGuide.pause();
                  },
                );

                fileDownloaderDialog();
              })
                  : fileDownloaderDialog();
            }, onFailedCallback: () {
              Controller.to.isAdsLoaded.value = false;
              showIntraAds(callback: () {
                Controller.to.fileVideo_Status.value == false
                    ? FileVideoGuide(
                  onCloseGuide: () {
                    Future.delayed(
                      const Duration(milliseconds: 0),
                          () {
                        Get.back();
                        Controller.to.fileVideoGuide.seekTo(Duration.zero);
                        Controller.to.fileVideoGuide.pause();
                      },
                    );
                    fileDownloaderDialog();
                  },
                ).then((value) async {
                  Future.delayed(
                    const Duration(milliseconds: 10),
                        () {
                      Controller.to.fileVideoGuide.seekTo(Duration.zero);
                      Controller.to.fileVideoGuide.pause();
                    },
                  );

                  fileDownloaderDialog();
                })
                    : fileDownloaderDialog();
              });
            });
          }

        });
        return;
      }
    }
    Controller.to.fileVideo_Status.value == false
        ? FileVideoGuide(
            onCloseGuide: () {
              Get.back();
              Future.delayed(
                const Duration(milliseconds: 100),
                () {
                  Controller.to.fileVideoGuide.seekTo(Duration.zero);
                  Controller.to.fileVideoGuide.pause();
                },
              );
            },
          ).then((value) async {
            Future.delayed(const Duration(milliseconds: 10), () {
              Controller.to.fileVideoGuide.seekTo(Duration.zero);
              Controller.to.fileVideoGuide.pause();
            });
            fileDownloaderDialog();
          })
        : fileDownloaderDialog();
  }

  Future<void> FileVideoGuide({GestureTapCallback? onCloseGuide}) {
    Controller.to.fileVideoGuide.play();
    Controller.to.fileVideoGuide.setPlaybackSpeed(0.8);
    Controller.to.isFileVideoEnd.value = false;
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.fileVideoGuide.addListener(() async {
          SharedPreferences pref_file = await SharedPreferences.getInstance();
          Controller.to.fileVideo_Status.value =
              await pref_file.setBool('file_video_status', true);
          if (Controller.to.isFileVideoEnd.value == false) {
            if (Controller.to.fileVideoGuide.value.position >=
                Controller.to.fileVideoGuide.value.duration) {
              Controller.to.isFileVideoEnd.value = true;
              Controller.to.fileVideoGuide.seekTo(Duration.zero);
              Controller.to.fileVideoGuide.pause();
              Get.back();
            }
          }
        });

        return WillPopScope(
          onWillPop: () {
            Get.back();
            Controller.to.fileVideoGuide.seekTo(Duration.zero);
            Controller.to.fileVideoGuide.pause();
            return Future.value(false);
          },
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
            child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 0.06.sh),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.white.withOpacity(0.5), width: 1),
                          borderRadius: BorderRadius.circular(16)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: AspectRatio(
                          aspectRatio:
                              Controller.to.fileVideoGuide.value.aspectRatio,
                          child: VideoPlayer(Controller.to.fileVideoGuide),
                        ),
                      ),
                    ),
                    // close button
                    Positioned(
                      top: 0.015.sh,
                      right: 0,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: onCloseGuide,
                        child: Container(
                          padding: EdgeInsets.all(5.r),
                          height: 25.h,
                          width: 25.h,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.9)),
                              color: Colors.white.withOpacity(0.9)),
                          child: Image.asset("assets/close2.png",
                              color: Colors.black.withOpacity(0.8), height: 12),
                        ),
                      ),
                    ),
                    // guide image
                    Positioned(
                      right: 0,
                      top: 0,
                      left: 0,
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/guide_image.png",
                            height: 90.h,
                          )),
                    ),
                  ],
                )),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    Controller.to.fileVideoGuide.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () async {
          if (Controller.to.inAppPurchaseTap.value == false) {
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500), () async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset(AssetsConfig.REMOVE_ADS),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
