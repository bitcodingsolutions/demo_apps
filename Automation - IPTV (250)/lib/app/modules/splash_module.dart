// ignore_for_file: non_constant_identifier_names, prefer_typing_uninitialized_variables, deprecated_member_use

import 'dart:io';
import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/dialog/common_widget.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:dialog_pp_flutter/api_call.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:dialog_pp_flutter/model_class.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../constant/color_config.dart';
import '../constant/text_config.dart';
import '../controller/get_controller.dart';
import '../model/provider.dart';
import 'first_time_guide.dart';
import 'open_channels_module.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> implements PurchaseListener {
  var fetchPrize;
  var isAlreadyPurchased;

  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 0),
      () {
        PPDialog.internetDialogCheck(callback: () {
          // initPlatformState().then((value) {
          getPreferenceValue();
          PPDialog.autoUpdate();
          loadData();
          // });
        });
      },
    );
    super.initState();
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);

    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        purchaseStatus();
      });
    }
    if (!mounted) return;
  }

  purchaseStatus() async {
    if (Controller.to.isPurchased.value == true) {
      modelApi?.adSetting?.appVersionCode =
          int.parse(AppDetailsPP.to.buildNumber.value);
      modelApi?.adsSequence?.clear();
      moreLiveApp!.record.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            width: 1000.w,
            height: 1000.h,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(AssetsConfig.SPLASH),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Align(
            child: Image.asset(AssetsConfig.SPLASH_TEXT, height: 125.h),
          ).marginOnly(top: 0.45.sh),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Obx(() => Controller.to.isLoadComplete.value == false
                  ? Container(height: 0)
                  : Controller.to.isFirstTimeCheck.value == false ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Checkbox(
                          value: Controller.to.privacyPolicy.value,
                          onChanged: (value) async {
                            Controller.to.privacyPolicy.value = value!;
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.setBool('privacyPolicy', Controller.to.privacyPolicy.value);
                          },
                        ),
                        Text.rich(
                          TextSpan(
                            text: 'I have read and accept the ',
                            style: TextStyle(
                                fontSize: 15.sp,
                                color: Colors.black.withOpacity(0.6)),
                            children: [
                              TextSpan(
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      launch(modelApi!
                                          .privacyPolicy!.privacyPolicy!);
                                    },
                                  text: 'Privacy Policy.',
                                  style: const TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Colors.blue)),
                            ],
                          ),
                        ).marginOnly(right: 0.04.sw)
                      ],
                    ).marginOnly(bottom: 10.r) : Container(height: 0,)),
              Obx(() => Controller.to.isLoadComplete.value == false
                  ? Center(
                      child: Lottie.asset(AssetsConfig.LOADING_ANIMATION,
                              repeat: true,
                              height: 90.h,
                              alignment: Alignment.center)
                          )
                  : ButtonWithTitle(
                      onTap: () async {
                        if(Controller.to.privacyPolicy.value == true){
                          Controller.to.isFirstTimeCheck.value = true;

                          SharedPreferences prefs = await SharedPreferences.getInstance();
                          prefs.setBool('firstChecked', Controller.to.isFirstTimeCheck.value);
                          if (Controller.to.isFirst.value == true) {
                            Get.off(const IsFirstGuide(),transition: Transition.fadeIn);
                          } else {
                            Get.off(const LinkFileQRCode(),transition: Transition.fadeIn);
                          }
                        }

                      },
                      button_color: Controller.to.privacyPolicy.value == true ? ColorConfig.COLOR_1 : ColorConfig.COLOR_1.withOpacity(0.5),
                      title: TextConfig.START))
            ],
          ).marginOnly(bottom: 15.r)
        ],
      )),
    );
  }

  getPreferenceValue() async {
    PPDialog.continueDataCheck();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Controller.to.isFirst.value =
        prefs.getBool(TextConfig.IS_FIRST_TIME) ?? true;
    Controller.to.privacyPolicy.value = prefs.getBool('privacyPolicy') ?? false;
    Controller.to.isFirstTimeCheck.value = prefs.getBool('firstChecked') ?? false;
  }

  loadData() async {
    await fetchAlbum();
    await fetchAdsSettings(
            appVersionCode: int.parse(AppDetailsPP.to.buildNumber.value),
            settingsUrl: settingsUrl,
            keyName: "com.expert.iptv.afghanistan")
        .then((value) async {
      modelApi = getAdsSettings();
      await purchaseStatus();
      initOpenAds(onOpenAdLoaded: () => {showOpenAds()});
      if (value?.adsSequence?.contains("app_lovin") ?? false) {
        if ((getAdsSettings()?.appLovin?.sdkKey ?? "").isNotEmpty) {
          await AppLovinAds.instance
              .initialize(value!.appLovin!.sdkKey!)
              .then((value) async {
            await checkData();
            await dialogDataFetch();
          });
        } else {
          await checkData();
          await dialogDataFetch();
        }
      } else {
        await checkData();
        await dialogDataFetch();
      }
    });
  }

  dialogDataFetch() async {
    if (modelApi!.adSetting!.appVersionCode !=
        int.tryParse(AppDetailsPP.to.buildNumber.value)) {
      await advertisementDrawerApi(modelApi!.moreLiveApps!);
      await advertisement_UpdateAppList_Api(modelApi!.appUpdate!.appListUrl!);
    }
  }

  showDialogPop() async {
    await Future.delayed(const Duration(seconds: 1));
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (aa) {
          return WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: AlertDialog(
              elevation: 0,
              backgroundColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.h))),
              actions: [
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.h),
                          topRight: Radius.circular(15.h))),
                  height: 40.h,
                  child: Text(
                    TextConfig.AGREEMENT,
                    style: TextStyle(color: Colors.white, fontSize: 20.w),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15.h),
                          bottomRight: Radius.circular(15.h))),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      Padding(
                          padding: EdgeInsets.all(10.h),
                          child: Text(
                            TextConfig.AGREEMENT_NOTICE,
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.grey.withOpacity(1)),
                          )),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              onTap: () async {
                                Get.back();
                                SharedPreferences pref =
                                    await SharedPreferences.getInstance();
                                await pref.setBool("agreement", true);
                                Future.delayed(const Duration(seconds: 2), () {
                                  Controller.to.isLoadComplete.value = true;
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                width: 100.w,
                                padding: EdgeInsets.all(10.h),
                                decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)),
                                    color: Colors.green),
                                child: Text(
                                  "Accept".tr,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16.w),
                                ),
                              ),
                            ),
                            InkWell(
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              onTap: () {
                                exit(0);
                              },
                              child: Container(
                                alignment: Alignment.center,
                                width: 100.w,
                                padding: EdgeInsets.all(8.h),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.red),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(7))),
                                child: Text(
                                  "Cancel".tr,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 16.w),
                                ),
                              ),
                            ),
                          ]).marginSymmetric(horizontal: 15.r, vertical: 15.r),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  checkData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var value = pref.getBool("agreement");
    if (value == true) {
      Future.delayed(const Duration(seconds: 1), () {
        Controller.to.isLoadComplete.value = true;
      });
    } else {
      showDialogPop();
    }
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    purchaseStatus();
  }
}
