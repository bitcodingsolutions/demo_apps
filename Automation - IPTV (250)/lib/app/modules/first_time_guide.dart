// ignore_for_file: non_constant_identifier_names

import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/dialog/common_widget.dart';
import 'package:automation_iptv/app/constant/font_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:marquee/marquee.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constant/color_config.dart';
import '../constant/text_config.dart';
import '../controller/get_controller.dart';
import 'open_channels_module.dart';

class IsFirstGuide extends StatefulWidget {
  const IsFirstGuide({Key? key}) : super(key: key);

  @override
  State<IsFirstGuide> createState() => _IsFirstGuideState();
}

class _IsFirstGuideState extends State<IsFirstGuide> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: ColorConfig.BACKGROUND,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Align(
                    alignment: Alignment.center,
                    child: TextConfig.APP_TITLE.length < 25
                        ? Text(
                            TextConfig.APP_TITLE.tr,
                            style: TextStyle(
                                color: ColorConfig.WHITE,
                                fontSize: 22.sp,
                                fontFamily: FontConfig.ROBOTO_REGULAR),
                          )
                        : SizedBox(
                            height: 25.h,
                            child: Marquee(
                              text: "${TextConfig.APP_TITLE}".tr,
                              style: TextStyle(
                                  color: ColorConfig.WHITE,
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: FontConfig.ROBOTO_REGULAR),
                              showFadingOnlyWhenScrolling: false,
                              scrollAxis: Axis.horizontal,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              blankSpace: 20.0,
                              velocity: 50.0,
                              pauseAfterRound: const Duration(seconds: 1),
                              startPadding: 0.0,
                              accelerationDuration: const Duration(seconds: 1),
                              accelerationCurve: Curves.linear,
                              decelerationDuration:
                                  const Duration(milliseconds: 500),
                              decelerationCurve: Curves.easeOut,
                            ),
                          ))
                .marginOnly(top: 30.r, left: 15.r, right: 15.r),
            const Spacer(),
            Stack(children: [
              /// todo background mobile image
              SizedBox(
                  width: double.infinity,
                  child: Image.asset(
                    AssetsConfig.MOBILE_LOGO,
                    height: 500.h,
                    color: ColorConfig.COLOR_1.withOpacity(0.35),
                  )),

              /// todo step guide ui
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [

                  ListTileView(
                      margin:
                          EdgeInsets.only(left: 40.r, right: 40.r, top: 40.r),
                      Number: '1',
                      richtext1: 'Copy ',
                      richtext2: 'URL',
                      richtext3: ' from Link Button',
                      textColor: ColorConfig.COLOR_1),

                  ListTileView(
                      margin:
                          EdgeInsets.only(left: 50.r, right: 50.r, top: 20.r),
                      Number: '2',
                      richtext1: 'Paste ',
                      richtext2: 'URL',
                      richtext3: ' in IPTV Player',
                      textColor: ColorConfig.COLOR_2),

                  ListTileView(
                      margin:
                          EdgeInsets.only(left: 60.r, right: 60.r, top: 20.r),
                      Number: '3',
                      richtext1: 'Watch your ',
                      richtext2: 'Favourite',
                      richtext3: ' videos',
                      textColor: ColorConfig.COLOR_3),
                  Text(
                    TextConfig.GUIDE,
                    style: TextStyle(
                        color: ColorConfig.WHITE,
                        fontSize: 25.sp,
                        fontFamily: FontConfig.ROBOTO_REGULAR),
                  ).marginOnly(top: 60.h, bottom: 20.h),
                  Text(
                    TextConfig.GUIDE_LABEL,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16.sp,
                        fontFamily: FontConfig.ROBOTO_REGULAR,
                        color: ColorConfig.WHITE.withOpacity(0.5)),
                  )
                ],
              )
            ]),
            ButtonWithTitle(
                    onTap: () async {
                      Controller.to.isFirst.value = false;
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool(TextConfig.IS_FIRST_TIME,
                          Controller.to.isFirst.value);
                      Get.off(const LinkFileQRCode(),transition: Transition.fadeIn);
                    },
                    button_color: ColorConfig.COLOR_1,
                    title: TextConfig.NEXT)
                .marginOnly(bottom: 20.r)
          ],
        ),
      ),
    );
  }
}

Widget ListTileView(
    {required EdgeInsets margin,
    required String Number,
    required String richtext1,
    required String richtext2,
    required String richtext3,
    required Color textColor}) {
  return Stack(
    children: [
      Container(
          margin: margin,
          padding: EdgeInsets.all(0.r),
          decoration: BoxDecoration(
              color: ColorConfig.WHITE,
              borderRadius: BorderRadius.circular(10.r)),
          child: Row(
            children: [
              Container(
                height: 35.h,
                width: 35.h,
                margin: EdgeInsets.only(left: 5.r, right: 10.r,top: 5.r,bottom: 5.r),
                // padding: EdgeInsets.all(0.022.sh),
                alignment: Alignment.center,
                decoration:
                    BoxDecoration(color: textColor, shape: BoxShape.circle),
                child: Text(Number.tr,
                    style: TextStyle(
                        color: ColorConfig.WHITE,
                        fontSize: 15.sp,
                        fontWeight: FontWeight.bold,
                        fontFamily: FontConfig.ROBOTO_REGULAR)),
              ),
              RichText(

                text: TextSpan(
                  text: richtext1.tr,
                  style: TextStyle(
                      fontFamily: FontConfig.ROBOTO_REGULAR,
                      fontSize: 14.sp,
                      color: ColorConfig.BLACK),
                  children: <TextSpan>[
                    TextSpan(
                        text: richtext2.tr,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: FontConfig.ROBOTO_REGULAR,
                            fontSize: 14.sp,
                            color: textColor)),
                    TextSpan(
                        text: richtext3.tr,
                        style: TextStyle(
                            fontFamily: FontConfig.ROBOTO_REGULAR,
                            fontSize: 14.sp,
                            color: ColorConfig.BLACK)),
                  ],
                ),
              )
            ],
          )),
    ],
  );
}
