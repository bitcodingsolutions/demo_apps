// ignore_for_file: prefer_typing_uninitialized_variables, non_constant_identifier_names

import 'package:animate_do/animate_do.dart';
import 'package:automation_iptv/app/ads/ads.dart';
import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/constant/color_config.dart';
import 'package:automation_iptv/app/dialog/common_widget.dart';
import 'package:automation_iptv/app/dialog/load_Dialog.dart';
import 'package:automation_iptv/app/dialog/update_dialog.dart';
import 'package:automation_iptv/app/modules/file/file_download.dart';
import 'package:automation_iptv/app/modules/link/link_copy.dart';
import 'package:automation_iptv/app/modules/qr_code/qr_code_download.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:lottie/lottie.dart';
import 'package:marquee/marquee.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant/font_config.dart';
import '../constant/text_config.dart';
import '../controller/get_controller.dart';
import '../model/provider.dart';

const String feature1 = 'feature1',
    feature2 = 'feature2',
    feature3 = 'feature3';

class LinkFileQRCode extends StatefulWidget {
  const LinkFileQRCode({Key? key}) : super(key: key);

  @override
  State<LinkFileQRCode> createState() => _LinkFileQRCodeState();
}

class _LinkFileQRCodeState extends AdsWidget<LinkFileQRCode>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget smallNativeShow1 = Container(
    height: 0,
  );
  Widget smallNativeShow2 = Container(
    height: 0,
  );
  Widget smallNativeShow3 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(
      Duration.zero,
      () {
        showBannerAds(
            size: AdSize.banner,
            onAdLoadedCallback: (p0) {
              _bannerShow = p0;
              Controller.to.bannerAds.value = true;
            });
        showMediumNativeAds(
            onAdLoadedCallback: (a01) => {
                  smallNativeShow1 = a01,
                  Controller.to.smallNativeAds1.value = true
                });
        showMediumNativeAds(
            onAdLoadedCallback: (b) => {
                  smallNativeShow3 = b,
                  Controller.to.smallNativeAds3.value = true
                });
        showMediumNativeAds(
            onAdLoadedCallback: (dd) => {
                  smallNativeShow2 = dd,
                  Controller.to.smallNativeAds2.value = true
                });
      },
    );
    initPlatformState();
    updateBuyRemoveAdsButton();
    fetchOldPurchaseDetails();
    restoreSharedPreferenceValue();

    Future.delayed(
      const Duration(seconds: 0),
      () {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          FeatureDiscovery.discoverFeatures(
            context,
            const <String>{feature1, feature2, feature3},
          );
        });
      },
    );
    Future.delayed(
      const Duration(seconds: 20),
      () {
        PPDialog.autoRate();
      },
    );
    super.initState();
  }

  restoreSharedPreferenceValue() async {
    // for update Dialog
    if (Controller.to.isUpdateDialogFirstTime.value == false) {
      updateDialog(context);
      Controller.to.isUpdateDialogFirstTime.value = true;
    }

    SharedPreferences pref = await SharedPreferences.getInstance();
    Controller.to.qrcodeVideo_Status.value =
        pref.getBool('qrcode_video_status') ?? false;
    Controller.to.linkVideo_Status.value =
        pref.getBool('link_video_status') ?? false;
    Controller.to.fileVideo_Status.value =
        pref.getBool('file_video_status') ?? false;
    Controller.to.isRateAppGuideFirstTime.value =
        pref.getBool('isFirstRate') ?? false;
    Controller.to.isInAppPurchaseGuideFirstTime.value =
        pref.getBool('isFirstInApp') ?? false;
    Controller.to.isDrawerGuideFirstTime.value =
        pref.getBool('isFirstDrawer') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: () {
        // ads controller
        Controller.to.isAdsLoaded.value = false;
        if (scaffoldKey.currentState!.isDrawerOpen) {
          scaffoldKey.currentState!.closeDrawer();
        } else {
          if (Controller.to.isDrawerGuideFirstTime.value == true &&
              Controller.to.isRateAppGuideFirstTime.value == true &&
              Controller.to.isInAppPurchaseGuideFirstTime.value == true) {
            PPDialog.exitDialog(
                appLogo: AssetsConfig.APP_LOGO,
                mediumNative: Obx(() =>
                    Controller.to.smallNativeAds1.value == false
                        ? Container(height: 0)
                        : smallNativeShow1));
          } else {
            if (Controller.to.isDrawerGuideFirstTime.value == true &&
                Controller.to.isRateAppGuideFirstTime.value == true) {
              if (modelApi!.adsSequence!.isEmpty ||
                  modelApi!.adSetting!.appVersionCode ==
                      int.parse(AppDetailsPP.to.buildNumber.value)) {
                PPDialog.exitDialog(
                    appLogo: AssetsConfig.APP_LOGO,
                    mediumNative: Obx(() =>
                        Controller.to.smallNativeAds1.value == false
                            ? Container(height: 0)
                            : smallNativeShow1));
              }
            }
          }
        }
        return Future.value(false);
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: ColorConfig.BACKGROUND,
        drawer: PPDialog.drawerScreen(
            appLogo: AssetsConfig.APP_LOGO,
            privacyPolicy: '${modelApi!.privacyPolicy!.privacyPolicy}',
            feedbackEmailId: '${modelApi!.feedbackSupport!.emailId}',
            emailSubject: '${modelApi!.feedbackSupport!.emailSubject}',
            emailMessage: '${modelApi!.feedbackSupport!.emailMessage}',
            apiAppVersionCode: modelApi!.adSetting!.appVersionCode!),
        appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            titleSpacing: 0,
            leading: DescribedFeatureOverlay(
              featureId: Controller.to.isDrawerGuideFirstTime.value == false
                  ? feature1
                  : '',
              tapTarget: const Icon(Icons.menu),
              backgroundColor: ColorConfig.COLOR_1.withGreen(0xff000000),
              title: const Text('Drawer'),
              description: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Text('\n- Privacy Policy'),
                  Text('- Feedback support'),
                  Text('- About the App'),
                  Text('- More apps'),
                ],
              ),
              onComplete: () async {
                Controller.to.isDrawerGuideFirstTime.value = true;
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setBool('isFirstDrawer',
                    Controller.to.isDrawerGuideFirstTime.value);
                return Future.value(true);
              },
              barrierDismissible: false,
              child: GestureDetector(
                onTap: () {
                  scaffoldKey.currentState?.openDrawer();
                },
                child: const Icon(Icons.menu),
              ),
            ),
            title: SizedBox(
              height: 25.h,
              child: TextConfig.APP_TITLE.length < 15
                  ? Center(
                      child: Text(TextConfig.APP_TITLE.tr,
                          style: TextStyle(
                              color: ColorConfig.WHITE,
                              fontSize: 22.sp,
                              fontWeight: FontWeight.w500,
                              fontFamily: FontConfig.ROBOTO_REGULAR)),
                    )
                  : Marquee(
                      text: TextConfig.APP_TITLE.tr,
                      style: TextStyle(
                          color: ColorConfig.WHITE,
                          fontSize: 22.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: FontConfig.ROBOTO_REGULAR),
                      showFadingOnlyWhenScrolling: false,
                      scrollAxis: Axis.horizontal,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      blankSpace: 20.0,
                      velocity: 50.0,
                      pauseAfterRound: const Duration(seconds: 1),
                      startPadding: 0.0,
                      accelerationDuration: const Duration(seconds: 1),
                      accelerationCurve: Curves.linear,
                      decelerationDuration: const Duration(milliseconds: 500),
                      decelerationCurve: Curves.easeOut,
                    ),
            ),
            actions: [
              DescribedFeatureOverlay(
                featureId: Controller.to.isRateAppGuideFirstTime.value == false
                    ? feature2
                    : '',
                tapTarget: Lottie.asset(
                  "assets/animation/star_animation.json",
                ),
                backgroundColor: ColorConfig.COLOR_2.withRed(0xff001B01),
                title: const Text('Rate App'),
                description: const Text('\n- Rate our app.'),
                onComplete: () async {
                  Controller.to.isRateAppGuideFirstTime.value = true;
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.setBool('isFirstRate',
                      Controller.to.isRateAppGuideFirstTime.value);

                  return true;
                },
                barrierDismissible: false,
                child: InkWell(
                  onTap: () {
                    PPDialog.rateDialogCustom(context,
                        logoApp: AssetsConfig.APP_LOGO);
                  },
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Lottie.asset(
                    "assets/animation/star_animation.json",
                  ),
                ),
              ),
              modelApi!.adsSequence!.isNotEmpty
                  ? modelApi!.adSetting!.appVersionCode !=
                          int.parse(AppDetailsPP.to.buildNumber.value)
                      ? DescribedFeatureOverlay(
                          featureId: Controller
                                      .to.isInAppPurchaseGuideFirstTime.value ==
                                  false
                              ? feature3
                              : '',
                          tapTarget: Swing(
                            infinite: true,
                            child: Image.asset(
                              'assets/remove_ads.png',
                              height: 30.h,
                            ),
                          ),
                          backgroundColor:
                              ColorConfig.COLOR_3.withBlue(0xff001B01),

                          title: const Text('Remove Ads'),
                          // enablePulsingAnimation: true,
                          // overflowMode: OverflowMode.wrapBackground,
                          description: const Text('\n- Remove ads from app'),
                          onComplete: () async {
                            Controller.to.isInAppPurchaseGuideFirstTime.value =
                                true;
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setBool('isFirstInApp',
                                Controller.to.isRateAppGuideFirstTime.value);

                            return true;
                          },
                          barrierDismissible: false,
                          child: buyButton ?? Container(height: 0),
                        )
                      : SizedBox(height: 0.h, width: 0.w)
                  : SizedBox(height: 0.h, width: 0.w),
            ]),
        body: Column(
            mainAxisAlignment: modelApi!.adSetting!.appVersionCode !=
                    int.parse(AppDetailsPP.to.buildNumber.value)
                ? MainAxisAlignment.center
                : MainAxisAlignment.spaceEvenly,
            children: [
              Obx(() => Container(
                    child: Controller.to.smallNativeAds2.value == false
                        ? Container(
                            height: 0,
                          )
                        : smallNativeShow2,
                  )).marginSymmetric(
                horizontal: 15.r,
              ),
              const Spacer(),
              BUTTON_RIGHT_ICON_LEFT_TITLE(
                      onButtonTap: () {
                        if (Controller.to.isAdsLoaded.value == false) {
                          Controller.to.isAdsLoaded.value = true;
                          showIntraAds(callback: () {
                            Controller.to.isAdsLoaded.value = false;
                            Get.to(const LinkCopy(),
                                transition: Transition.fadeIn)!.then((value) {
                              Controller.to.isAdsLoaded.value = false;
                            });
                          });
                        }
                      },
                      title: TextConfig.LINK,
                      logoPath: AssetsConfig.LINK_GROUP,
                      buttonColor: ColorConfig.COLOR_1)
                  .marginOnly(
                      left: 30.r,
                      right: 30.r,
                      top: 15.r,
                      bottom: modelApi!.adSetting!.appVersionCode !=
                              int.parse(AppDetailsPP.to.buildNumber.value)
                          ? 10.r
                          : 25.r),
              BUTTON_LEFT_ICON_RIGHT_TITLE(
                      onButtonTap: () {
                        if (Controller.to.isAdsLoaded.value == false) {
                          Controller.to.isAdsLoaded.value = true;
                          showIntraAds(callback: () {
                            Controller.to.isAdsLoaded.value = false;
                            Get.to(const FileDownload(),
                                transition: Transition.fadeIn)!.then((value) {
                              Controller.to.isAdsLoaded.value = false;
                            });
                          });
                        }
                      },
                      title: TextConfig.FILE,
                      logoPath: AssetsConfig.FILE_GROUP,
                      buttonColor: ColorConfig.COLOR_2)
                  .marginOnly(
                      left: 30.r,
                      right: 30.r,
                      bottom: modelApi!.adSetting!.appVersionCode !=
                              int.parse(AppDetailsPP.to.buildNumber.value)
                          ? 10.r
                          : 25.r),
              BUTTON_RIGHT_ICON_LEFT_TITLE(
                      onButtonTap: () {
                        if (Controller.to.isAdsLoaded.value == false) {
                          Controller.to.isAdsLoaded.value = true;
                          showIntraAds(callback: () {
                            Controller.to.isAdsLoaded.value = false;
                            Get.to(const QRCodeDownload(),
                                transition: Transition.fadeIn)!.then((value)  {
                            Controller.to.isAdsLoaded.value = false;
                            });
                          });
                        }
                      },
                      title: TextConfig.QRCODE,
                      logoPath: AssetsConfig.QRCODE_GROUP,
                      buttonColor: ColorConfig.COLOR_3)
                  .marginOnly(left: 30.r, right: 30.r, bottom: 15.r),
              const Spacer(),
            ]),
        bottomNavigationBar: modelApi!.adSetting!.isFullAds == false
            ? Obx(() => Controller.to.bannerAds.value == false
                ? Container(height: 0)
                : BottomAppBar(child: _bannerShow))
            : Obx(() => Controller.to.smallNativeAds3.value == false
                ? Container(height: 0)
                : Container(
                    child: smallNativeShow3,
                  )).marginOnly(left: 15.r, right: 15.r, bottom: 6.9.r),
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () {
          if (Controller.to.inAppPurchaseTap.value == false) {
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500), () async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset(AssetsConfig.REMOVE_ADS),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
