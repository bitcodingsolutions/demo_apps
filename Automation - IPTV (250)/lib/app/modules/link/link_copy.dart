// ignore_for_file: prefer_typing_uninitialized_variables, non_constant_identifier_names

import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:automation_iptv/app/constant/assets_config.dart';
import 'package:automation_iptv/app/dialog/reward_dialog.dart';
import 'package:automation_iptv/app/model/provider.dart';
import 'package:automation_iptv/app/modules/open_channels_module.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:clipboard/clipboard.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import '../../constant/color_config.dart';
import '../../controller/get_controller.dart';
import '../../dialog/common_widget.dart';
import '../../constant/font_config.dart';
import '../../constant/text_config.dart';

class LinkCopy extends StatefulWidget {
  const LinkCopy({Key? key}) : super(key: key);

  @override
  State<LinkCopy> createState() => _LinkCopyState();
}

class _LinkCopyState extends State<LinkCopy>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget smallNativeShow1 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  var link = modelApi!.extraUrl;

  // todo load video from assets
  loadLinkVideoInit() {
    Controller.to.linkVideoGuide =
        VideoPlayerController.asset('assets/video_guide/link_guide.mp4')
          ..initialize();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    initPlatformState();
    updateBuyRemoveAdsButton();
    fetchOldPurchaseDetails();
    loadLinkVideoInit();
    Future.delayed(
      Duration.zero,
      () {
        showBannerAds(
            size: AdSize.banner,
            onAdLoadedCallback: (p0) {
              setState(() {
                _bannerShow = p0;
              });
            });
        showMediumNativeAds(onAdLoadedCallback: (a01) {
          setState(() {
            smallNativeShow1 = a01;
          });
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Controller.to.linkVideoGuide.seekTo(Duration.zero);
        Controller.to.linkVideoGuide.pause();
        // ads controller
        Controller.to.isAdsLoaded.value = false;

        return Future.value(true);
      },
      child: Scaffold(
        backgroundColor: ColorConfig.BACKGROUND,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
          leading: GestureDetector(
            onTap: () {
              Get.back();
              Controller.to.linkVideoGuide.seekTo(Duration.zero);
              Controller.to.linkVideoGuide.pause();
              // ads controller
              Controller.to.isAdsLoaded.value = false;
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            TextConfig.LINK,
            style: TextStyle(
                color: ColorConfig.WHITE,
                fontWeight: FontWeight.bold,
                fontSize: 22.sp,
                fontFamily: FontConfig.ROBOTO_REGULAR),
          ),
          actions: [
            GuideButton(
              onTap: () {
                Controller.to.isLinkVideoEnd.value = false;
                linkVideoGuide(onCloseCallback: () {
                  Get.back();
                  Controller.to.linkVideoGuide.seekTo(Duration.zero);
                  Controller.to.linkVideoGuide.pause();
                });
              },
            ).marginSymmetric(horizontal: 15.r),
            modelApi!.adsSequence!.isNotEmpty
                ? modelApi!.adSetting!.appVersionCode !=
                        int.parse(AppDetailsPP.to.buildNumber.value)
                    ? buyButton ?? Container(height: 0)
                    : Container(height: 0)
                : Container(height: 0)
          ],
        ),
        body: Column(
          children: [
            smallNativeShow1.marginSymmetric(
              horizontal: 15.r,
            ),
            const Spacer(),
            Image.asset(AssetsConfig.LINK_LOGO,
                    height: modelApi!.adSetting!.appVersionCode !=
                            int.parse(AppDetailsPP.to.buildNumber.value)
                        ? 250.h
                        : 300.h)
                .marginSymmetric(vertical: 10.r, horizontal: 10.r),
            const Spacer(),
            ButtonWithTitle(
                    onTap: () {
                      Controller.to.isLinkVideoEnd.value = false;
                      if (modelApi!.adSetting!.isFullAds == true) {
                        if (Controller.to.isAdsLoaded.value == false) {
                          Controller.to.isAdsLoaded.value = true;
                          showIntraAds(callback: () {
                            Controller.to.isAdsLoaded.value = false;
                            rewardPopupLink(context);
                          });
                        }
                      } else {
                        rewardPopupLink(context);
                      }
                    },
                    button_color: ColorConfig.COLOR_1,
                    title: TextConfig.GET_LINK)
                .marginOnly(bottom: 15.r),
          ],
        ),
        bottomNavigationBar: BottomAppBar(child: _bannerShow),
      ),
    );
  }

  rewardPopupLink(BuildContext context) {
    if (modelApi!.adsSequence!.isNotEmpty) {
      if (modelApi!.adSetting!.appVersionCode !=
          int.tryParse(AppDetailsPP.to.buildNumber.value)) {
        RewardDialog.show(context,
            rewardMessage: modelApi!.rewardDialog!.rewardMessage!, onTap: () {
          Navigator.pop(context);
          if (Controller.to.isAdsLoaded.value == false) {
            Controller.to.isAdsLoaded.value = true;
            // ads Controller
            Future.delayed(const Duration(milliseconds: 50),() {
              Controller.to.isAdsLoaded.value = false;
            },);
            showRewardAds(onFailedCallback: () {
              Controller.to.isAdsLoaded.value = false;
              showIntraAds(callback: () {
                Controller.to.linkVideo_Status.value == false
                    ? linkVideoGuide(onCloseCallback: () {
                        Get.back();
                        Future.delayed(const Duration(milliseconds: 10), () {
                          Controller.to.linkVideoGuide.seekTo(Duration.zero);
                          Controller.to.linkVideoGuide.pause();
                        });
                      }).then((value) async {
                        Future.delayed(const Duration(milliseconds: 10), () {
                          Controller.to.linkVideoGuide.seekTo(Duration.zero);
                          Controller.to.linkVideoGuide.pause();
                        });
                        forLinkCopy();
                      })
                    : forLinkCopy();
              });
            }, callback: () {
              Controller.to.isAdsLoaded.value = false;
              Controller.to.linkVideo_Status.value == false
                  ? linkVideoGuide(onCloseCallback: () {
                      Get.back();
                      Future.delayed(const Duration(milliseconds: 10), () {
                        Controller.to.linkVideoGuide.seekTo(Duration.zero);
                        Controller.to.linkVideoGuide.pause();
                      });
                    }).then((value) async {
                      Future.delayed(const Duration(milliseconds: 10), () {
                        Controller.to.linkVideoGuide.seekTo(Duration.zero);
                        Controller.to.linkVideoGuide.pause();
                      });
                      forLinkCopy();
                    })
                  : forLinkCopy();
            });
          }
        });
        return;
      }
    }
    Controller.to.linkVideo_Status.value == false
        ? linkVideoGuide(onCloseCallback: () {
            Get.back();
            Future.delayed(const Duration(milliseconds: 10), () {
              Controller.to.linkVideoGuide.seekTo(Duration.zero);
              Controller.to.linkVideoGuide.pause();
            });
          }).then((value) async {
            Future.delayed(const Duration(milliseconds: 10), () {
              Controller.to.linkVideoGuide.seekTo(Duration.zero);
              Controller.to.linkVideoGuide.pause();
            });
            forLinkCopy();
          })
        : forLinkCopy();
  }

  forLinkCopy() {
    FlutterClipboard.copy(link!).then((value) {
      afterRewardDialog(context,
          title: TextConfig.COPY_COMPLETE,
          widget: Image.asset(
            AssetsConfig.LINK_ONLY,
            height: 120.h,
          ),
          marginLeft: 40,
          smallNote: TextConfig.COPY_COMPLETE_LABEL, videoGuide: () {
        Controller.to.isLinkVideoEnd.value = false;
        linkVideoGuide(onCloseCallback: () {
          Get.back();
          Controller.to.linkVideoGuide.seekTo(Duration.zero);
          Controller.to.linkVideoGuide.pause();
        });
      });
    });
  }

  Future<void> linkVideoGuide({GestureTapCallback? onCloseCallback}) {
    Controller.to.linkVideoGuide.play();
    Controller.to.linkVideoGuide.setPlaybackSpeed(0.8);
    Controller.to.isLinkVideoEnd.value = false;
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.linkVideoGuide.addListener(() async {
          SharedPreferences prefLink = await SharedPreferences.getInstance();
          Controller.to.linkVideo_Status.value =
              await prefLink.setBool('link_video_status', true);
          if (Controller.to.isLinkVideoEnd.value == false) {
            if (Controller.to.linkVideoGuide.value.position >=
                Controller.to.linkVideoGuide.value.duration) {
              Controller.to.isLinkVideoEnd.value = true;

              Get.back();
              Controller.to.linkVideoGuide.seekTo(Duration.zero);
              Controller.to.linkVideoGuide.pause();
            }
          }
        });

        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
          child: WillPopScope(
            onWillPop: () {
              Get.back();
              Controller.to.linkVideoGuide.seekTo(Duration.zero);
              Controller.to.linkVideoGuide.pause();
              return Future.value(false);
            },
            child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 0.06.sh),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.white.withOpacity(0.5), width: 1),
                          borderRadius: BorderRadius.circular(16.0)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16.0),
                        child: AspectRatio(
                          aspectRatio:
                              Controller.to.linkVideoGuide.value.aspectRatio,
                          child: VideoPlayer(Controller.to.linkVideoGuide),
                        ),
                      ),
                    ),
                    // close button
                    Positioned(
                      top: 0.015.sh,
                      right: 0,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: onCloseCallback,
                        child: Container(
                          padding: EdgeInsets.all(5.r),
                          height: 25.h,
                          width: 25.h,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.9)),
                              color: Colors.white.withOpacity(0.9)),
                          child: Image.asset("assets/close2.png",
                              color: Colors.black.withOpacity(0.8), height: 12),
                        ),
                      ),
                    ),
                    // guide image
                    Positioned(
                      right: 0,
                      top: 0,
                      left: 0,
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/guide_image.png",
                            height: 90.h,
                          )),
                    ),
                  ],
                )),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    Controller.to.linkVideoGuide.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () async {
          if (Controller.to.inAppPurchaseTap.value == false) {
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500), () async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset(AssetsConfig.REMOVE_ADS),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
