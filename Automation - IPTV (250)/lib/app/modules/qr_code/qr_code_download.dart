// ignore_for_file: non_constant_identifier_names, deprecated_member_use, avoid_print, prefer_typing_uninitialized_variables

import 'dart:io';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:automation_iptv/app/dialog/load_Dialog.dart';
import 'package:automation_iptv/app/dialog/reward_dialog.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:restart_app/restart_app.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import '../../constant/assets_config.dart';
import '../../constant/color_config.dart';
import '../../controller/get_controller.dart';
import '../../dialog/common_widget.dart';
import '../../constant/font_config.dart';
import '../../constant/text_config.dart';
import '../../model/provider.dart';
import '../open_channels_module.dart';

class QRCodeDownload extends StatefulWidget {
  const QRCodeDownload({Key? key}) : super(key: key);

  @override
  State<QRCodeDownload> createState() => _QRCodeDownloadState();
}

class _QRCodeDownloadState extends State<QRCodeDownload>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget smallNativeShow1 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  GlobalKey globalKey = GlobalKey();
  ScreenshotController screenshotController = ScreenshotController();
  var link = modelApi!.extraUrl;
  String dataPath = "";

  loadQrCodeVideoInit() {
    Controller.to.qrCodeVideoGuide =
        VideoPlayerController.asset('assets/video_guide/qr_code_guide.mp4')
          ..initialize();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    initPlatformState();
    updateBuyRemoveAdsButton();
    fetchOldPurchaseDetails();
    loadQrCodeVideoInit();
    Future.delayed(
      Duration.zero,
      () {
        showBannerAds(
            size: AdSize.banner,
            onAdLoadedCallback: (p0) {
              setState(() {
                _bannerShow = p0;
              });
            });
        showMediumNativeAds(onAdLoadedCallback: (a01) {
          setState(() {
            smallNativeShow1 = a01;
          });
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
        Controller.to.qrCodeVideoGuide.pause();
        // ads controller
        Controller.to.isAdsLoaded.value = false;
        return Future.value(true);
      },
      child: Stack(
        children: [
          Center(
            child: RepaintBoundary(
              key: globalKey,
              child: Screenshot(
                controller: screenshotController,
                child: Container(
                  height: 200.h,
                  width: 200.h,
                  color: ColorConfig.WHITE,
                  child: Center(
                    child: QrImage(
                      data: modelApi!.extraUrl!,
                      version: QrVersions.auto,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Scaffold(
            backgroundColor: ColorConfig.BACKGROUND,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.transparent,
              centerTitle: true,
              leading: GestureDetector(
                onTap: () {
                  Get.back();
                  Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
                  Controller.to.qrCodeVideoGuide.pause();
                  // ads controller
                  Controller.to.isAdsLoaded.value = false;
                },
                child: const Icon(Icons.arrow_back),
              ),
              title: Text(
                TextConfig.QRCODE,
                style: TextStyle(
                    color: ColorConfig.WHITE,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.sp,
                    fontFamily: FontConfig.ROBOTO_REGULAR),
              ),
              actions: [
                GuideButton(
                  onTap: () {
                    Controller.to.isQRCodeVideoEnd.value = false;
                    QRCodeVideoGuide(onCloseCallback: () {
                      Get.back();
                      Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
                      Controller.to.qrCodeVideoGuide.pause();
                    });
                  },
                ).marginSymmetric(horizontal: 15.r),
                modelApi!.adsSequence!.isNotEmpty
                    ? modelApi!.adSetting!.appVersionCode !=
                            int.parse(AppDetailsPP.to.buildNumber.value)
                        ? buyButton ?? Container(height: 0)
                        : Container(height: 0)
                    : Container(height: 0)
              ],
            ),
            body: Column(
              children: [
                smallNativeShow1.marginSymmetric(
                  horizontal: 15.r,
                ),
                const Spacer(),
                Image.asset(AssetsConfig.QRCODE_LOGO,
                        height: modelApi!.adSetting!.appVersionCode !=
                                int.parse(AppDetailsPP.to.buildNumber.value)
                            ? 255.h
                            : 300.h)
                    .marginSymmetric(vertical: 10.r),
                const Spacer(),
                ButtonWithTitle(
                        onTap: () async {
                          Controller.to.isQRCodeVideoEnd.value = false;
                          if (Platform.isAndroid) {
                            final androidInfo =
                                await DeviceInfoPlugin().androidInfo;
                            final sdkInt = androidInfo.version.sdkInt;

                            if (33 <= sdkInt) {
                              print('API-level above');

                              if(modelApi!.adSetting!.isFullAds == true){
                                if(Controller.to.isAdsLoaded.value == false){
                                  Controller.to.isAdsLoaded.value = true;
                                  showIntraAds(callback: () {
                                    Controller.to.isAdsLoaded.value = false;
                                    rewardPopupQR(context);
                                  });
                                }

                              }else{
                                Future.delayed(Duration.zero, () {
                                  rewardPopupQR(context);
                                });
                              }

                            } else {
                              print('API-level below');
                              await Permission.storage
                                  .request()
                                  .then((value) async {
                                if (value.isGranted) {
                                  if(modelApi!.adSetting!.isFullAds == true){
                                    if(Controller.to.isAdsLoaded.value == false){
                                      Controller.to.isAdsLoaded.value = true;
                                      showIntraAds(callback: () {
                                        Controller.to.isAdsLoaded.value = false;
                                        rewardPopupQR(context);
                                      });
                                    }
                                  }else{
                                    Future.delayed(Duration.zero, () {
                                      rewardPopupQR(context);
                                    });
                                  }

                                } else if (value.isPermanentlyDenied) {
                                  OpenSettingDialog(context);
                                }
                              });
                            }
                          }
                        },
                        button_color: ColorConfig.COLOR_1,
                        title: TextConfig.DOWNLOAD)
                    .marginOnly(bottom: 15.r),
              ],
            ),
            bottomNavigationBar: _bannerShow,
          ),
        ],
      ),
    );
  }

  Future takeScreenshot() async {
    Controller.to.pathhh.create();
    String dirname = Controller.to.pathhh.path;
    Controller.to.dynamicQrCodeName.value =
        '${Controller.to.FileSaveName}(${DateTime.now().millisecondsSinceEpoch}).png';
    await screenshotController
        .captureAndSave(dirname,
            fileName: Controller.to.dynamicQrCodeName.value)
        .then((value) {
      Controller.to.isQRCodeDownloading.value = true;
    });
  }

  rewardPopupQR(BuildContext context) {
    if (modelApi!.adsSequence!.isNotEmpty) {
      if (modelApi!.adSetting!.appVersionCode !=
          int.tryParse(AppDetailsPP.to.buildNumber.value)) {
        RewardDialog.show(context,
            rewardMessage: modelApi!.rewardDialog!.rewardMessage!, onTap: () {
          Navigator.pop(context);
          if(Controller.to.isAdsLoaded.value == false){
            Controller.to.isAdsLoaded.value = true;
            // ads Controller
            Future.delayed(const Duration(milliseconds: 50),() {
              Controller.to.isAdsLoaded.value = false;
            },);
            showRewardAds(
                callback: () => {
                  Controller.to.isAdsLoaded.value = false,
                  Controller.to.qrcodeVideo_Status.value == false
                      ? QRCodeVideoGuide(
                    onCloseCallback: () {
                      Get.back();
                      Future.delayed(const Duration(milliseconds: 10),
                              () {
                            Controller.to.qrCodeVideoGuide
                                .seekTo(Duration.zero);
                            Controller.to.qrCodeVideoGuide.pause();
                          });
                    },
                  ).then((value) async {
                    Future.delayed(
                      const Duration(milliseconds: 10),
                          () {
                        Controller.to.qrCodeVideoGuide
                            .seekTo(Duration.zero);
                        Controller.to.qrCodeVideoGuide.pause();
                      },
                    );
                    qrDownloader();
                  })
                      : qrDownloader()
                },
                onFailedCallback: () => {
                Controller.to.isAdsLoaded.value = false,
                  showIntraAds(callback: () {
                    Controller.to.qrcodeVideo_Status.value == false
                        ? QRCodeVideoGuide(
                      onCloseCallback: () {
                        Get.back();
                        Future.delayed(const Duration(milliseconds: 10),
                                () {
                              Controller.to.qrCodeVideoGuide
                                  .seekTo(Duration.zero);
                              Controller.to.qrCodeVideoGuide.pause();
                            });
                      },
                    ).then((value) async {
                      Future.delayed(
                        const Duration(milliseconds: 10),
                            () {
                          Controller.to.qrCodeVideoGuide
                              .seekTo(Duration.zero);
                          Controller.to.qrCodeVideoGuide.pause();
                        },
                      );
                      qrDownloader();
                    })
                        : qrDownloader();
                  })
                });
          }

        });
        return;
      }
    }
    Controller.to.qrcodeVideo_Status.value == false
        ? QRCodeVideoGuide(
            onCloseCallback: () {
              Get.back();
              Future.delayed(const Duration(milliseconds: 10), () {
                Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
                Controller.to.qrCodeVideoGuide.pause();
              });
            },
          ).then((value) async {
            Future.delayed(
              const Duration(milliseconds: 10),
              () {
                Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
                Controller.to.qrCodeVideoGuide.pause();
              },
            );
            qrDownloader();
          })
        : qrDownloader();
  }

  qrDownloader() {
    LoaderDialog(context);
    Future.delayed(const Duration(milliseconds: 500), () {
      takeScreenshot().then((value) async {
        if (Controller.to.isQRCodeDownloading.value == true) {
          Get.back();
          Controller.to.isQRCodeDownloading.value = false;
        }
        afterRewardDialog(
          context,
          title: TextConfig.DOWNLOAD_COMPLETE,
          widget: SizedBox(
            height: 170.h,
            width: 170.h,
            child: Center(
              child: QrImage(
                data: modelApi!.extraUrl!,
                version: QrVersions.auto,
                size: 170.0,
                foregroundColor: Colors.black,
                backgroundColor: Colors.white,
              ),
            ),
          ),
          marginLeft: 0,
          smallNote: TextConfig.DOWNLOAD_QRCODE_COMPLETE_LABEL,
          videoGuide: () {
            Controller.to.isQRCodeVideoEnd.value = false;
            QRCodeVideoGuide(onCloseCallback: () {
              Get.back();
              Future.delayed(Duration.zero, () {
                Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
                Controller.to.qrCodeVideoGuide.pause();
              });
            });
          },
        );

// todo local notification
        bool isAllowed = await AwesomeNotifications().isNotificationAllowed();
        if (!isAllowed) {
          AwesomeNotifications().requestPermissionToSendNotifications();
        } else {
          AwesomeNotifications().createNotification(
            content: NotificationContent(
                payload: {
                  "name": "Clickable Data",
                },
                color: const Color(0xff046689),
                id: 12,
                groupKey: 'qrcode',
                channelKey: 'basic_qrcode',
                title: '${TextConfig.APP_TITLE}.png',
                body: 'Download complete',
                largeIcon: 'asset://assets/app_logo.png'),
            actionButtons: [
              NotificationActionButton(
                key: "accept",
                label: "Open",
              ),
              NotificationActionButton(key: "reject", label: "View"),
            ],
          );
        }
      });
    });
  }

  Future<void> QRCodeVideoGuide({GestureTapCallback ? onCloseCallback}) {
    Controller.to.qrCodeVideoGuide.play();
    Controller.to.qrCodeVideoGuide.setPlaybackSpeed(0.8);
    Controller.to.isQRCodeVideoEnd.value = false;
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.qrCodeVideoGuide.addListener(() async {
          SharedPreferences pref_code = await SharedPreferences.getInstance();
          Controller.to.qrcodeVideo_Status.value =
              await pref_code.setBool('qrcode_video_status', true);
          if (Controller.to.isQRCodeVideoEnd.value == false) {
            if (Controller.to.qrCodeVideoGuide.value.position >=
                Controller.to.qrCodeVideoGuide.value.duration) {
              Controller.to.isQRCodeVideoEnd.value = true;
              Get.back();
              Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
              Controller.to.qrCodeVideoGuide.pause();
            }
          }
        });

        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
          child: WillPopScope(
            onWillPop: () {
              Get.back();
              Controller.to.qrCodeVideoGuide.seekTo(Duration.zero);
              Controller.to.qrCodeVideoGuide.pause();
              return Future.value(false);
            },
            child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 0.06.sh),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.white.withOpacity(0.5), width: 1),
                          borderRadius: BorderRadius.circular(16.0)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16.0),
                        child: AspectRatio(
                          aspectRatio:
                              Controller.to.qrCodeVideoGuide.value.aspectRatio,
                          child: VideoPlayer(Controller.to.qrCodeVideoGuide),
                        ),
                      ),
                    ),
                    // todo close button
                    Positioned(
                      top: 0.015.sh,
                      right: 0,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: onCloseCallback,
                        child: Container(
                          padding: EdgeInsets.all(5.r),
                          height: 25.h,
                          width: 25.h,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.9)),
                              color: Colors.white.withOpacity(0.9)),
                          child: Image.asset("assets/close2.png",
                              color: Colors.black.withOpacity(0.8), height: 12),
                        ),
                      ),
                    ),
                    // guide button
                    Positioned(
                      right: 0,
                      top: 0,
                      left: 0,
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/guide_image.png",
                            height: 90.h,
                          )),
                    ),
                  ],
                )),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    Controller.to.qrCodeVideoGuide.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () {
          if(Controller.to.inAppPurchaseTap.value == false){
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500),() async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset(AssetsConfig.REMOVE_ADS),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
