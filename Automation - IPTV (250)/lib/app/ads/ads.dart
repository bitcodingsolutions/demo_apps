import 'package:flutter/material.dart';
import 'package:google_applovin_unity_ads/ads/ads.dart';
import '../model/provider.dart';

abstract class AdsWidget<T extends StatefulWidget> extends State<T> {
  Widget? _bannerShow= Container(height:0);
  Widget? _smallNativeShow1= Container(height:0);
  Widget? _smallNativeShow2= Container(height:0);
  Widget? _smallNativeShow3 = Container(height:0);
  Widget? _fullAdsMediumnativeShow;

  Widget bannerAds() {
    if (_bannerShow == null) {
      _bannerShow = Container(height: 0);

      showBannerAds(onAdLoadedCallback: (p0) {
        setState(() {
          _bannerShow = p0;
        });
      });
    }

    return _bannerShow!;
  }

  Widget smallNativeAds1() {
    if (_smallNativeShow1 == null) {
      _smallNativeShow1 = Container(height: 0);

      showMediumNativeAds(
          onAdLoadedCallback: (a) => {
                setState(() {
                  _smallNativeShow1 = a;
                })
              });
    }
    return _smallNativeShow1!;
  }

  Widget smallNativeAds2() {
    if (_smallNativeShow2 == null) {
      _smallNativeShow2 = Container(height: 0);

      showMediumNativeAds(
          onAdLoadedCallback: (b) => {
                setState(() {
                  _smallNativeShow2 = b;
                })
              });
    }

    return _smallNativeShow2!;
  }

  Widget smallNativeAds3() {
    if (_smallNativeShow3 == null) {


      showMediumNativeAds(
          onAdLoadedCallback: (c01) => {
            setState(() {
              _smallNativeShow3 = c01;
            })
          });
    }

    return _smallNativeShow3!;
  }

  Widget fullAdsMediumNativeAds() {
    if (_fullAdsMediumnativeShow == null) {
      _fullAdsMediumnativeShow = Container(height: 0);

      showFullMediumNativeAds(
          onAdLoadedCallback: (a) => {
                setState(() {
                  _fullAdsMediumnativeShow = a;
                })
              });
    }

    return _fullAdsMediumnativeShow!;
  }



  isFullAdss({Function? callback}) {
    if (modelApi!.adSetting!.isFullAds == true) {
      showIntraAds(callback: callback);
    } else {
      if (callback != null) {
        callback();
      }
    }
  }
}
