// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:automation_iptv/app/constant/text_config.dart';
import 'package:google_applovin_unity_ads/ads/SettingsModel.dart';
import 'package:http/http.dart' as http;
import '../utilities/firebase_database_util.dart';

String settingsUrl = '';
Settings? modelApi;

Future<Settings> fetchAlbum() async {
  await FirebaseDatabaseUtil.instance.initState();

  dynamic settingsData =
      await FirebaseDatabaseUtil.instance.readData(TextConfig.PROVIDER_DATA_1);
  if (settingsData != null) {
     settingsUrl = settingsData[int.parse(TextConfig.PROVIDER_DATA_2)];
     print("kishan --->$settingsUrl");

  } else {
    settingsUrl =
        "";
  }

  final response = await http.get(Uri.parse(settingsUrl));

  if (response.statusCode == 200) {
    return modelApi = Settings.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}
