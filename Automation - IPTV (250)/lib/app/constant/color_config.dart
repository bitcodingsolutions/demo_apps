// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

class ColorConfig{
  // todo change here
  static Color BACKGROUND = const Color(0xff001B01);
  static Color COLOR_1 = const Color(0xff43A248);
  static Color COLOR_2 = const Color(0xffAD2A34);
  static Color COLOR_3 = const Color(0xff265627);


  static Color GREEN = const Color(0xff16BE01);
  static Color GREY = const Color(0xff9f9c9c);
  static Color WHITE = const Color(0xffFFFFFF);
  static Color BLACK = const Color(0xff000000);
  static Color BLUE = const Color(0xff0777b5);
}