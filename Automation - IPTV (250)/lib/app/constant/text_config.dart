// ignore_for_file: non_constant_identifier_names

import 'package:get/get.dart';

class TextConfig{
  // todo change here
  static String APP_TITLE = 'Arabic IPTV'.tr;
  static String FILE_SAVE_NAME = 'IPTV'.tr;

  // for provider data
  static String PROVIDER_DATA_1 = 'setting_data_1';
  static String PROVIDER_DATA_2 = '1';

  static String START = 'START'.tr;
  static String APP_SETTINGS = 'App Settings'.tr;
  static String GUIDE = 'Guide'.tr;
  static String NEXT = 'NEXT'.tr;
  static String FILE = 'File'.tr;
  static String LINK = 'Link'.tr;
  static String GET_LINK = 'Copy Link'.tr;
  static String QRCODE = 'QR Code'.tr;
  static String DOWNLOAD = 'Download'.tr;
  static String COPY = 'Copy'.tr;
  static String IPTV_PLAYER = 'IPTV Player (Ad)'.tr;
  static String AGREEMENT = "Agreement".tr;
  static String AGREEMENT_NOTICE = "Before you download please read carefully we are not providing TV Channel and it’s not TV channel app and this app gives you channel URL or File and QR code also".tr;
  static String REMOVE_ADS_NOTE = 'No Ads will display on this app be safe from annoying popup Ads.'.tr;
  static String GUIDE_LABEL = 'Guide for a better\nexperience of this app'.tr;
  static String DOWNLOAD_COMPLETE = 'Download Successfully'.tr;
  static String DOWNLOAD_QRCODE_COMPLETE_LABEL = 'Your IPTV list (QR Code) has been downloaded.'.tr;
  static String DOWNLOAD_FILE_COMPLETE_LABEL = "Your IPTV list (.m3u File) has been downloaded.".tr;
  static String COPY_COMPLETE = 'Copy Successfully'.tr;
  static String COPY_COMPLETE_LABEL = 'Your IPTV list (URL) has been copied.'.tr;
  static String PERMISSION_DENIED_1 = 'Please allow permission in'.tr;
  static String PERMISSION_DENIED_2 = 'app Setting'.tr;

  //for shared preferences
  static String IS_FIRST_TIME = 'isFirstTime';
}