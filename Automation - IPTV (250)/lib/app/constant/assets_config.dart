// ignore_for_file: non_constant_identifier_names

class AssetsConfig{
  // todo change here
  static String APP_LOGO = 'assets/app_logo.png';
  static String SPLASH = 'assets/splash_screen.png';


  static String MOBILE_LOGO = 'assets/mobile_logo.png';
  static String SPLASH_TEXT = 'assets/splash_text.png';
  static String REMOVE_ADS = 'assets/remove_ads.png';
  static String LOADING_ANIMATION = 'assets/animation/loading_animation.json';
  static String LINK_GROUP = 'assets/link_logo_grp.png';
  static String LINK_LOGO = 'assets/link_logo.png';
  static String FILE_GROUP = 'assets/file_logo_grp.png';
  static String FILE_LOGO = 'assets/file_logo.png';
  static String QRCODE_GROUP = 'assets/qrcode_logo_grp.png';
  static String QRCODE_LOGO = 'assets/qrcode_logo.png';
  static String DONE = 'assets/done.png';
  static String FILE_ONLY = 'assets/file.png';
  static String LINK_ONLY = 'assets/link.png';
  static String QR_CODE_ONLY = 'assets/qr_code.png';
  static String GUIDE_LOGO = "assets/guide.png";
  // static String START_LOGO = 'assets/start_logo.png';
}