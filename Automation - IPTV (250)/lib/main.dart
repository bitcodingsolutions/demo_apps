import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'app/modules/splash_module.dart';
import 'notification.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  /// notification
  listenActionStream();
  AwesomeNotifications().initialize('resource://drawable/iptv', [
    NotificationChannel(
      channelGroupKey: 'basic_test',
      channelKey: 'basic_file',
      channelName: 'Basic notifications',
      channelDescription: 'Notification channel for basic tests',
      importance: NotificationImportance.High,
      enableVibration: true,
    ),
    NotificationChannel(
      channelGroupKey: 'qrcode',
      channelKey: 'basic_qrcode',
      channelName: 'Basic notifications',
      channelDescription: 'Notification channel for basic tests',
      importance: NotificationImportance.High,
      enableVibration: true,
    ),
  ]);
  await Firebase.initializeApp();
  MobileAds.instance.initialize();
  FacebookAds.instance.initialize();
  await PPDialog.newVersionCode();
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(FeatureDiscovery(child: ScreenUtilInit(builder: (BuildContext context, Widget? child) {
    return const GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Splash(),
    );
  })));
}
