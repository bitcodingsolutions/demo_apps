// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:marquee/marquee.dart';
import 'package:open_filex/open_filex.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';
import 'app/constant/color_config.dart';
import 'app/constant/font_config.dart';
import 'app/constant/text_config.dart';
import 'app/controller/get_controller.dart';
import 'app/model/provider.dart';

listenActionStream() {
  AwesomeNotifications().setListeners(
      onActionReceivedMethod: (receivedAction) async {
    if (receivedAction.buttonKeyPressed == "accept") {

      OpenFilex.open('/storage/emulated/0/Download/');
    } else if (receivedAction.buttonKeyPressed == "reject") {
      Get.to(const QRCodeViewer());
    } else if (receivedAction.channelKey == 'basic_qrcode') {
      print('dynamic qrcode name ---> ${Controller.to.dynamicQrCodeName.value}');
      OpenFilex.open(
          '/storage/emulated/0/Download/${TextConfig.APP_TITLE.replaceAll(" ", "")}/${Controller.to.dynamicQrCodeName.value}');
    } else if (receivedAction.channelKey == 'basic_file') {
      print('dynamic file name ---> ${Controller.to.dynamicFileName.value}');
      OpenFilex.open(
          '/storage/emulated/0/Download/${TextConfig.APP_TITLE.replaceAll(" ", "")}/${Controller.to.dynamicFileName.value}');
    }
  });
}



class QRCodeViewer extends StatelessWidget {
  const QRCodeViewer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var link = modelApi!.extraUrl;
    return WillPopScope(
      onWillPop: () {
        Get.back();
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            Align(
                    alignment: Alignment.center,
                    child: TextConfig.APP_TITLE.length < 25
                        ? Text(
                            TextConfig.APP_TITLE.tr,
                            style: TextStyle(
                                color: ColorConfig.WHITE,
                                fontSize: 22.sp,
                                fontWeight: FontWeight.bold,
                                fontFamily: FontConfig.ROBOTO_REGULAR),
                          )
                        : SizedBox(
                            height: 25.h,
                            child: Marquee(
                              text: TextConfig.APP_TITLE.tr,
                              style: TextStyle(
                                  color: ColorConfig.BLACK,
                                  fontSize: 22.sp,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: FontConfig.ROBOTO_REGULAR),
                              showFadingOnlyWhenScrolling: false,
                              scrollAxis: Axis.horizontal,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              blankSpace: 20.0,
                              velocity: 50.0,
                              pauseAfterRound: const Duration(seconds: 1),
                              startPadding: 0.0,
                              accelerationDuration: const Duration(seconds: 1),
                              accelerationCurve: Curves.linear,
                              decelerationDuration:
                                  const Duration(milliseconds: 500),
                              decelerationCurve: Curves.easeOut,
                            ),
                          ))
                .marginOnly(top: 40.r, left: 15.r, right: 15.r),
            const Spacer(),
            Center(
              child: Container(
                color: ColorConfig.WHITE,
                child: Center(
                  child: QrImage(
                    data: '$link',
                    version: QrVersions.auto,
                  ),
                ),
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
