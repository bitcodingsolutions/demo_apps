import 'package:animate_do/animate_do.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:dialog_pp_flutter/api_call.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/start_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api/all_json_api/provider.dart';
import 'dialog/data_fill_dialog.dart';
import 'dialog/getx_controller_common.dart';
import 'firebase_database.dart';
import 'main.dart';
import 'notification.dart';

class SplashScreen1 extends StatefulWidget {
  const SplashScreen1({Key? key}) : super(key: key);

  @override
  State<SplashScreen1> createState() => _SplashScreen1State();
}

class _SplashScreen1State extends State<SplashScreen1> {
  @override
  void initState() {
    restoreSharedPreferenceValue();
    PPDialog.internetDialogCheck(callback: () async {
      connectionCheck();
      listenActionStream();
      AwesomeNotifications().initialize('resource://drawable/iptv', [
        NotificationChannel(
            channelGroupKey: 'file',
            channelKey: 'file_download',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            // channelShowBadge: true,
            importance: NotificationImportance.High,
            enableVibration: true,
            onlyAlertOnce: true),
        NotificationChannel(
            channelGroupKey: 'qrcode',
            channelKey: 'qrcode_save',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            // channelShowBadge: true,
            importance: NotificationImportance.High,
            enableVibration: true,
            onlyAlertOnce: true),
      ]);
      PPDialog.autoUpdate();
    });
    super.initState();
  }

  connectionCheck() async {
    PPDialog.continueDataCheck();
    MobileAds.instance.initialize();
    FacebookAds.instance.initialize();
    await FirebaseDatabaseUtil.instance.initState();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    await AdsSettingProvider();
    await fetchAdsSettings(
            appVersionCode: int.parse(AppDetailsPP.to.buildNumber.value),
            settingsUrl: settingsUrl,
            keyName: keyName)
        .then((value) async => {
              await initOpenAds(onOpenAdLoaded: () => {showOpenAds()}),
              apiModel = getAdsSettings(),
              if (value?.adsSequence?.contains("app_lovin") ?? false)
                {
                  if ((getAdsSettings()?.appLovin?.sdkKey ?? "").isNotEmpty)
                    {
                      AppLovinAds.instance
                          .initialize(value!.appLovin!.sdkKey!)
                          .then((value) async => {
                                await allCountryApiJson(),
                                await methodApiAndAds()
                              })
                    }
                  else
                    {await allCountryApiJson(), await methodApiAndAds()}
                }
              else
                {await allCountryApiJson(), await methodApiAndAds()}
            });
  }

  restoreSharedPreferenceValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Controller.to.privacyCheck1.value = prefs.getBool('privacy') ?? false;
    Controller.to.privacyCheck2.value = prefs.getBool('terms') ?? false;
  }

  methodApiAndAds() async {
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    if (apiModel!.adSetting!.appVersionCode !=
        int.parse(AppDetailsPP.to.buildNumber.value)) {
      await advertisementDrawerApi(apiModel!.moreLiveApps!);
      await advertisement_UpdateAppList_Api(apiModel!.appUpdate!.appListUrl!);
    }
      Get.off(const StartButtonScreen());
   }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Container(
          height: 1000.h,
          width: 1000.w,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            colorGradient1,
            colorGradient2,
          ], begin: Alignment.topCenter)),
          child: SafeArea(
            child: Column(
              children: [
                SizedBox(
                  height: 80.h,
                ),
                FadeInDownBig(
                  child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      child: Image.asset(
                        appLogo,
                        height: 120.h,
                        width: 120.h,
                      )),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FadeInRightBig(
                      child: Text(
                        appNameSplashScreen,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.w,
                            color: const Color(0xfffc5f5f)),
                      ),
                    ),
                    FadeInUpBig(
                      child: Text(
                        "FREE ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.w,
                            color: Colors.white),
                      ),
                    ),
                    FadeInLeftBig(
                      child: Text(
                        "IPTV",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.w,
                            color: Colors.yellowAccent),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: EdgeInsets.only(bottom: 45.h),
                  child: FadeInDownBig(
                    child: const SpinKitWave(
                        color: Colors.white, type: SpinKitWaveType.start),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
