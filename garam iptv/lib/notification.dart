import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:open_filex/open_filex.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dialog/getx_controller_common.dart';
import 'main.dart';

listenActionStream() {
  AwesomeNotifications().setListeners(
      onActionReceivedMethod: (receivedAction) async {
    if (receivedAction.buttonKeyPressed == 'Open') {
      OpenFilex.open('/storage/emulated/0/Download/');
    } else if (receivedAction.buttonKeyPressed == "view") {
      Get.to(const QRCodeViewer());
    } else if (receivedAction.channelKey == 'qrcode_save') {
      OpenFilex.open(
          '/storage/emulated/0/Download/${appName.replaceAll(" ", "")}/${Controller.to.dynamicImageName.value}');
    }else if (receivedAction.channelKey == 'file_download') {
      OpenFilex.open(
          '/storage/emulated/0/Download/${appName.replaceAll(" ", "")}/${Controller.to.dynamicFileName.value}');
    }
  });
}

class QRCodeViewer extends StatelessWidget {
  const QRCodeViewer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var link = Controller.to.dynamicLink.value;
    return WillPopScope(
      onWillPop: () {
        Get.back();
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            Align(
                alignment: Alignment.center,
                child: Text(
                  "${Controller.to.dynamicTitle.value}  IPTV",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22.sp,
                      fontWeight: FontWeight.bold,
                      ),
                )).marginOnly(top: 40.r, left: 15.r, right: 15.r),
            const Spacer(),
            Center(
              child: Container(
                color: Colors.white,
                child: Center(
                  child: QrImage(
                    data: link,
                    version: QrVersions.auto,
                  ),
                ),
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
