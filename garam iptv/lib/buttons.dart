import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Widget buttonGreen(String title) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 0.h, top: 0.h),
    alignment: Alignment.center,
    decoration: const BoxDecoration(
        color: Color(0xff00CB4C),
        borderRadius: BorderRadius.all(Radius.circular(14))),
    child: Center(
      child: Text(
        title,
        style: TextStyle(
            color: Colors.white, fontSize: 18.w, fontWeight: FontWeight.bold),
      ),
    ),
  );
}

Widget buttonAdLable(String title) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 0.h, top: 0.h),
    alignment: Alignment.center,
    decoration: const BoxDecoration(
        color: Color(0xff00CB4C),
        borderRadius: BorderRadius.all(Radius.circular(14))),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          child: Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.w,
                fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          margin: EdgeInsets.all(0.5.h),
          padding: EdgeInsets.all(0.1.h),
          child: const Text(
            "(Ad)",
            style: TextStyle(fontWeight: FontWeight.w400, color: Colors.white),
          ),
        ),
      ],
    ),
  );
}

Widget blueGreen(String title) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 0.h, top: 0.h),
    alignment: Alignment.center,
    decoration: const BoxDecoration(
        color: Color(0xff007AC1),
        borderRadius: BorderRadius.all(Radius.circular(14))),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.w,
                fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          padding: EdgeInsets.all(0.1.h),
          child: Text(
            " (Ad)",
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white.withOpacity(0.7),),
          ),
        ),
      ],
    ),
  );
}

//todo appbar title
String appbarTitle = "Indian IPTV List";

//todo Agreement Dialog
Color themeButtonColor = const Color(0xff46A819);
Color themeColor = const Color(0xff0B091C);

/// todo SplashScreen
String commonImage = "assets/new/splash.png";

//todo buttonImages
String downloadFileImage = "assets/new/file.png";
String linkImage = "assets/new/link.png";
String qrCodeImage = "assets/new/qr.png";

Color downloadFileColor = const Color(0xffFF9100);
Color linkColor = const Color(0xff0073E6);
Color qrCodeColor = const Color(0xff46A819);

String playButtonImage = "assets/play_button_icon.png";

//todo getStartScreen
String getStartImage = "assets/new/splashsecond.png";

//todo save file
String fileSaveName = "Indian";

Widget customButton(String title, {Color? color = const Color(0xff00CB4C)}) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 0.h, top: 0.h),
    // padding: EdgeInsets.all(1.5.h),
    alignment: Alignment.center,
    decoration: BoxDecoration(
        color: color,
        borderRadius: const BorderRadius.all(Radius.circular(14))),
    child: Center(
      child: Text(
        title,
        style: TextStyle(
            color: Colors.white, fontSize: 18.w, fontWeight: FontWeight.bold),
      ),
    ),
  );
}


Widget howToUse(String title, {Color? color =  Colors.grey}) {
  return Container(
    width: 100.w,
    height: 40.h,
    margin: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 0.h, top: 0.h),
    // padding: EdgeInsets.all(1.5.h),
    alignment: Alignment.center,
    decoration: BoxDecoration(
        color: color,
        borderRadius: const BorderRadius.all(Radius.circular(14))),
    child: Center(
      child: Text(
        title,
        style: TextStyle(
            color: Colors.white, fontSize: 18.w, fontWeight: FontWeight.bold),
      ),
    ),
  );
}
