/*
import 'package:animate_do/animate_do.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:dialog_pp_flutter/api_call.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/start_button.dart';
import 'package:lottie/lottie.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'api/all_json_api/provider.dart';
import 'dialog/data_fill_dialog.dart';
import 'dialog/getx_controller_common.dart';
import 'firebase_database.dart';
import 'main.dart';
import 'notification.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget? _smallNativeShow1 = Container(
    height: 0,
  );
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  @override
  void initState() {
    super.initState();
    restoreSharedPreferenceValue();
    PPDialog.internetDialogCheck(callback: () async {
      initPlatformState();
      updateBuyRemoveAdsButton();
      fetchOldPurchaseDetails();

      connectionCheck();
      listenActionStream();
      AwesomeNotifications().initialize('resource://drawable/iptv', [
        NotificationChannel(
            channelGroupKey: 'file',
            channelKey: 'file_download',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            // channelShowBadge: true,
            importance: NotificationImportance.High,
            enableVibration: true,
            onlyAlertOnce: true),
        NotificationChannel(
            channelGroupKey: 'qrcode',
            channelKey: 'qrcode_save',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            // channelShowBadge: true,
            importance: NotificationImportance.High,
            enableVibration: true,
            onlyAlertOnce: true),
      ]);
      PPDialog.autoUpdate();

    });
    Future.delayed(
      Duration.zero,
          () {
        showMediumNativeAds(
            onAdLoadedCallback: (p1) => {
              setState(() {
                _smallNativeShow1 = p1;
              })
            });
      },
    );

  }

  connectionCheck() async {
    PPDialog.continueDataCheck();
    MobileAds.instance.initialize();
    FacebookAds.instance.initialize();
    await FirebaseDatabaseUtil.instance.initState();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    await AdsSettingProvider();
    await fetchAdsSettings(
            appVersionCode: int.parse(AppDetailsPP.to.buildNumber.value),
            settingsUrl: settingsUrl,
            keyName: keyName)
        .then((value) async => {
              await initOpenAds(onOpenAdLoaded: () => {showOpenAds()}),
              apiModel = getAdsSettings(),
              if (value?.adsSequence?.contains("app_lovin") ?? false)
                {
                  if ((getAdsSettings()?.appLovin?.sdkKey ?? "").isNotEmpty)
                    {
                      AppLovinAds.instance
                          .initialize(value!.appLovin!.sdkKey!)
                          .then((value) async => {
                                await allCountryApiJson(),
                                await methodApiAndAds()
                              })
                    }
                  else
                    {await allCountryApiJson(), await methodApiAndAds()}
                }
              else
                {await allCountryApiJson(), await methodApiAndAds()}
            });
  }

  methodApiAndAds() async {
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    if (apiModel!.adSetting!.appVersionCode !=
        int.parse(AppDetailsPP.to.buildNumber.value)) {
      await advertisementDrawerApi(apiModel!.moreLiveApps!);
      await advertisement_UpdateAppList_Api(apiModel!.appUpdate!.appListUrl!);
    }

    if(Controller.to.privacyCheck1.value == false && Controller.to.privacyCheck2.value == false){
      privacyPolicyDialogNew(context);
    }else{
      Controller.to.DataLoadStatus.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Controller.to.DataLoadStatus.value == false
        ? WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: Scaffold(
              body: Container(
                height: 1000.h,
                width: 1000.w,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  colorGradient1,
                  colorGradient2,
                ], begin: Alignment.topCenter)),
                child: SafeArea(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 80.h,
                      ),
                      FadeInDownBig(
                        child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(20)),
                            child: Image.asset(
                              appLogo,
                              height: 120.h,
                              width: 120.h,
                            )),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FadeInRightBig(
                            child: Text(
                              appNameSplashScreen,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.w,
                                  color: const Color(0xfffc5f5f)),
                            ),
                          ),
                          FadeInUpBig(
                            child: Text(
                              "FREE ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.w,
                                  color: Colors.white),
                            ),
                          ),
                          FadeInLeftBig(
                            child: Text(
                              "IPTV",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.w,
                                  color: Colors.yellowAccent),
                            ),
                          ),
                        ],
                      ),
                      const Spacer(),
                      Padding(
                        padding: EdgeInsets.only(bottom: 45.h),
                        child: FadeInDownBig(
                          child: const SpinKitWave(
                              color: Colors.white, type: SpinKitWaveType.start),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        : WillPopScope(
            onWillPop: () async {
              if (scaffoldKey.currentState!.isDrawerOpen) {
                scaffoldKey.currentState!.closeDrawer();
              } else {
                if (Controller.to.isDrawerGuideFirstTime.value == true &&
                    Controller.to.isInAppPurchaseGuideFirstTime.value == true &&
                    Controller.to.isRateAppGuideFirstTime.value == true) {
                  PPDialog.exitDialog(
                      appLogo: appLogo, mediumNative: _smallNativeShow1);
                }
              }
              return false;
            },
            child: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  height: 1000.h,
                  width: 1000.w,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    colorGradient1,
                    colorGradient2,
                  ], begin: Alignment.topCenter)),
                ),
                Scaffold(
                  extendBodyBehindAppBar: true,
                  backgroundColor: Colors.transparent,
                  key: scaffoldKey,
                  appBar: AppBar(
                      backgroundColor: Colors.transparent,
                      centerTitle: true,
                      leading: DescribedFeatureOverlay(
                        featureId: Controller.to.isDrawerGuideFirstTime.value == false ? feature1 : '',
                        tapTarget: const Icon(Icons.menu),
                        backgroundColor: Colors.teal,

                        title: const Text(
                            'This is overly long on purpose to test OverflowMode.clip!'),
                        description: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const <Widget>[
                            Text(
                                'Also, notice how the pulsing animation is not playing because it is deactivated for this feature.'),
                          ],
                        ),
                        child: GestureDetector(
                            onTap: () {
                              scaffoldKey.currentState!.openDrawer();
                            },
                            child: const Icon(Icons.menu)),
                        onComplete: () async {
                          print('drawer guide done');
                          Controller.to.isDrawerGuideFirstTime.value = true;
                          SharedPreferences prefs =
                              await SharedPreferences.getInstance();
                          prefs.setBool('isFirstDrawer',
                              Controller.to.isDrawerGuideFirstTime.value);

                          return Future.value(true);
                        },
                        barrierDismissible: false,
                      ),
                      title: Text(
                        appName,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 19.sp,
                          fontWeight: FontWeight.bold,
                          shadows: const <Shadow>[
                            Shadow(
                              offset: Offset(2.0, 2.0),
                              blurRadius: 6.0,
                              color: Colors.redAccent,
                            ),
                          ],
                        ),
                      ),
                      actions: [
                        DescribedFeatureOverlay(
                          featureId: Controller.to.isRateAppGuideFirstTime.value == false ? feature2 : '',
                          tapTarget: Lottie.asset(
                            "assets/star_animation.json",
                          ),
                          backgroundColor: Colors.blue,
                          title: const Text('Search your compounds'),
                          description: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const Text(
                                  'Tap the magnifying glass to quickly scan your compounds'),
                              TextButton(
                                onPressed: () async =>
                                    FeatureDiscovery.completeCurrentStep(
                                        context),
                                child: Text(
                                  'Understood',
                                  style: Theme.of(context)
                                      .textTheme
                                      .button!
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                              TextButton(
                                onPressed: () =>
                                    FeatureDiscovery.dismissAll(context),
                                child: Text(
                                  'Dismiss',
                                  style: Theme.of(context)
                                      .textTheme
                                      .button!
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          onComplete: () async {
                            print('rate guide done');

                            Controller.to.isRateAppGuideFirstTime.value = true;
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setBool('isFirstRate',
                                Controller.to.isRateAppGuideFirstTime.value);

                            return true;
                          },
                          barrierDismissible: false,
                          child: InkWell(
                            onTap: () {
                              PPDialog.rateDialogCustom(context,
                                  logoApp: appLogo);
                            },
                            highlightColor: Colors.transparent,
                            splashColor: Colors.transparent,
                            child: Lottie.asset(
                              "assets/star_animation.json",
                            ),
                          ),
                        ),

                        DescribedFeatureOverlay(
                          featureId: Controller.to.isInAppPurchaseGuideFirstTime.value == false ? feature3: '',
                          tapTarget: Swing(
                            infinite: true,
                            child: Image.asset(
                              'assets/remove_ads.png',
                            ),
                          ),
                          backgroundColor: Colors.green,
                          title: const Text('Search your compounds'),
                          enablePulsingAnimation: false,
                          overflowMode: OverflowMode.wrapBackground,
                          description: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const Text(
                                  'Tap the magnifying glass to quickly scan your compounds'),
                              TextButton(
                                onPressed: () async =>
                                    FeatureDiscovery.completeCurrentStep(
                                        context),
                                child: Text(
                                  'Understood',
                                  style: Theme.of(context)
                                      .textTheme
                                      .button!
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                              TextButton(
                                onPressed: () =>
                                    FeatureDiscovery.dismissAll(context),
                                child: Text(
                                  'Dismiss',
                                  style: Theme.of(context)
                                      .textTheme
                                      .button!
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          onComplete: () async {
                            print('in app guide done');

                            Controller.to.isInAppPurchaseGuideFirstTime.value =
                                true;
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setBool('isFirstInApp',
                                Controller.to.isRateAppGuideFirstTime.value);

                            return true;
                          },
                          barrierDismissible: false,
                          child: buyButton!,
                        ) ,
                      ],
                      elevation: 0),
                  drawer: PPDialog.drawerScreen(
                      appLogo: appLogo,
                      privacyPolicy:
                          "${apiModel!.privacyPolicy!.privacyPolicy}",
                      feedbackEmailId: "${apiModel!.feedbackSupport!.emailId}",
                      emailSubject:
                          "${apiModel!.feedbackSupport!.emailSubject}",
                      emailMessage:
                          "${apiModel!.feedbackSupport!.emailMessage}",
                      apiAppVersionCode: apiModel!.adSetting!.appVersionCode!),
                  body: const StartButtonScreen(),
                ),
              ],
            ),
          ));
  }

  restoreSharedPreferenceValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Controller.to.isRateAppGuideFirstTime.value =
        prefs.getBool('isFirstRate') ?? false;
    Controller.to.isInAppPurchaseGuideFirstTime.value =
        prefs.getBool('isFirstInApp') ?? false;
    Controller.to.isDrawerGuideFirstTime.value =
        prefs.getBool('isFirstDrawer') ?? false;
    Controller.to.privacyCheck1.value = prefs.getBool('privacy') ?? false;
    Controller.to.privacyCheck2.value = prefs.getBool('terms') ?? false;

    if(Controller.to.isDrawerGuideFirstTime.value == true &&
        Controller.to.isInAppPurchaseGuideFirstTime.value == true &&
        Controller.to.isRateAppGuideFirstTime.value == true){
      FeatureDiscovery.dismissAll(context);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () async {
          await Controller.to.btcPurchasePlugin
              .launchPurchase(Controller.to.purchaseKey.value);
 showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );

        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset('assets/remove_ads.png'),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
*/
