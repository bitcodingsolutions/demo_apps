// ignore_for_file: deprecated_member_use, non_constant_identifier_names, prefer_typing_uninitialized_variables

import 'package:animate_do/animate_do.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/main.dart';
import 'package:iptv/open_channel.dart';
import 'package:loading_gifs/loading_gifs.dart';
import 'package:lottie/lottie.dart';
import 'package:restart_app/restart_app.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'api/all_json_api/provider.dart';
import 'dialog/data_fill_dialog.dart';
import 'dialog/getx_controller_common.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget? _bannerShow = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    initPlatformState();
    updateBuyRemoveAdsButton();
    fetchOldPurchaseDetails();
    showBannerAds(
        size: AdSize.banner,
        onAdLoadedCallback: (p0) {
          setState(() {
            _bannerShow = p0;
          });
        });
    AdsController.instance.widgetlist.clear();
    Future.delayed(const Duration(seconds: 0), () {
      updateDialogNew(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: () {
        if (scaffoldKey.currentState!.isDrawerOpen) {
          scaffoldKey.currentState!.closeDrawer();
        } else {
          Get.back();
        }
        return Future.value(false);
      },
      child: Scaffold(
        key: scaffoldKey,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: GestureDetector(
              onTap: () {
                scaffoldKey.currentState!.openDrawer();
              },
              child: const Icon(Icons.menu)),
          title: Text(
            appName,
            style: TextStyle(
              color: Colors.white,
              fontSize: 19.sp,
              fontWeight: FontWeight.bold,
              shadows: const <Shadow>[
                Shadow(
                  offset: Offset(2.0, 2.0),
                  blurRadius: 6.0,
                  color: Colors.redAccent,
                ),
              ],
            ),
          ),
          actions: [
            InkWell(
              onTap: () {
                PPDialog.rateDialogCustom(context, logoApp: appLogo);
              },
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Lottie.asset(
                "assets/star_animation.json",
              ),
            ),
            apiModel!.adsSequence!.isNotEmpty
                ? apiModel!.adSetting!.appVersionCode !=
                        int.parse(AppDetailsPP.to.buildNumber.value)
                    ? buyButton ?? Container(height: 0)
                    : SizedBox(height: 0.h, width: 0.w)
                : SizedBox(height: 0.h, width: 0.w)
          ],
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        drawer: PPDialog.drawerScreen(
            appLogo: appLogo,
            privacyPolicy: "${apiModel!.privacyPolicy!.privacyPolicy}",
            feedbackEmailId: "${apiModel!.feedbackSupport!.emailId}",
            emailSubject: "${apiModel!.feedbackSupport!.emailSubject}",
            emailMessage: "${apiModel!.feedbackSupport!.emailMessage}",
            apiAppVersionCode: apiModel!.adSetting!.appVersionCode!),
        bottomNavigationBar: BottomAppBar(
          child: _bannerShow,
        ),
        body: Container(
            height: 1000.h,
            width: 1000.w,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              colorGradient1,
              colorGradient2,
            ], begin: Alignment.topCenter)),
            child: SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 10.h),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: FutureBuilder(
                        future: allCountryApiJson(),
                        builder: (context, snapshot) {
                          AdsController.instance.widgetlist.clear();
                          AdsController.instance.widgetlist.addAll(
                              channelApiList!.records[recodeIndex].subLinks);
                          return ScrollConfiguration(
                              behavior: const ScrollBehavior(
                                  androidOverscrollIndicator:
                                      AndroidOverscrollIndicator.stretch),
                              child: showAdsInListView(
                                widgetMargin:
                                    EdgeInsets.symmetric(vertical: 10.r),
                                isNativeAds: true,
                                bannerSize: AdSize.mediumRectangle,
                                adsDistance:
                                    apiModel!.adSetting!.nativeAdListInterval!,
                                controller: AdsController.instance,
                                itemBuilder: (context, index) {
                                  Widget widget = Container(
                                    height: 0,
                                  );
                                  if (AdsController
                                              .instance.widgetlist[index] !=
                                          null &&
                                      AdsController.instance.widgetlist[index]
                                          is String) {
                                    int pos = index -
                                        (index ~/
                                            (apiModel!.adSetting!
                                                .nativeAdListInterval!)) +
                                        1;

                                    widget = InkWell(
                                      splashColor: Colors.transparent,
                                      hoverColor: Colors.transparent,
                                      highlightColor: Colors.transparent,
                                      onTap: () {
                                          showIntraAds(callback: () {
                                            Get.to(OpenChannelScreen(
                                                AdsController
                                                    .instance.widgetlist[index],
                                                "$appNameSplashScreen$pos"));
                                          });
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(20.h),
                                        margin: EdgeInsets.only(
                                            top: 10.h, bottom: 10.h),
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                child: FadeInImage.assetNetwork(
                                                  fit: BoxFit.contain,
                                                  placeholder:
                                                      cupertinoActivityIndicator,
                                                  image: channelApiList!
                                                      .records[recodeIndex]
                                                      .countryImageUrl,
                                                  height: 55
                                                      .h, /* placeholderScale: 50*/
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 20.w,
                                            ),
                                            SizedBox(
                                              child: Text(
                                                "$appNameSplashScreen $pos",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14.w),
                                              ),
                                              width: 150.w,
                                            ),
                                            const Spacer(),
                                            const Icon(Icons.arrow_forward_ios),
                                            SizedBox(
                                              width: 10.w,
                                            ),
                                            SizedBox(
                                              width: 10.w,
                                            )
                                          ],
                                        ),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.9),
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(18))),
                                      ),
                                    );
                                  }
                                  return VisibilityDetector(
                                      key: Key(index.toString()),
                                      onVisibilityChanged:
                                          (VisibilityInfo info) {
                                        if (info.visibleFraction == 1) {
                                          AdsController.instance.currentIndex
                                              .value = index;
                                        }
                                      },
                                      child: widget);
                                },
                              ));
                        },
                      ),
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () {
          if(Controller.to.inAppPurchaseTap.value == false){
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500),() async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset('assets/remove_ads.png'),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
