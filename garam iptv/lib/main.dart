import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:iptv/splash_screen.dart';
import 'package:iptv/splashscreen.dart';

String appName = "NangBros IPTV";
String appNameSplashScreen = "NangBros ";
Color colorGradient1 = const Color(0xff00a4e5);
Color colorGradient2 = const Color(0xff641f79);
String providerData1 = "Setting_Data";
String providerData2 = "abear_url";
String appLogo = "assets/appLogo.png";
String keyName = "com.livex.abear.tv.channels";
int recodeIndex = 1;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  timeDilation = 1.0;
  await PPDialog.newVersionCode();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(FeatureDiscovery(child: ScreenUtilInit(
    builder: (context, child) => const GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen1(),
    ),
  )));
}


