// ignore_for_file: must_be_immutable, prefer_typing_uninitialized_variables, import_of_legacy_library_into_null_safe, avoid_print, non_constant_identifier_names, unused_local_variable
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:animate_do/animate_do.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:clipboard/clipboard.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/dialog/data_fill_dialog.dart';
import 'package:iptv/dialog/openAppSetting.dart';
import 'package:iptv/guide/video_guide/qrcode_guide.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:restart_app/restart_app.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api/all_json_api/provider.dart';
import 'dialog/dialog_and_widget.dart';
import 'dialog/getx_controller_common.dart';
import 'dialog/guide.dart';
import 'dialog/reward Dialog.dart';
import 'guide/video_guide/file_guide.dart';
import 'guide/video_guide/link_guide.dart';
import 'main.dart';

class OpenChannelScreen extends StatefulWidget {
  var link;
  var name;

  OpenChannelScreen(this.link, this.name, {Key? key}) : super(key: key);

  @override
  State<OpenChannelScreen> createState() => _OpenChannelScreenState();
}

class _OpenChannelScreenState extends State<OpenChannelScreen>
    with WidgetsBindingObserver
    implements PurchaseListener {
  /// ads
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget _smallNativeShow1 = Container(
    height: 0,
  );
  Widget _smallNativeShow2 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  GlobalKey gk = GlobalKey();
  Directory pathhh = Directory('/storage/emulated/0/Download/$appName');
  final ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();
    showBannerAds(
        size: AdSize.banner,
        onAdLoadedCallback: (p0) {
            setState(() {
              _bannerShow = p0;
            });
        });

    showMediumNativeAds(
        onAdLoadedCallback: (a) {
                setState(() {
                  _smallNativeShow1 = a;
                });
            });

    showMediumNativeAds(
        onAdLoadedCallback: (b) {
                setState(() {
                  _smallNativeShow2 = b;
                });
            });

    WidgetsBinding.instance.addObserver(this);
    GuideForFirstTime();
    Future.delayed(const Duration(seconds: 20), () {
      PPDialog.autoRate();
    });
    Future.delayed(Duration.zero, () {
      initPlatformState();
      updateBuyRemoveAdsButton();
      fetchOldPurchaseDetails();
      Controller.to.dynamicLink.value = widget.link;
      Controller.to.dynamicTitle.value = widget.name;
      LinkGuide.loadLinkVideoInit();
      FileGuide.loadFileVideoInit();
      QRCodeGuide.loadQRCodeVideoInit();
    });
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      setState(() {});
    });
    FlutterDownloader.registerCallback(downloadCallback);
  }

  GuideForFirstTime() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    final bool repeat = pref.getBool('guide') ?? false;
    if (repeat == false) {
      guideIntro(context);
    }
  }

  RewardLinkPopup() async {
    SharedPreferences pref_link = await SharedPreferences.getInstance();
    Controller.to.linkVideo_Status.value =
        pref_link.getBool('link_video_status') ?? false;

    if (apiModel!.adsSequence!.isNotEmpty) {
      if (apiModel!.adSetting!.appVersionCode !=
          int.parse(AppDetailsPP.to.buildNumber.value)) {
        PPDialog.rewardDialog(
            rewardMessage: apiModel!.rewardDialog!.rewardMessage!,
            onTap: () {
              Navigator.pop(context);
              showRewardAds(callback: () {
                Controller.to.linkVideo_Status.value == false
                    ? LinkGuide.linkVideoGuide(context, onTap: () {
                        Get.back();
                        Controller.to.linkController.seekTo(Duration.zero);
                        Controller.to.linkController.pause();
                      }).then((value) {
                        linkCopy();
                      })
                    : linkCopy();
              }, onFailedCallback: () {
                showIntraAds(callback: () {
                  Controller.to.linkVideo_Status.value == false
                      ? LinkGuide.linkVideoGuide(context, onTap: () {
                          Get.back();
                          Controller.to.linkController.seekTo(Duration.zero);
                          Controller.to.linkController.pause();
                        }).then((value) {
                          linkCopy();
                        })
                      : linkCopy();
                });
              });
            });
        return;
      }
    }
    Controller.to.linkVideo_Status.value == false
        ? LinkGuide.linkVideoGuide(context, onTap: () {
            Get.back();
            Controller.to.linkController.seekTo(Duration.zero);
            Controller.to.linkController.pause();
          }).then((value) {
            linkCopy();
          })
        : linkCopy();
  }

  RewardFilePopup() async {
    SharedPreferences pref_file = await SharedPreferences.getInstance();
    Controller.to.fileVideo_Status.value =
        pref_file.getBool('file_video_status') ?? false;
    Future.delayed(
      const Duration(seconds: 0),
      () {
        Controller.to.isOpenFileDialog.value = false;
      },
    );
    if (apiModel!.adsSequence!.isNotEmpty) {
      if (apiModel!.adSetting!.appVersionCode !=
          int.parse(AppDetailsPP.to.buildNumber.value)) {
        RewardDialog.show(context,
            rewardMessage: apiModel!.rewardDialog!.rewardMessage!, onTap: () {
          Navigator.pop(context);
          showRewardAds(callback: () {
            Controller.to.fileVideo_Status.value == false
                ? FileGuide.fileVideoGuide(context, onTap: () {
                    Get.back();
                    Controller.to.fileController.seekTo(Duration.zero);
                    Controller.to.fileController.pause();
                  }).then((value) {
                    fileDownloader();
                  })
                : fileDownloader();
          }, onFailedCallback: () {
            showIntraAds(callback: () {
              Controller.to.fileVideo_Status.value == false
                  ? FileGuide.fileVideoGuide(context, onTap: () {
                      Get.back();
                      Controller.to.fileController.seekTo(Duration.zero);
                      Controller.to.fileController.pause();
                    }).then((value) {
                      fileDownloader();
                    })
                  : fileDownloader();
            });
          });
        });
        return;
      }
    }
    Controller.to.fileVideo_Status.value == false
        ? FileGuide.fileVideoGuide(context, onTap: () {
            Get.back();
            Controller.to.fileController.seekTo(Duration.zero);
            Controller.to.fileController.pause();
          }).then((value) {
            fileDownloader();
          })
        : fileDownloader();
  }

  RewardQrCodePopup() async {
    SharedPreferences pref_qr = await SharedPreferences.getInstance();
    Controller.to.qrcodeVideo_Status.value =
        pref_qr.getBool('qrcode_video_status') ?? false;
    Future.delayed(
      const Duration(seconds: 0),
      () {
        Controller.to.isOPenQrCodeDialog.value = false;
      },
    );
    if (apiModel!.adsSequence!.isNotEmpty) {
      if (apiModel!.adSetting!.appVersionCode !=
          int.parse(AppDetailsPP.to.buildNumber.value)) {
        RewardDialog.show(context,
            rewardMessage: apiModel!.rewardDialog!.rewardMessage!, onTap: () {
          Navigator.pop(context);
          showRewardAds(callback: () {
            Controller.to.qrcodeVideo_Status.value == false
                ? QRCodeGuide.qrcodeVideoGuide(context, closeVideo: () {
                    Get.back();
                    Controller.to.qrcodeController.seekTo(Duration.zero);
                    Controller.to.qrcodeController.pause();
                  }).then((value) {
                    qrDownloader();
                  })
                : qrDownloader();
          }, onFailedCallback: () {
            showIntraAds(callback: () {
              Controller.to.qrcodeVideo_Status.value == false
                  ? QRCodeGuide.qrcodeVideoGuide(context, closeVideo: () {
                      Get.back();
                      Controller.to.qrcodeController.seekTo(Duration.zero);
                      Controller.to.qrcodeController.pause();
                    }).then((value) {
                      qrDownloader();
                    })
                  : qrDownloader();
            });
          });
        });
        return;
      }
    }
    Controller.to.qrcodeVideo_Status.value == false
        ? QRCodeGuide.qrcodeVideoGuide(context, closeVideo: () {
            Get.back();
            Controller.to.qrcodeController.seekTo(Duration.zero);
            Controller.to.qrcodeController.pause();
          }).then((value) {
            qrDownloader();
          })
        : qrDownloader();
  }

  Future downloadFile() async {
    pathhh.create();
    Controller.to.dynamicFileName.value =
        '${widget.name.replaceAll(" ", "")}.(${DateTime.now().millisecondsSinceEpoch}).m3u';
    Dio dio = Dio();

    await dio
        .download(widget.link,
            "${pathhh.path}/${Controller.to.dynamicFileName.value}")
        .then((value) {
      Controller.to.isFileDownloading.value = false;
      Controller.to.DownloadFileStatus.value = value.statusCode!;
    });
    /* await FlutterDownloader.enqueue(
       url: widget.link,
       savedDir:
           '/storage/emulated/0/Download/${appName.replaceAll(" ", "")}',
       fileName: Controller.to.dynamicTitle.value,
       saveInPublicStorage: false,
       showNotification: false,
       openFileFromNotification: false);*/
  }

  Future takeScreenshot() async {
    pathhh.create();
    Controller.to.dynamicImageName.value =
        '${widget.name.replaceAll(" ", "")}.(${DateTime.now().millisecondsSinceEpoch}).png';
    Controller.to.isQrCodeDownloading.value = false;
    await Controller.to.screenshotController.captureAndSave(pathhh.path,
        fileName: Controller.to.dynamicImageName.value);
  }

  linkCopy() {
    FlutterClipboard.copy('${widget.link}').then((value) {
      toastMessage(context, 'Copy Successfully');
      afterAdsDialog(context,
          title: 'Link',
          imageWidget: Image.asset(
            "assets/link_group.png",
            height: 60.h,
          ),
          titleNote: "To Watch HD videos, Paste link on IPTV player",
          titleGuideTap: () {
        Controller.to.LinkVideoEnded.value = false;
        LinkGuide.linkVideoGuide(
          context,
          onTap: () {
            Get.back();
            Controller.to.linkController.seekTo(Duration.zero);
            Controller.to.linkController.pause();
          },
        );
      });
    });
  }

  fileDownloader() {
    LoaderDialog(context);
    Future.delayed(
      const Duration(milliseconds: 400),
      () {
        downloadFile().then((value) {
          if (Controller.to.isFileDownloading.value == false) {
            Get.back();
            Controller.to.isFileDownloading.value = true;
          }
          toastMessage(context, 'Download Successfully');
          afterAdsDialog(context,
              title: 'File',
              imageWidget: Image.asset(
                "assets/file_group.png",
                height: 60.h,
              ),
              titleNote: "To Watch HD videos , Open File on IPTV player",
              titleGuideTap: () {
            Controller.to.FileVideoEnded.value = false;
            FileGuide.fileVideoGuide(context, onTap: () {
              Get.back();
              Controller.to.fileController.seekTo(Duration.zero);
              Controller.to.fileController.pause();
            });
          });

          /// todo notification
          if (Controller.to.DownloadFileStatus.value == 200) {
            AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
              if (!isAllowed) {
                AwesomeNotifications().requestPermissionToSendNotifications();
              } else {
                AwesomeNotifications().createNotification(
                    content: NotificationContent(
                        id: 10,
                        color: const Color(0xff046689),
                        groupKey: 'file',
                        channelKey: 'file_download',
                        title: '${widget.name}.m3u',
                        body: 'Download Complete',
                        largeIcon: 'asset://assets/appLogo.png'),
                    actionButtons: [
                      NotificationActionButton(key: 'Open', label: 'Open')
                    ]);
              }
            });
          }
        });
      },
    );
  }

  qrDownloader() {
    LoaderDialog(context);
    Future.delayed(
      const Duration(milliseconds: 500),
      () {
        takeScreenshot().then((value) async {
          if (Controller.to.isQrCodeDownloading.value == false) {
            Get.back();
            Controller.to.isQrCodeDownloading.value = true;
          }
          toastMessage(context, 'Download Successfully');
          afterAdsDialog(context,
              title: "QR Code",
              imageWidget: Center(
                child: QrImage(
                  data: "${widget.link}",
                  version: QrVersions.auto,
                  size: 170.0,
                ),
              ),
              titleNote: "To Watch HD Videos , Scan QR Code in IPTV Player",
              titleGuideTap: () {
            QRCodeGuide.qrcodeVideoGuide(context, closeVideo: () {
              Get.back();
              Controller.to.qrcodeController.seekTo(Duration.zero);
              Controller.to.qrcodeController.pause();
            });
          });

          // todo notification
          bool isAllowed = await AwesomeNotifications().isNotificationAllowed();
          if (!isAllowed) {
            AwesomeNotifications().requestPermissionToSendNotifications();
          } else {
            AwesomeNotifications().createNotification(
                content: NotificationContent(
                    id: 20,
                    color: const Color(0xff046689),
                    groupKey: 'qrcode',
                    channelKey: 'qrcode_save',
                    title: '${widget.name}.png',
                    body: 'Download complete',
                    largeIcon: 'asset://assets/appLogo.png'),
                actionButtons: [
                  NotificationActionButton(key: 'open', label: 'Open'),
                  NotificationActionButton(key: 'view', label: 'View'),
                ]);
          }
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Controller.to.linkController.seekTo(Duration.zero);
        Controller.to.linkController.pause();
        Controller.to.fileController.seekTo(Duration.zero);
        Controller.to.fileController.pause();
        Controller.to.qrcodeController.seekTo(Duration.zero);
        Controller.to.qrcodeController.pause();
        Get.back();
        return false;
      },
      child: Stack(
        children: [
          Center(
            child: RepaintBoundary(
              key: gk,
              child: Screenshot(
                  controller: Controller.to.screenshotController,
                  child: Container(
                    color: Colors.white,
                    child: QrImage(
                      data: '${widget.link}',
                      version: QrVersions.auto,
                    ),
                  )),
            ),
          ),
          Scaffold(
            body: Container(
              height: 1000.h,
              width: 1000.w,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                colorGradient1,
                colorGradient2.withOpacity(0.5),
                colorGradient1.withOpacity(0.8)
              ], begin: Alignment.topCenter)),
              child: Scaffold(
                appBar: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  centerTitle: true,
                  title: Text("${widget.name ?? ""} IPTV"),
                  actions: [
                    apiModel!.adsSequence!.isNotEmpty
                        ? apiModel!.adSetting!.appVersionCode !=
                                int.parse(AppDetailsPP.to.buildNumber.value)
                            ? buyButton ?? Container(height: 0)
                            : SizedBox(height: 0.h, width: 0.w)
                        : SizedBox(height: 0.h, width: 0.w)
                  ],
                ),
                backgroundColor: Colors.transparent,
                body: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Spacer(),
                      _smallNativeShow1,
                      const Spacer(),
                      SizedBox(
                        height: 20.h,
                      ),
                      // todo link
                      InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () async {
                            RewardLinkPopup();
                          },
                          child: container("Copy Link", "Group 419 (3).png")),
                      SizedBox(
                        height: 15.h,
                      ),
                      // todo file
                      InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () async {
                            if (Controller.to.isOpenFileDialog.value == false) {
                              Controller.to.isOpenFileDialog.value = true;
                              if (Platform.isAndroid) {
                                final androidInfo =
                                    await DeviceInfoPlugin().androidInfo;
                                final sdkInt = androidInfo.version.sdkInt;
                                if (33 <= sdkInt) {
                                  RewardFilePopup();
                                } else {
                                  await Permission.storage
                                      .request()
                                      .then((value) {
                                    if (value.isGranted) {
                                      RewardFilePopup();
                                    } else if (value.isPermanentlyDenied) {
                                      OpenSettingDialog(context);
                                      Controller.to.isOpenFileDialog.value =
                                          false;
                                    } else {
                                      Controller.to.isOpenFileDialog.value =
                                          false;
                                    }
                                  });
                                }
                              }
                            }
                          },
                          child: container("Get File", "Group 419.png")),
                      SizedBox(
                        height: 15.h,
                      ),
                      // todo QR
                      InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () async {
                            if (Controller.to.isOPenQrCodeDialog.value ==
                                false) {
                              Controller.to.isOPenQrCodeDialog.value = true;
                              if (Platform.isAndroid) {
                                final androidInfo =
                                    await DeviceInfoPlugin().androidInfo;
                                final sdkInt = androidInfo.version.sdkInt;
                                if (33 <= sdkInt) {
                                  RewardQrCodePopup();
                                } else {
                                  await Permission.storage
                                      .request()
                                      .then((value) {
                                    if (value.isGranted) {
                                      RewardQrCodePopup();
                                    } else if (value.isPermanentlyDenied) {
                                      OpenSettingDialog(context);
                                      Controller.to.isOPenQrCodeDialog.value =
                                          false;
                                    } else {
                                      Controller.to.isOPenQrCodeDialog.value =
                                          false;
                                    }
                                  });
                                }
                              }
                            }
                          },
                          child: container("Get QR Code", "Group 419 (4).png")),
                      SizedBox(
                        height: 20.h,
                      ),
                      const Spacer(),
                      apiModel!.adSetting!.isFullAds == true
                          ? _smallNativeShow2
                          : Container(
                              height: 0,
                            ),
                      const Spacer(),
                      SizedBox(
                        height: 20.h,
                      ),
                    ]),
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              child: _bannerShow,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      if (Controller.to.fileController.value.isPlaying) {
        Controller.to.fileController.pause();
      } else if (Controller.to.linkController.value.isPlaying) {
        Controller.to.linkController.pause();
      } else if (Controller.to.qrcodeController.value.isPlaying) {
        Controller.to.qrcodeController.pause();
      }
    } else if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      if (Controller.to.fileController.value.position != Duration.zero) {
        Controller.to.fileController.play();
      } else if (Controller.to.linkController.value.position != Duration.zero) {
        Controller.to.linkController.pause();
      } else if (Controller.to.qrcodeController.value.position !=
          Duration.zero) {
        Controller.to.qrcodeController.play();
      }
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    Controller.to.fileController.dispose();
    Controller.to.linkController.dispose();
    Controller.to.qrcodeController.dispose();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () {

          if(Controller.to.inAppPurchaseTap.value == false){
            Controller.to.inAppPurchaseTap.value= true;
            Future.delayed(const Duration(milliseconds: 500),() async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            },);
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset('assets/remove_ads.png'),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }

  @pragma('vm:entry-point')
  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort? send =
        IsolateNameServer.lookupPortByName('downloader_send_port');
    send?.send([id, status, progress]);
  }
}

// todo guide widget
/*Widget guideWidget(VoidCallback onTap) {
  return InkWell(
    onTap: onTap,
    child: Container(
      height: 58.h,
      alignment: Alignment.center,
      // width: 10.w,
      padding: EdgeInsets.all(1.5.h),
      decoration: BoxDecoration(
          color: const Color(0xffFFFFFF).withOpacity(0.4),
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Padding(
        padding: EdgeInsets.only(left: 15.w, right: 15.w),
        child: Text(
          "Guide",
          style: TextStyle(
              color: Colors.white,
              fontSize: 20.w,
              fontWeight: FontWeight.w500),
        ),
      ),
    ),
  );
}*/
