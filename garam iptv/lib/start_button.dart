// ignore_for_file: import_of_legacy_library_into_null_safe, prefer_typing_uninitialized_variables, non_constant_identifier_names

import 'package:animate_do/animate_do.dart';
import 'package:btc_purchase/PurchaseListener.dart';
import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:dialog_pp_flutter/get_controller.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/dialog/data_fill_dialog.dart';
import 'package:lottie/lottie.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api/all_json_api/provider.dart';
import 'dialog/getx_controller_common.dart';
import 'home_screen.dart';
import 'main.dart';

const String feature1 = 'feature1',
    feature2 = 'feature2',
    feature3 = 'feature3';

class StartButtonScreen extends StatefulWidget {
  const StartButtonScreen({Key? key}) : super(key: key);

  @override
  State<StartButtonScreen> createState() => _StartButtonScreenState();
}

class _StartButtonScreenState extends State<StartButtonScreen>
    with WidgetsBindingObserver
    implements PurchaseListener {
  Widget _bannerShow = Container(
    height: 0,
  );
  Widget smallNativeShow1 = Container(
    height: 0,
  );
  Widget smallNativeShow2 = Container(
    height: 0,
  );
  Widget? smallNativeShow3 = Container(
    height: 0,
  );

  /// in app purchase data ///
  Widget? buyButton;
  var isAlreadyPurchased;
  String? fetchPrize;

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   FeatureDiscovery.discoverFeatures(
    //     context,
    //     const <String>{feature1,feature2,feature3},
    //   );
    // });
    WidgetsBinding.instance.addObserver(this);
    restoreSharedPreferenceValue();
    privacyPolicyDialogNew(context).then((value) {
      Future.delayed(
        const Duration(seconds: 0),
        () {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            FeatureDiscovery.discoverFeatures(
              context,
              const <String>{feature1, feature2, feature3},
            );
          });
        },
      );
    });
    Future.delayed(Duration.zero, () {
      initPlatformState();
      updateBuyRemoveAdsButton();
      fetchOldPurchaseDetails();
      showBannerAds(
          size: AdSize.banner,
          onAdLoadedCallback: (p0) {
            _bannerShow = p0;
            Controller.to.bannerAd.value = true;
          });
      showMediumNativeAds(onAdLoadedCallback: (a01) {
        smallNativeShow1 = a01;
        Controller.to.smallNativeAds1.value = true;
      });
      showMediumNativeAds(onAdLoadedCallback: (a) {
        smallNativeShow2 = a;
        Controller.to.smallNativeAds2.value = true;
      });
      showMediumNativeAds(onAdLoadedCallback: (b) {
        smallNativeShow3 = b;
        Controller.to.smallNativeAds3.value = true;
      });
    });
  }

  restoreSharedPreferenceValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Controller.to.isRateAppGuideFirstTime.value =
        prefs.getBool('isFirstRate') ?? false;
    Controller.to.isInAppPurchaseGuideFirstTime.value =
        prefs.getBool('isFirstInApp') ?? false;
    Controller.to.isDrawerGuideFirstTime.value =
        prefs.getBool('isFirstDrawer') ?? false;
    Controller.to.privacyCheck1.value = prefs.getBool('privacy') ?? false;
    Controller.to.privacyCheck2.value = prefs.getBool('terms') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

    return WillPopScope(
      onWillPop: () {
        if (scaffoldKey.currentState!.isDrawerOpen) {
          scaffoldKey.currentState!.closeDrawer();
        } else {
          if (Controller.to.isDrawerGuideFirstTime.value == true &&
              Controller.to.isRateAppGuideFirstTime.value == true &&
              Controller.to.isInAppPurchaseGuideFirstTime.value == true) {
            PPDialog.exitDialog(
                appLogo: appLogo,
                mediumNative: Obx(() =>
                    Controller.to.smallNativeAds1.value == false
                        ? Container(height: 0)
                        : SizedBox(child: smallNativeShow1)));
          } else {
            if (Controller.to.isDrawerGuideFirstTime.value == true &&
                Controller.to.isRateAppGuideFirstTime.value == true) {
              if (apiModel!.adsSequence!.isEmpty ||
                  apiModel!.adSetting!.appVersionCode ==
                      int.parse(AppDetailsPP.to.buildNumber.value)) {
                PPDialog.exitDialog(
                    appLogo: appLogo,
                    mediumNative: Obx(() =>
                        Controller.to.smallNativeAds1.value == false
                            ? Container(height: 0)
                            : SizedBox(child: smallNativeShow1)));
              }
            }
          }
        }
        return Future.value(false);
      },
      child: Stack(
        children: [
          Container(
            height: 1000.h,
            width: 1000.w,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              colorGradient1,
              colorGradient2,
            ], begin: Alignment.topCenter)),
          ),
          Scaffold(
            key: scaffoldKey,
            backgroundColor: Colors.transparent,
            appBar: AppBar(
                backgroundColor: Colors.transparent,
                centerTitle: true,
                leading: DescribedFeatureOverlay(
                  featureId: Controller.to.isDrawerGuideFirstTime.value == false
                      ? feature1
                      : '',
                  tapTarget: const Icon(Icons.menu),
                  backgroundColor:
                      colorGradient2.withRed(0x2DFF00FF).withBlue(0x9e9e9eff),
                  title: const Text('Drawer'),
                  description: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const <Widget>[
                      Text('\n-  Privacy Policy'),
                      Text('-  Feedback support'),
                      Text('-  About the App'),
                      Text('-  More apps'),
                    ],
                  ),
                  child: GestureDetector(
                      onTap: () {
                        scaffoldKey.currentState!.openDrawer();
                      },
                      child: const Icon(Icons.menu)),
                  onComplete: () async {
                    Controller.to.isDrawerGuideFirstTime.value = true;
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    prefs.setBool('isFirstDrawer',
                        Controller.to.isDrawerGuideFirstTime.value);

                    return Future.value(true);
                  },
                  barrierDismissible: false,
                ),
                title: Text(
                  appName,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 19.sp,
                    fontWeight: FontWeight.bold,
                    shadows: const <Shadow>[
                      Shadow(
                        offset: Offset(2.0, 2.0),
                        blurRadius: 6.0,
                        color: Colors.redAccent,
                      ),
                    ],
                  ),
                ),
                actions: [
                  DescribedFeatureOverlay(
                    featureId:
                        Controller.to.isRateAppGuideFirstTime.value == false
                            ? feature2
                            : '',
                    tapTarget: Lottie.asset(
                      "assets/star_animation.json",
                    ),
                    backgroundColor:
                        colorGradient2.withRed(0x2DFF00FF).withBlue(0x9e9e9eff),
                    title: const Text('Rate App'),
                    description: const Text('\n-  Rate our app.'),
                    onComplete: () async {
                      Controller.to.isRateAppGuideFirstTime.value = true;
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool('isFirstRate',
                          Controller.to.isRateAppGuideFirstTime.value);

                      return true;
                    },
                    barrierDismissible: false,
                    child: InkWell(
                      onTap: () {
                        PPDialog.rateDialogCustom(context, logoApp: appLogo);
                      },
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      child: Lottie.asset(
                        "assets/star_animation.json",
                      ),
                    ),
                  ),
                  apiModel!.adsSequence!.isNotEmpty
                      ? apiModel!.adSetting!.appVersionCode !=
                              int.parse(AppDetailsPP.to.buildNumber.value)
                          ? DescribedFeatureOverlay(
                              featureId: Controller
                                          .to
                                          .isInAppPurchaseGuideFirstTime
                                          .value ==
                                      false
                                  ? feature3
                                  : '',
                              tapTarget: Swing(
                                infinite: true,
                                child: Image.asset(
                                  'assets/remove_ads.png',
                                  height: 30.h,
                                ),
                              ),
                              backgroundColor: colorGradient2
                                  .withRed(0x2DFF00FF)
                                  .withBlue(0x9e9e9eff),
                              title: const Text('Remove Ads'),
                              // enablePulsingAnimation: true,
                              // overflowMode: OverflowMode.wrapBackground,
                              description:
                                  const Text('\n-  Remove ads from app'),
                              onComplete: () async {
                                Controller.to.isInAppPurchaseGuideFirstTime
                                    .value = true;
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.setBool(
                                    'isFirstInApp',
                                    Controller
                                        .to.isRateAppGuideFirstTime.value);

                                return true;
                              },
                              barrierDismissible: false,
                              child: buyButton ?? Container(height: 0),
                            )
                          : SizedBox(height: 0.h, width: 0.w)
                      : SizedBox(height: 0.h, width: 0.w),
                ],
                elevation: 0),
            drawer: PPDialog.drawerScreen(
                appLogo: appLogo,
                privacyPolicy: "${apiModel!.privacyPolicy!.privacyPolicy}",
                feedbackEmailId: "${apiModel!.feedbackSupport!.emailId}",
                emailSubject: "${apiModel!.feedbackSupport!.emailSubject}",
                emailMessage: "${apiModel!.feedbackSupport!.emailMessage}",
                apiAppVersionCode: apiModel!.adSetting!.appVersionCode!),
            bottomNavigationBar: Obx(() => Controller.to.bannerAd.value == false
                ? Container(height: 0)
                : BottomAppBar(
                    child: _bannerShow,
                  )),
            body: SafeArea(
              child: Column(
                children: [
                  SizedBox(
                    height: 30.h,
                  ),
                  Obx(() => Controller.to.smallNativeAds3.value == false
                      ? Container(height: 0)
                      : SizedBox(
                          child: smallNativeShow3,
                        )),
                  const Spacer(),
                  InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        if (apiModel!.adSetting!.isFullAds == true) {
                            showIntraAds(
                                callback: () => {
                                      Get.to(const HomeScreen())
                                    });
                        } else {
                          Get.to(const HomeScreen());
                        }
                      },
                      child: Container(
                        width: 1000.w,
                        margin: EdgeInsets.only(left: 30.w, right: 30.w),
                        padding: EdgeInsets.all(10.h),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.5),
                              offset: const Offset(0, 10),
                              blurRadius: 20.0,
                            ),
                          ],
                          // border: Border.all(),
                          color: Colors.blue,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "Continue",
                          style: TextStyle(color: Colors.white, fontSize: 22.w),
                        ),
                      )),
                  apiModel!.adSetting!.isFullAds == true
                      ? apiModel!.adSetting!.appVersionCode !=
                              int.parse(AppDetailsPP.to.buildNumber.value)
                          ? const Spacer()
                          : Container(height: 0)
                      : Container(height: 0),
                  apiModel!.adSetting!.isFullAds == true
                      ? SizedBox(
                          child: Obx(() =>
                              Controller.to.smallNativeAds2.value == false
                                  ? Container(height: 0)
                                  : smallNativeShow2),
                        )
                      : Container(
                          height: 0,
                        ),
                  SizedBox(
                    height: 30.h,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Controller.to.inAppPurchaseTap.value = false;
      Controller.to.btcPurchasePlugin.PurchaseSync();
      Controller.to.btcPurchasePlugin
          .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this)
          .then((value) => {
                if (value == Controller.to.purchaseKey.value)
                  {
                    setState(() {
                      Controller.to.isPurchased.value = true;
                    })
                  }
              });
    }
  }

  Future<void> initPlatformState() async {
    fetchPrize = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    isAlreadyPurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    if (fetchPrize?.isNotEmpty == true) {
      setState(() {
        if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
          Controller.to.isPurchased.value = true;
        }
        updateBuyRemoveAdsButton();
      });
    }
    if (!mounted) return;
  }

  Widget? updateBuyRemoveAdsButton() {
    if (Controller.to.isPurchased.value) {
      buyButton = Container();
      return buyButton;
    } else {
      buyButton = GestureDetector(
        onTap: () {
          if (Controller.to.inAppPurchaseTap.value == false) {
            Controller.to.inAppPurchaseTap.value = true;
            Future.delayed(const Duration(milliseconds: 500), () async {
              await Controller.to.btcPurchasePlugin
                  .launchPurchase(Controller.to.purchaseKey.value);
            });
          }

          /* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/
        },
        child: Padding(
            padding: EdgeInsets.all(10.r),
            child: Swing(
              infinite: true,
              child: Image.asset('assets/remove_ads.png'),
            )),
      );
      return buyButton;
    }
  }

  fetchOldPurchaseDetails() async {
    var purchasePrice = await Controller.to.btcPurchasePlugin
        .getPurchaseList(Controller.to.purchaseKey.value, this);
    var alreadypurchased = await Controller.to.btcPurchasePlugin
        .getAlreadyPurchasedList(Controller.to.purchaseKey.value, this);

    setState(() {
      fetchPrize = purchasePrice;
      isAlreadyPurchased = alreadypurchased;

      if (isAlreadyPurchased == Controller.to.purchaseKey.value) {
        Controller.to.isPurchased.value = true;
      }
      updateBuyRemoveAdsButton();
    });
  }

  @override
  void OnPurchaseListener() {
    setState(() {
      Controller.to.isPurchased.value = true;
    });
    updateBuyRemoveAdsButton();
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        Restart.restartApp();
      },
    );
  }
}
