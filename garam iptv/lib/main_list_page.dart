// import 'package:animate_do/animate_do.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:get/get.dart';
// import 'package:iptv/dialog/data_fill_dialog.dart';
// import 'package:iptv/home_screen.dart';
// import 'package:loading_gifs/loading_gifs.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// import 'about_dialog.dart';
// import 'ads/ads_pakage.dart';
// import 'api/all_json_api/provider.dart';
// import 'main.dart';
//
// class MainListPage extends StatefulWidget {
//   const MainListPage({Key? key}) : super(key: key);
//
//   @override
//   State<MainListPage> createState() => _MainListPageState();
// }
//
// class _MainListPageState extends State<MainListPage> {
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     Future.delayed(const Duration(seconds: 1), () {
//       // updateDialog();
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       bottomNavigationBar: bannerAds(),
//       extendBodyBehindAppBar: true,
//       body: Container(
//         height: 1000.h,
//         width: 1000.w,
//         decoration: BoxDecoration(
//             gradient: LinearGradient(
//                 colors: [colorGradient1, colorGradient2],
//                 begin: Alignment.topCenter)),
//         child: SafeArea(
//           child: Column(
//             children: [
//               SizedBox(
//                 height: 20.h,
//               ),
//               Row(
//                 children: [
//                   SizedBox(
//                     width: 10.w,
//                   ),
//                   InkWell(
//                     child: Text(
//                       appName,
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 22.w,
//                         fontWeight: FontWeight.bold,
//                         shadows: const <Shadow>[
//                           Shadow(
//                             offset: Offset(2.0, 2.0),
//                             blurRadius: 6.0,
//                             color: Colors.redAccent,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   Spacer(),
//                   PopupMenuButton(
//                     // color: Colors.blueAccent,
//                     shape: const RoundedRectangleBorder(
//                       borderRadius: BorderRadius.all(
//                         Radius.circular(20.0),
//                       ),
//                     ),
//                     child: const Icon(
//                       Icons.more_vert,
//                       color: Colors.white,
//                     ),
//                     itemBuilder: (ctx) => [
//                       _buildPopupMenuItem('Privacy Policy', () {
//                         launch(apiModel!
//                             .privacyPolicy.privacyPolicy);
//                       }, ""),
//                       _buildPopupMenuItem('Rate us', () {
//                         Future.delayed(const Duration(seconds: 0), () {
//                           RatingDialogNew(context);
//                         });
//                       }, ""),
//                       _buildPopupMenuItem('More App', () {
//                         launch(apiModel!.moreAppUrl);
//                       }, "  (Ad)"),
//                       _buildPopupMenuItem('Terms & Conditions', () {
//                         launch(apiModel!
//                             .termsOfUse.termsOfUse);
//                       }, ""),
//                       _buildPopupMenuItem('Feedback', () {
//                         launch(
//                             "${apiModel!.feedbackSupport.emailId}?subject=${apiModel!.feedbackSupport.emailSubject}&body=${apiModel!.feedbackSupport.emailMessage}");
//                       }, ""),
//                       _buildPopupMenuItem('About Us', () {
//                         showIntraAds(callback: () {
//                           Future.delayed(const Duration(seconds: 0), () {
//                             showAboutUs(context);
//                           });
//                         });
//                       }, ""),
//                       _buildPopupMenuItem('Exit', () {
//                         Future.delayed(const Duration(seconds: 0), () {
//                           ExitUsDialogNew(context);
//                         });
//                       }, ""),
//                     ],
//                   ),
//                   SizedBox(
//                     width: 30.w,
//                   )
//                 ],
//               ),
//               SizedBox(
//                 height: 20.h,
//               ),
//               Expanded(
//                   child: ListView.builder(
//                 itemCount: channelApiList!.records.length,
//                 // shrinkWrap: true,
//                 itemBuilder: (context, index) {
//                   return Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     mainAxisSize: MainAxisSize.min,
//                     children: [
//                       index %
//                                       apiModel!.adSetting
//                                           .nativeAdListInterval ==
//                                   0 &&
//                               index > 0
//                           ? Container(
//                               child: FadeInRight(child: mediumNativeAds()),
//                               decoration: const BoxDecoration(
//                                   borderRadius:
//                                       BorderRadius.all(Radius.circular(18))),
//                               margin: EdgeInsets.all(1.h),
//                             )
//                           : Container(),
//                       FadeInRight(
//                         child: InkWell(
//                           splashColor: Colors.transparent,
//                           hoverColor: Colors.transparent,
//                           highlightColor: Colors.transparent,
//                           onTap: () {},
//                           child: InkWell(
//                             onTap: () {
//                               showIntraAds();
//                               Get.to(HomeScreen(
//                                   channelApiList!.records[index].subLinks,
//                                   channelApiList!.records[index].countryName,
//                                   channelApiList!
//                                       .records[index].countryImageUrl));
//                             },
//                             // child: Container(
//                             //   padding: EdgeInsets.all(10.h),
//                             //   margin: EdgeInsets.all(10.h),
//                             //   child: Row(
//                             //     children: [
//                             //       SizedBox(
//                             //         child: ClipRRect(
//                             //           borderRadius: BorderRadius.circular(10.0),
//                             //           child: FadeInImage.assetNetwork(
//                             //               fit: BoxFit.fill,
//                             //               placeholder:
//                             //                   cupertinoActivityIndicator,
//                             //               image: channelApiList!
//                             //                   .records[index].countryImageUrl,
//                             //               placeholderScale: 5),
//                             //         ),
//                             //         height: 50.h,
//                             //         width: 80.w,
//                             //       ),
//                             //       SizedBox(
//                             //         width: 10.w,
//                             //       ),
//                             //       Text(
//                             //         channelApiList!.records[index].countryName,
//                             //         style: TextStyle(
//                             //             color: Colors.black,
//                             //             fontWeight: FontWeight.bold,
//                             //             fontSize: 15.w),
//                             //       ),
//                             //       const Spacer(),
//                             //       SizedBox(
//                             //         width: 31.w,
//                             //       ),
//                             //       const Icon(Icons.arrow_forward_ios),
//                             //       SizedBox(
//                             //         width: 3.w,
//                             //       ),
//                             //     ],
//                             //   ),
//                             //   decoration: BoxDecoration(
//                             //       color: Colors.white.withOpacity(0.9),
//                             //       borderRadius: const BorderRadius.all(
//                             //           Radius.circular(18))),
//                             // ),
//                           ),
//                         ),
//                       ),
//                     ],
//                   );
//                 },
//               ))
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   PopupMenuItem _buildPopupMenuItem(
//       String title, VoidCallback? tapEvent, var ad) {
//     return PopupMenuItem(
//       onTap: tapEvent,
//       child: Row(children: [
//         Text(title),
//         Align(
//           alignment: Alignment.topRight,
//           child: Text(
//             "$ad",
//             style: TextStyle(fontSize: 3.2.w, color: Colors.grey),
//           ),
//         ),
//       ]),
//     );
//   }
// }
