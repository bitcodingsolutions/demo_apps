import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import '../../dialog/getx_controller_common.dart';

class LinkGuide {
  static loadLinkVideoInit() {
    Controller.to.linkController =
        VideoPlayerController.asset('assets/guide/video/guide_link.m4v')
          ..initialize();
  }

  static Future linkVideoGuide(BuildContext context,
      {required GestureTapCallback onTap}) {
    Controller.to.linkController.play();
    Controller.to.linkController.setPlaybackSpeed(1);
    Controller.to.LinkVideoEnded.value = false;
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.linkController.addListener(() async {
          SharedPreferences prefLink = await SharedPreferences.getInstance();
          Controller.to.linkVideo_Status.value =
              await prefLink.setBool('link_video_status', true);
          if (Controller.to.LinkVideoEnded.value == false) {
            if (Controller.to.linkController.value.position >=
                Controller.to.linkController.value.duration) {
              Controller.to.LinkVideoEnded.value = true;
              Get.back();
              Controller.to.linkController.seekTo(Duration.zero);
              Controller.to.linkController.pause();
            }
          }
        });

        return BackdropFilter(filter: ImageFilter.blur(sigmaY: 4,sigmaX: 4),child: WillPopScope(
          onWillPop: () {
            Get.back();
            Controller.to.linkController.seekTo(Duration.zero);
            Controller.to.linkController.pause();
            return Future.value(true);
          },
          child: Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0),
              ),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: AspectRatio(
                        aspectRatio:
                        Controller.to.linkController.value.aspectRatio,
                        child: VideoPlayer(Controller.to.linkController),
                      ),
                    ).marginOnly(top: 0.06.sh),
                    // todo close button
                    Positioned(
                      top: 0.015.sh,
                      right: 0,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: onTap,
                        child: Container(
                          padding: EdgeInsets.all(5.r),
                          height: 25.h,
                          width: 25.h,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.9)),
                              color: Colors.white.withOpacity(0.9)),
                          child: Image.asset("assets/close2.png",
                              color: Colors.black.withOpacity(0.8), height: 12),
                        ),
                      ),
                    ),
                    // todo guide image
                    Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            "assets/guide_image.png",
                            height: 90.h,
                          )),
                    ),
                  ],
                ),
              )),
        ),);
      },
    );
  }
}
