import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';

import '../../dialog/getx_controller_common.dart';

class FileGuide {

  static loadFileVideoInit() {
    Controller.to.fileController =
    VideoPlayerController.asset('assets/guide/video/guide_files.m4v')
      ..initialize();
  }

  static Future fileVideoGuide(BuildContext context,{required GestureTapCallback onTap}) {
    Controller.to.fileController.play();
    Controller.to.fileController.setPlaybackSpeed(1);
    Controller.to.FileVideoEnded.value = false;

    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.fileController.addListener(() async {
          SharedPreferences prefFile = await SharedPreferences.getInstance();
          Controller.to.fileVideo_Status.value =
          await prefFile.setBool('file_video_status', true);
          if(Controller.to.FileVideoEnded.value == false){
            if(Controller.to.fileController.value.position >= Controller.to.fileController.value.duration){
              Controller.to.FileVideoEnded.value = true;
              Get.back();
              Controller.to.fileController.seekTo(Duration.zero);
              Controller.to.fileController.pause();
            }
          }
        });

        return BackdropFilter(filter: ImageFilter.blur(sigmaY: 4,sigmaX: 4),
          child: WillPopScope(
            onWillPop: () {
              Get.back();
              Controller.to.fileController.seekTo(Duration.zero);
              Controller.to.fileController.pause();
              return Future.value(true);
            },
            child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Container(
                  margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: AspectRatio(
                          aspectRatio:
                          Controller.to.fileController.value.aspectRatio,
                          child: VideoPlayer(Controller.to.fileController),
                        ),
                      ).marginOnly(top: 0.06.sh),
                      // todo close button
                      Positioned(
                        top: 0.015.sh,
                        right: 0,
                        child: GestureDetector(
                          onTap: onTap,
                          child: Container(
                            padding: EdgeInsets.all(5.r),
                            height: 25.h,
                            width: 25.h,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Colors.black.withOpacity(0.9)),
                                color: Colors.white.withOpacity(0.9)),
                            child: Image.asset("assets/close2.png",
                                color: Colors.black.withOpacity(0.8), height: 12),
                          ),
                        ),
                      ),
                      // todo guide image
                      Positioned(
                        right: 0,
                        top: 0,
                        left: 0,
                        child: Align(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/guide_image.png",
                              height: 90.h,
                            )),
                      ),
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }

}