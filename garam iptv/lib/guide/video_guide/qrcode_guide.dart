import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import '../../dialog/getx_controller_common.dart';

class QRCodeGuide {
  static loadQRCodeVideoInit() {
    Controller.to.qrcodeController =
        VideoPlayerController.asset('assets/guide/video/guide_qr_code.m4v')
          ..initialize();
  }

  static Future qrcodeVideoGuide(BuildContext context,
      {required GestureTapCallback closeVideo}) {
    Controller.to.qrcodeController.play();
    Controller.to.qrcodeController.setPlaybackSpeed(1);
    Controller.to.QrCodeVideoEnded.value = false;

    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        Controller.to.qrcodeController.addListener(() async {
          SharedPreferences prefQrcode = await SharedPreferences.getInstance();
          Controller.to.qrcodeVideo_Status.value =
              await prefQrcode.setBool('qrcode_video_status', true);
          if (Controller.to.QrCodeVideoEnded.value == false) {
            if (Controller.to.qrcodeController.value.position >=
                Controller.to.qrcodeController.value.duration) {
              Controller.to.QrCodeVideoEnded.value = true;
              Get.back();
              Controller.to.qrcodeController.seekTo(Duration.zero);
              Controller.to.qrcodeController.pause();
            }
          }
        });
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
          child: WillPopScope(
            onWillPop: () {
              Get.back();
              Controller.to.qrcodeController.seekTo(Duration.zero);
              Controller.to.qrcodeController.pause();
              return Future.value(true);
            },
            child: Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0),
                ),
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Container(
                  margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                  child: Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: AspectRatio(
                          aspectRatio:
                              Controller.to.qrcodeController.value.aspectRatio,
                          child: VideoPlayer(Controller.to.qrcodeController),
                        ),
                      ).marginOnly(top: 0.06.sh),
                      // todo close button
                      Positioned(
                        top: 0.015.sh,
                        right: 0,
                        child: GestureDetector(
                          onTap: closeVideo,
                          child: Container(
                            padding: EdgeInsets.all(5.r),
                            height: 25.h,
                            width: 25.h,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Colors.black.withOpacity(0.9)),
                                color: Colors.white.withOpacity(0.9)),
                            child: Image.asset("assets/close2.png",
                                color: Colors.black.withOpacity(0.8),
                                height: 12),
                          ),
                        ),
                      ),
                      // todo guide image
                      Positioned(
                        right: 0,
                        top: 0,
                        left: 0,
                        child: Align(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/guide_image.png",
                              height: 90.h,
                            )),
                      ),
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }
}
