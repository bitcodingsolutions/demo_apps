import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:iptv/ads/ads_pakage.dart';

import '../../../api/all_json_api/provider.dart';
import 'file2.dart';

class FIleGuideScreen1 extends StatefulWidget {
  const FIleGuideScreen1({Key? key}) : super(key: key);

  @override
  State<FIleGuideScreen1> createState() => _FIleGuideScreen1State();
}

class _FIleGuideScreen1State extends AdsWidget<FIleGuideScreen1> {





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: apiModel!.adsSequence!.isNotEmpty
          ? BottomAppBar(
              child: bannerAds(),
            )
          : Container(
              height: 0,
            ),
      backgroundColor: const Color(0xff0B091C),
      body: Column(
        children: [
          SafeArea(
              child: Center(
            child: SizedBox(
                height: 500.h,
                width: 1000.w,
                child: Center(
                  child: Image.asset(
                    "assets/guide/images/file1.png",
                    fit: BoxFit.fill,
                  ),
                )),
          )),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  isFullAdss(callback: (){
                    Get.off(const fileGuideScreen2());
                  });

                },
                child: Container(
                  padding: EdgeInsets.all(10.h),
                  alignment: Alignment.center,
                  width: 100.w,
                  decoration: BoxDecoration(
                      border: Border.all(),
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                  child: Text(
                    "Next",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.w),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 25.h,
          )
        ],
      ),
    );
  }
}
