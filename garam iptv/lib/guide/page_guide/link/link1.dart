// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:iptv/ads/ads_pakage.dart';

import '../../../api/all_json_api/provider.dart';
import 'link2.dart';

class linkGuideScreen1 extends StatefulWidget {
  const linkGuideScreen1({Key? key}) : super(key: key);

  @override
  State<linkGuideScreen1> createState() => _linkGuideScreen1State();
}

class _linkGuideScreen1State extends AdsWidget<linkGuideScreen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: apiModel!.adsSequence!.isNotEmpty
          ? BottomAppBar(
              child: bannerAds(),
            )
          : Container(
              height: 0,
            ),
      backgroundColor: const Color(0xff0B091C),
      body: Column(
        children: [
          SafeArea(
              child: Center(
            child: SizedBox(
                height: 500.h,
                width: 1000.w,
                child: Center(
                  child: Image.asset(
                    "assets/guide/images/link1.png",
                    fit: BoxFit.fill,
                  ),
                )),
          )),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  isFullAdss(callback: () {
                    Get.off(const linkGuideScreen2());
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(10.h),
                  alignment: Alignment.center,
                  width: 100.w,
                  child: Text(
                    "Next",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18.w),
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(),
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                ),
              )
            ],
          ),
          SizedBox(
            height: 25.h,
          )
        ],
      ),
    );
  }
}
