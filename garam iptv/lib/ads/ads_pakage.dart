import 'package:flutter/material.dart';
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';
import 'package:iptv/api/all_json_api/provider.dart';

abstract class AdsWidget<T extends StatefulWidget> extends State<T> {
  Widget? _bannerShow = null;
  Widget? _smallNativeShow1 = null;
  Widget? _smallNativeShow2 = null;

  Widget? _fullAdsMediumnativeShow = null;

  Widget bannerAds() {
    if (_bannerShow == null) {
      _bannerShow = Container(height: 0);

      showBannerAds(onAdLoadedCallback: (p0) {
        setState(() {
          _bannerShow = p0;
        });
      });
    }

    return _bannerShow!;
  }

  Widget smallNativeAds1() {
    if (_smallNativeShow1 == null) {
      _smallNativeShow1 = Container(height: 0);

      showMediumNativeAds(
          onAdLoadedCallback: (a) => {
                setState(() {
                  _smallNativeShow1 = a;
                })
              });
    }

    return _smallNativeShow1!;
  }

  Widget smallNativeAds2() {
    if (_smallNativeShow2 == null) {
      _smallNativeShow2 = Container(height: 0);

      showMediumNativeAds(
          onAdLoadedCallback: (b) => {
                setState(() {
                  _smallNativeShow2 = b;
                })
              });
    }

    return _smallNativeShow2!;
  }

  Widget fullAdsMediumNativeAds() {
    if (_fullAdsMediumnativeShow == null) {
      _fullAdsMediumnativeShow = Container(height: 0);

      showFullMediumNativeAds(
          onAdLoadedCallback: (a) => {
                setState(() {
                  _fullAdsMediumnativeShow = a;
                })
              });
    }

    return _fullAdsMediumnativeShow!;
  }

  isFullAdss({Function? callback}) {
    if (apiModel!.adSetting!.isFullAds == true) {
      showIntraAds(callback: callback);
    } else {
      if (callback != null) {
        callback();
      }
    }
  }
}
