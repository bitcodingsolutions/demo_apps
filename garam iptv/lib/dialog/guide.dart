
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

guideIntro(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.h)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        top: 0.05.sh,
                      ),
                      margin:  EdgeInsets.only(top: 0.05.sh),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: const <BoxShadow>[
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 0.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Center(
                              child: Padding(
                                padding: EdgeInsets.all(15.h),
                                child: Text("Welcome!",
                                    style: TextStyle(
                                        fontSize: 20.w,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold)),
                              )),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            "Step 1",
                            style: TextStyle(
                                color: const Color(0xffee0fa1),
                                fontSize: 20.w,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          Text(
                            "Copy URL From Link button",
                            style:
                            TextStyle(color: Colors.grey, fontSize: 17.w),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            "Step 2",
                            style: TextStyle(
                                color: const Color(0xffff7b37),
                                fontSize: 20.w,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          Text(
                            "Paste URL in video player app",
                            style:
                            TextStyle(color: Colors.grey, fontSize: 17.w),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Text(
                            "Step 3",
                            style: TextStyle(
                                color: const Color(0xff000eff),
                                fontSize: 20.w,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          Text(
                            "Watch your favourite videos",
                            style:
                            TextStyle(color: Colors.grey, fontSize: 17.w),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 15.h,
                          ),
                          InkWell(
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              onTap: () async {
                                SharedPreferences pref =
                                await SharedPreferences.getInstance();
                                pref.setBool("guide", true);

                                Get.back();
                              },
                              child: Container(
                                height: 50.h,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 15.r, vertical: 15.r),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius:
                                    BorderRadius.circular(15.r)),
                                child: Text('UnderStood?',
                                    style: TextStyle(
                                        fontSize: 25.sp,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white)),
                              )),
                          SizedBox(
                            height: 2.h,
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      right: 0,
                      left: 0,
                      top: -5,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                            alignment: Alignment.center,
                            child: Image.asset(
                              'assets/play_button_icon.png',
                              height: 80.h,
                            )),
                      ),
                    ),
                  ],
                ),
              )),
        );
      },
      context: context);
}

// for in app purchase model bottom sheet
/* showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    ListTile(
                        title: Text(TextConfig.APP_TITLE,
                            style: TextStyle(
                              fontSize: 18.sp,
                            )),
                        subtitle: Text(
                          TextConfig.REMOVE_ADS_NOTE,
                          style: TextStyle(fontSize: 12.sp),
                        ),
                        trailing: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                            await Controller.to.btcPurchasePlugin
                                .launchPurchase(
                                    Controller.to.purchaseKey.value);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xff048c13),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    blurRadius: 1.0,
                                  ),
                                ]),
                            child: Text("$fetchPrize",
                                style: const TextStyle(
                                    fontSize: 15, color: Colors.white)),
                          ),
                        ))
                  ],
                ),
              );
            },
          );*/