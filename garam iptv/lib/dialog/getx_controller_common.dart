// ignore_for_file: non_constant_identifier_names

import 'package:btc_purchase/btc_purchase.dart';
import 'package:get/get.dart';
import 'package:screenshot/screenshot.dart';
import 'package:video_player/video_player.dart';

class Controller extends GetxController {
  static Controller to = Get.put(Controller());

  RxBool linkVideo_Status = false.obs;
  RxBool fileVideo_Status = false.obs;
  RxBool qrcodeVideo_Status = false.obs;

  /// in app purchase param
  RxString purchaseKey = 'remove_ads'.obs;
  final btcPurchasePlugin = BtcPurchase();
  RxBool isPurchased = false.obs;

  // video ended or not event
  RxBool FileVideoEnded = false.obs;
  RxBool LinkVideoEnded = false.obs;
  RxBool QrCodeVideoEnded = false.obs;


  // Video play controller
  late VideoPlayerController linkController;
  late VideoPlayerController fileController;
  late VideoPlayerController qrcodeController;

  // dynamic name generate
  RxString dynamicTitle = ''.obs;
  RxString dynamicImageName = ''.obs;
  RxString dynamicFileName = ''.obs;
  RxString dynamicLink = ''.obs;

  // download .m3u file status
  RxInt DownloadFileStatus = 0.obs;

  // qr code controller
  ScreenshotController screenshotController = ScreenshotController();

  // Dialog Controller
  RxBool isOpenFileDialog = false.obs;
  RxBool isOPenQrCodeDialog = false.obs;

  // Button Enable/Disable event
  RxBool isFileDownloading = true.obs;
  RxBool isQrCodeDownloading = true.obs;

// first time guide show controller
  RxBool isDrawerGuideFirstTime = false.obs;
  RxBool isRateAppGuideFirstTime = false.obs;
  RxBool isInAppPurchaseGuideFirstTime = false.obs;

// terms and condition controller
  RxBool privacyCheck2 = false.obs;
  RxBool privacyCheck1 = false.obs;

// for start screen native and banner ads loader
  RxBool bannerAd = false.obs;
  RxBool smallNativeAds1 = false.obs;
  RxBool smallNativeAds2 = false.obs;
  RxBool smallNativeAds3 = false.obs;

// inApp Purchase
  RxBool inAppPurchaseTap = false.obs;

// ads Controller
//   RxBool isAdsLoaded = false.obs;

}