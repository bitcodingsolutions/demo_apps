import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:iptv/start_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../api/all_json_api/provider.dart';
import 'getx_controller_common.dart';

updateDialogNew(BuildContext context) {
  return PPDialog.updateDialog(context,
      isPopup: apiModel!.appUpdate!.isPopupDialog!,
      apiVersionCode: apiModel!.appUpdate!.updatedVersionCode!,
      isUpdateRequired: apiModel!.appUpdate!.isUpdateRequire!,
      adSettingAppVersionCode: apiModel!.adSetting!.appVersionCode!,
      shuffle: apiModel!.appUpdate!.isShuffle!,
      clickCount: apiModel!.appUpdate!.changeDialogdataClickCount!);
}

termsConditions(BuildContext context,
    {required String privacyPolicyUrl,
    required String termsConditionUrl,
    required bool isPrivacyPolicy,
    required onTap}) async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  var checkTerms = pref.getBool("privacy") ?? false;
  // var checkTerms = false;

  if (isPrivacyPolicy == true) {
    if (checkTerms == false) {
      return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              String smallString = termsConditionUrl.substring(
                  0, 4); //<-- this string will be abcde
              return SimpleDialog(
                backgroundColor: Colors.transparent,
                insetPadding: EdgeInsets.zero,
                contentPadding: EdgeInsets.zero,
                children: [
                  Container(
                    margin: EdgeInsets.all(20.h),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          height: 55.h,
                          decoration: const BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(15),
                                  topLeft: Radius.circular(15))),
                          alignment: Alignment.center,
                          child: Text(
                            "Terms & Conditions",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 20.w),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.h),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(10.h),
                                  child: smallString != "http"
                                      ? Padding(
                                          padding: const EdgeInsets.all(1),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [Text(termsConditionUrl)],
                                          ),
                                        )
                                      : SizedBox(
                                          height: 350.h,
                                          child: Stack(
                                            children: [
                                              WebView(
                                                onPageFinished: (finish) {},
                                                javascriptMode:
                                                    JavascriptMode.unrestricted,
                                                initialUrl: termsConditionUrl,
                                              ),
                                            ],
                                          ),
                                        ),
                                ),
                                Row(
                                  children: [
                                    Obx(
                                      () => Checkbox(
                                        value:
                                            Controller.to.privacyCheck2.value,
                                        onChanged: (value) {
                                          Controller.to.privacyCheck2.value =
                                              value!;
                                        },
                                        activeColor: Colors.green,
                                      ),
                                    ),
                                    Text.rich(
                                      TextSpan(
                                        children: [
                                          TextSpan(
                                              text: '    Agree',
                                              style: TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.6))),
                                          const TextSpan(
                                              text:
                                                  ' Terms and Conditions    '),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Obx(
                                      () => Checkbox(
                                        activeColor: Colors.green,
                                        value:
                                            Controller.to.privacyCheck1.value,
                                        onChanged: (value) {
                                          Controller.to.privacyCheck1.value =
                                              value!;
                                        },
                                      ),
                                    ),
                                    InkWell(
                                        onTap: () {
                                          launch(privacyPolicyUrl);
                                        },
                                        child: Text.rich(
                                          TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: '    Agree',
                                                  style: TextStyle(
                                                      color: Colors.black
                                                          .withOpacity(0.6))),
                                              const TextSpan(
                                                  text: ' Privacy Policy    ',
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      color: Colors.blue)),
                                            ],
                                          ),
                                        ))
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 0.03.sw,
                                    ),
                                    InkWell(
                                        onTap: onTap,
                                        child: Obx(
                                          () => Container(
                                            padding: EdgeInsets.all(10.h),
                                            margin: EdgeInsets.only(
                                                left: 80.r,
                                                right: 80.r,
                                                top: 10.h,
                                                bottom: 10.h),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              color: Controller.to.privacyCheck1
                                                              .value ==
                                                          true &&
                                                      Controller
                                                              .to
                                                              .privacyCheck2
                                                              .value ==
                                                          true
                                                  ? Colors.green
                                                  : Colors.grey,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(10)),
                                            ),
                                            child: Text(
                                              "Agree",
                                              style: TextStyle(
                                                  fontSize: 21.r,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      width: 0.03.sw,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            }),
          );
        },
      );
    }
  }
}

Future privacyPolicyDialogNew(BuildContext context) {
  return termsConditions(context,
      privacyPolicyUrl: "${apiModel?.privacyPolicy!.privacyPolicy}",
      termsConditionUrl: "${apiModel?.termsOfUse!.termsOfUse}",
      isPrivacyPolicy: apiModel!.privacyPolicy!.isPrivacyPolicy!,
      onTap: () async {
    if (Controller.to.privacyCheck1.value == true &&
        Controller.to.privacyCheck2.value == true) {
      SharedPreferences pref = await SharedPreferences.getInstance();
      await pref.setBool("privacy", true);
      await pref.setBool("terms", true);
      // Controller.to.DataLoadStatus.value = true;
      Navigator.pop(context);
    }
  });
}

Widget container(String title, String imagePath) {
  return Container(
    height: 58.h,
    width: 0.65.sw,
    padding: EdgeInsets.all(10.h),
    decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10))),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 0.w,
        ),
        Image.asset(
          "assets/$imagePath",
          height: 56.h,
        ),
        SizedBox(
          width: 15.w,
        ),
        Text(
          title,
          style: TextStyle(fontSize: 20.w, fontWeight: FontWeight.w400),
        )
      ],
    ),
  );
}
