import 'package:dialog_pp_flutter/dialog_pp_flutter.dart';
import 'package:flutter/material.dart';
import '../main.dart';

aboutUsDialog(BuildContext context) async {
  return PPDialog.showAboutUs(context, appLogo);
}
