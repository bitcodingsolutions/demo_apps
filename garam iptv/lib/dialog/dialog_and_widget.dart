
// ignore_for_file: non_constant_identifier_names

import 'dart:ui';
import 'package:external_app_launcher/external_app_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../api/all_json_api/provider.dart';
import '../buttons.dart';

ToastFuture toastMessage(BuildContext context, String status) {
  return showToast(
    status,
    context: context,
    animation: StyledToastAnimation.fadeScale,
    reverseAnimation: StyledToastAnimation.fadeScale,
    position: StyledToastPosition.center,
    animDuration: const Duration(seconds: 1),
    duration: const Duration(seconds: 2),
    curve: Curves.elasticOut,
    reverseCurve: Curves.linear,
  );
}

Future afterAdsDialog(BuildContext context,
    {required String title,
      required Widget imageWidget,
      required String titleNote,
      required GestureTapCallback titleGuideTap}) {
  return showDialog(
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        top: 0.05.sh,
                      ),
                      margin: EdgeInsets.only(
                        top: 0.05.sh,
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: const <BoxShadow>[
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 0.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            height: 15.h,
                          ),
                          Center(
                              child: Text(title,
                                  style: TextStyle(
                                      fontSize: 20.w,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold)) //
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          imageWidget,
                          SizedBox(
                            height: 10.h,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5.w, right: 5.w),
                            child: Text(
                              titleNote,
                              style: TextStyle(
                                  color: const Color(0xff707070),
                                  fontSize: 15.w),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          InkWell(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onTap: () async {
                                await LaunchApp.openApp(
                                  androidPackageName:
                                  apiModel!.iptvPlayerUrl,
                                  iosUrlScheme: '',
                                  appStoreLink: '',
                                  // openStore: false
                                );
                                // Get.back();
                              },
                              child: blueGreen("IPTV Player"))
                              .marginSymmetric(horizontal: 5.r),
                          SizedBox(
                            height: 10.h,
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      right: 0,
                      left: 0,
                      child: Align(
                          alignment: Alignment.center,
                          child: Image.asset(
                            "assets/play_button_icon.png",
                            height: 80.h,
                          )),
                    ),
                    Positioned(
                        top: 0.03.sh,
                        right: -5,
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: titleGuideTap,
                          child: Container(
                            padding: EdgeInsets.all(5.r),
                            height: 0.1.sh,
                            width: 50.h,
                            child: Column(
                              children: [
                                SvgPicture.asset("assets/video_guide.svg"),
                                Text(
                                  "Guide",
                                  style: TextStyle(fontSize: 12.sp),
                                )
                              ],
                            ),
                          ),
                        ).paddingOnly(top: 15.r))
                  ],
                ),
              )),
        );
      },
      context: context);
}

LoaderDialog(BuildContext context) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return WillPopScope(
        onWillPop: () {
          return Future.value(false);
        },
        child: Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          insetPadding: EdgeInsets.symmetric(horizontal: 0.4.sw),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Center(
                child: const CircularProgressIndicator().marginAll(15.r),
              )
            ],
          ),
        ),
      );
    },
  );
}