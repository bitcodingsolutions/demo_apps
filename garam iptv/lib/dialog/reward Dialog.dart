import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'getx_controller_common.dart';

class RewardDialog {
  static show(BuildContext context,
      {required String rewardMessage, required GestureTapCallback onTap}) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return WillPopScope(
          onWillPop: () {
            Get.back();
            Controller.to.isOPenQrCodeDialog.value = false;
            Controller.to.isOPenQrCodeDialog.value = false;
            return Future.value(true);
          },
          child: SimpleDialog(
            contentPadding: EdgeInsets.zero,
            insetPadding: EdgeInsets.symmetric(horizontal: 0.03.sw),
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.r)),
                  color: Colors.white),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                          padding: EdgeInsets.only(right: 10.w, top: 10.h),
                          child: InkWell(
                              onTap: () {
                                Get.back();
                                Controller.to.isOPenQrCodeDialog.value = false;
                                Controller.to.isOPenQrCodeDialog.value = false;

                              },
                              child: const Icon(Icons.close)))),
                  Padding(
                    padding:
                        EdgeInsets.only(left: 20.w, right: 20.w, top: 20.h),
                    child: Image.asset("assets/reward.png",
                        gaplessPlayback: true, height: 120.h),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Text(
                    "Reward Ad",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 25.w),
                  ),
                  SizedBox(
                    height: 15.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10.w, right: 10.w),
                    child: Text(
                      rewardMessage,
                      style: TextStyle(
                          color: const Color(0xffA1A1A1), fontSize: 15.w),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  InkWell(
                    onTap: onTap,
                    child: Container(
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.20),
                                blurRadius: 8.0,
                                offset: const Offset(0, 10))
                          ],
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          color: const Color(0xff5291CC)),
                      padding: EdgeInsets.symmetric(
                          horizontal: 40.w, vertical: 10.h),
                      child: Text(
                        "Watch Ad",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.w),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
