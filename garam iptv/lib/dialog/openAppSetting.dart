// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

OpenSettingDialog(BuildContext context) {
  showDialog(
      builder: (context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                child: Container(
                  margin: EdgeInsets.only(
                    top: 15.h,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(16.0),
                      boxShadow: const <BoxShadow>[
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 0.0,
                          offset: Offset(0.0, 0.0),
                        ),
                      ]),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Center(
                        child: Text('Please allow permission in',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18.w,
                                color: Colors.black,
                                fontWeight: FontWeight.bold))
                            .marginOnly(top: 25.h),
                      ),
                      Center(
                        child: Text('app Setting',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18.w,
                                color: Colors.black,
                                fontWeight: FontWeight.bold))
                            .marginOnly(top: 5.h),
                      ),

                      GestureDetector(onTap: (){
                        Navigator.pop(context);
                        openAppSettings();
                      },child: Container(
                        margin: EdgeInsets.only(bottom: 10.r, right: 25.r, left: 25.r),
                        padding: EdgeInsets.all(15.r),
                        alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: const Color(0xff0777b5), borderRadius: BorderRadius.circular(10.r)),
                        child: Text("App Setting",style: TextStyle(fontSize: 18.sp,color: Colors.white,fontWeight: FontWeight.bold)),
                      )).marginOnly(bottom: 10.h, top: 20.h)


                    ],
                  ),
                ),
              )),
        );
      },
      context: context);
}