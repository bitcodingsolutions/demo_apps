// // To parse this JSON data, do
// //
// //     final moreLiveApp = moreLiveAppFromJson(jsonString);
//
//
// import 'dart:convert';
//
// MoreLiveApp? moreLiveApp;
//
// MoreLiveApp moreLiveAppFromJson(String str) => MoreLiveApp.fromJson(json.decode(str));
//
// String moreLiveAppToJson(MoreLiveApp data) => json.encode(data.toJson());
//
// class MoreLiveApp {
//   MoreLiveApp({
//    required this.record,
//   });
//
//   List<Record> record;
//
//   factory MoreLiveApp.fromJson(Map<String, dynamic> json) => MoreLiveApp(
//     record: List<Record>.from(json["record"].map((x) => Record.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "record": List<dynamic>.from(record.map((x) => x.toJson())),
//   };
// }
//
// class Record {
//   Record({
//    required this.links,
//    required this.image,
//    required this.title,
//    required this.subtitle,
//   });
//
//   String links;
//   String image;
//   String title;
//   String subtitle;
//
//   factory Record.fromJson(Map<String, dynamic> json) => Record(
//     links: json["Links"],
//     image: json["Image"],
//     title: json["Title"],
//     subtitle: json["Subtitle"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "Links": links,
//     "Image": image,
//     "Title": title,
//     "Subtitle": subtitle,
//   };
// }
