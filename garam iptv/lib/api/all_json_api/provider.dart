// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:google_applovin_unity_ads/ads/SettingsModel.dart';
import 'package:http/http.dart' as http;
import 'package:iptv/main.dart';

import '../../firebase_database.dart';
import 'model_class.dart';

AllCountryJson? channelApiList;

Future allCountryApiJson() async {
  final response =
      await http.get(Uri.parse(apiModel!.extraUrl!));

  if (response.statusCode == 200) {
    return channelApiList = AllCountryJson.fromJson(jsonDecode(response.body));

  } else {
    throw Exception('Failed to load album');
  }
}

String settingsUrl = "";
Settings? apiModel;


Future AdsSettingProvider() async {
  dynamic settingsData =
      await FirebaseDatabaseUtil.instance.readData(providerData1);
  // print("settingdata ===>>>  $settingsData");

  if (settingsData != null) {
    // settingsUrl = "https://gitlab.com/bitcodingsolutions/iptv_channels/-/raw/main/LiveX/Settings/Abear.json";
    settingsUrl = settingsData[providerData2];
    // print("json ===>>> $settingsUrl");
  } else {
    settingsUrl = "";
  }

  final response = await http.get(Uri.parse(settingsUrl));

  if (response.statusCode == 200) {

    apiModel = Settings.fromJson(jsonDecode(response.body));

  } else {
    throw Exception('Failed to load album');
  }


}
