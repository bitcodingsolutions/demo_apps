// To parse this JSON data, do
//
//     final allCountryJson = allCountryJsonFromJson(jsonString);

import 'dart:convert';

AllCountryJson allCountryJsonFromJson(String str) =>
    AllCountryJson.fromJson(json.decode(str));

String allCountryJsonToJson(AllCountryJson data) => json.encode(data.toJson());

class AllCountryJson {
  AllCountryJson({
    required this.records,
  });

  List<Record> records;

  factory AllCountryJson.fromJson(Map<String, dynamic> json) => AllCountryJson(
        records:
            List<Record>.from(json["records"].map((x) => Record.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "records": List<dynamic>.from(records.map((x) => x.toJson())),
      };
}

class Record {
  Record({
    required this.countryImageUrl,
    required this.countryName,
    required this.links,
    required this.subLinks,
  });

  String countryImageUrl;
  String countryName;
  String links;
  List<String> subLinks;

  factory Record.fromJson(Map<String, dynamic> json) => Record(
        countryImageUrl: json["country_imageUrl"],
        countryName: json["country_name"],
        links: json["Links"],
        subLinks: List<String>.from(json["SubLinks"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "country_imageUrl": countryImageUrl,
        "country_name": countryName,
        "Links": links,
        "SubLinks": List<dynamic>.from(subLinks.map((x) => x)),
      };
}
