Add the dependency in `pubspec.yaml`:
## PERSONAL PACKAGE
```yaml  
dependencies:  
 ... google_applovin_unity_ads: ^0.0.7  
```  


## ADD ANDROID MANIFEST

```xml  

//PERMISSION 
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="com.google.android.gms.permission.AD_ID"/>


//META DATA
 <meta-data 
	 android:name="applovin.sdk.key" 
	 android:value="ENTER APPLOVING SDK KEY" />  
	 
 <meta-data 
	 android:name="applovin.sdk.verbose_logging" 
	 android:value="true" />  
 <meta-data 
	 android:name="com.google.android.gms.ads.AD_MANAGER_APP" 
	 android:value="true" />  




FOR ANDROID SPLASH SCREEN
<meta-data  
  android:name="io.flutter.embedding.android.SplashScreenDrawable"  
  android:resource="@drawable/launch_background" />
```  

## BUILD GRADLE

```xml  
multiDexEnabled true
targetSdkVersion 33 
android:usesCleartextTraffic="true"
```  


## ADD MAIN.DART



```dart  
WidgetsFlutterBinding.ensureInitialized();  
  
await APICALLINGMETHOD();  
  
if ((apiModel? ?? "").toString().isNotEmpty) {  
GoogleApplovinUnityAds.initialize(  
jsonEncode(apiModel?).toString(),  
callback: () => {runApp(const IPTV())});  
}  
```
**ADD VERSION CODE FUNCTION**
```yaml  
dependencies:  
   package_info_plus:  
```  
```dart  

int versionCode = 1;

PackageInfo packageInfo = await PackageInfo.fromPlatform();
setState(() {  
  versionCode = int.parse(packageInfo.buildNumber);  
  print("11111111- ${versionCode}");  
});
```
## ADS_FILE.DART
```dart  
import 'package:flutter/material.dart';  
import 'package:google_applovin_unity_ads/google_applovin_unity_ads.dart';  
import 'package:google_applovin_unity_ads/native/controller.dart';  
import 'package:sizer/sizer.dart';  
import '../main.dart';  
  
showIntraAds({Function? callback = null}) async {  
  if (apiModel!.adSetting.appVersionCode == versionCode) {  
    if (callback != null) {  
      callback();  
  }  
  } else {  
    GoogleApplovinUnityAds.showIntraAds(callback: callback);  
  }  
}  
  
Future<void> showRewardAds({Function? callback}) async {  
  if (apiModel!.adSetting.appVersionCode == versionCode) {  
    if (callback != null) {  
      callback();  
  }  
  } else {  
    GoogleApplovinUnityAds.showRewardAds(callback: callback);  
  }  
}  
  
bannerAds() {  
  print(" palsana ${apiModel!.adSetting.appVersionCode}");  
  print(" palsana ${versionCode}");  
  
 if (apiModel!.adSetting.appVersionCode == versionCode ||  
      apiModel!.adsSequence.isEmpty) {  
    return Container(  
      height: 0,  
  );  
  }  
  
  return GoogleApplovinUnityAds.bannerAds();  
}  
  
fullNativeAds() {  
  if (apiModel!.adSetting.appVersionCode == versionCode ||  
      apiModel!.adsSequence.isEmpty ||  
      (apiModel!.adsSequence.length == 1 &&  
          apiModel!.adsSequence[0] == "custom_ads")) {  
    return Container(  
      height: 0,  
  );  
  }  
  return GoogleApplovinUnityAds.nativeAds(  
      NativeSize(Size(double.infinity, 50.h)), "F",  
  error: Container(  
        height: 0,  
  ));  
}  
  
mediumNativeAds() {  
  if (apiModel!.adSetting.appVersionCode == versionCode ||  
      apiModel!.adsSequence.isEmpty ||  
      (apiModel!.adsSequence.length == 1 &&  
          apiModel!.adsSequence[0] == "custom_ads")) {  
    return Container(  
      height: 0,  
  );  
  }  
  return GoogleApplovinUnityAds.nativeAds(  
      NativeSize(Size(double.infinity, 25.h)), "M");  
}  
  
showOpenAd() {  
  if (apiModel!.adSetting.appVersionCode == versionCode ||  
      apiModel!.adsSequence.isEmpty) {  
    return;  
  }  
  
  GoogleApplovinUnityAds.showOpenAds();  
}  
  
smallNativeAds() {  
  if (apiModel!.adSetting.appVersionCode == versionCode ||  
      apiModel!.adsSequence.isEmpty ||  
      (apiModel!.adsSequence.length == 1 &&  
          apiModel!.adsSequence[0] == "custom_ads")) {  
    return Container(  
      height: 0,  
  );  
  }  
  return GoogleApplovinUnityAds.nativeAds(  
      NativeSize(Size(double.infinity, 50.h)), "S");  
}  
  
class nativeAdsClass {}


```


## OPEN ADS
```dart
bool isAdShow = false;
with WidgetsBindingObserver

WidgetsBinding.instance.addObserver(this);


@override
void didChangeAppLifecycleState(AppLifecycleState state) {
  if (state == AppLifecycleState.resumed) {
    print("CHECK On Resumed Called");
    if(!isAdShow) {
      GoogleApplovinUnityAds.showOpenAds(callback: () => {
        isAdShow = true
      });
    }
  }

  if (state == AppLifecycleState.paused) {
    
    isAdShow = false;
  }
}



@override
void dispose() {
  WidgetsBinding.instance.removeObserver(this);
  super.dispose();
}
```