package com.livex.abear.tv.channels

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import com.btcpiyush.ads.google_applovin_unity_ads.CustomNativeAd
import com.btcpiyush.ads.google_applovin_unity_ads.GoogleApplovinUnityAdsPlugin

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        val factory: CustomNativeAd =
                CustomNativeAd(
                        getLayoutInflater()
                )
        GoogleApplovinUnityAdsPlugin.registerNativeAdFactory(flutterEngine, "nativeFactoryId", factory)
    }

    override fun cleanUpFlutterEngine(flutterEngine: FlutterEngine) {
        GoogleApplovinUnityAdsPlugin.unregisterNativeAdFactory(flutterEngine, "nativeFactoryId")
    }
}
