import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:live_score_ipl/app/constant/assets_config.dart';
import 'package:live_score_ipl/app/constant/common_widget.dart';
import 'package:live_score_ipl/app/modules/menu_screen.dart';
import '../constant/color_config.dart';
import '../constant/font_config.dart';
import '../constant/text_config.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: () {
        if (scaffoldKey.currentState!.isDrawerOpen) {
          scaffoldKey.currentState!.closeDrawer();
        } else {}
        return Future.value(false);
      },
      child: Stack(
        children: [
          gradientContainer(),
          Scaffold(
            key: scaffoldKey,
            backgroundColor: Colors.transparent,
            drawer: Drawer(
                width: 0.75.sw,
                backgroundColor: ColorConfig.blue,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(40.r))),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 0.22.sh,
                        color: Colors.green,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.r, vertical: 10.r),
                        child: Column(
                          children: [
                            drawerItem(
                                imagePath: AssetsConfig.privacyPolicy,
                                // color: ColorConfig.backGroundColor.withOpacity(0.5),
                                title: TextConfig.privacyPolicy,
                                onTap: () {}),
                            SizedBox(
                              height: 15.h,
                            ),
                            drawerItem(
                                imagePath: AssetsConfig.feedback,
                                // color: Colors.red,
                                title: TextConfig.feedback,
                                onTap: () {}),
                            SizedBox(
                              height: 15.h,
                            ),
                            drawerItem(
                                imagePath: AssetsConfig.aboutAs,
                                // color: Colors.orange,
                                title: TextConfig.aboutAs,
                                onTap: () {}),
                            SizedBox(
                              height: 15.h,
                            ),
                          ],
                        ),
                      )
                    ])),
            appBar: AppBar(
                elevation: 0,
                backgroundColor: Colors.transparent,
                centerTitle: true,
                leading: GestureDetector(
                  onTap: () {
                    scaffoldKey.currentState?.openDrawer();
                  },
                  child: Container(
                      padding: EdgeInsets.all(15.r),
                      child: Image.asset(
                        AssetsConfig.menuIcon,
                      )),
                ),
                title: Text(
                  TextConfig.cricket,
                  style: TextStyle(fontFamily: FontConfig.lato),
                )),
            body: Column(children: [
              GestureDetector(
                onTap: () {
                  Get.to(const MenuList(),transition: Transition.fadeIn);
                },
                child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 10.r),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(15.r)),
                  child: Image.asset(AssetsConfig.ipl2023, height: 0.2.sh),
                ),
              )
            ]).paddingAll(0.03.sw),
          ),
        ],
      ),
    );
  }
}
