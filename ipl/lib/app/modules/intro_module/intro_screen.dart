import 'package:flutter/material.dart';
import 'package:flutter_onboard/flutter_onboard.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:live_score_ipl/app/constant/common_widget.dart';
import 'package:live_score_ipl/app/modules/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constant/assets_config.dart';
import '../../constant/color_config.dart';
import '../../constant/font_config.dart';
import '../../constant/text_config.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({super.key});

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            gradientContainer(),
            OnBoard(
              pageController: _pageController,
              onSkip: () async {

                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setBool("intro", true);
                Get.off(const HomeScreen(), transition: Transition.fadeIn);
              },
              onDone: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setBool("intro", true);
                Get.off(const HomeScreen(), transition: Transition.fadeIn);
              },
              onBoardData: onBoardData,
              titleStyles: TextStyle(
                color: ColorConfig.white,
                fontFamily: FontConfig.lato,
                fontSize: 18.sp,
                fontWeight: FontWeight.w900,
                letterSpacing: 0.15,
              ),
              descriptionStyles: TextStyle(
                  fontSize: 16.sp,
                  color: Colors.white.withOpacity(0.8),
                  fontFamily: FontConfig.lato),
              pageIndicatorStyle: PageIndicatorStyle(
                width: 100,
                inactiveColor: ColorConfig.white.withOpacity(0.54),
                activeColor: ColorConfig.white,
                inactiveSize: const Size(8, 8),
                activeSize: const Size(12, 12),
              ),
              // Either Provide onSkip Callback or skipButton Widget to handle skip state
              skipButton: GestureDetector(
                onTap: () async {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setBool("intro", true);
                  Get.off(const HomeScreen(), transition: Transition.fadeIn);
                },
                child: Text(
                  "Skip",
                  style: TextStyle(color: ColorConfig.white),
                ),
              ).marginOnly(right: 0.05.sw),
              // Either Provide onDone Callback or nextButton Widget to handle done state
              nextButton: OnBoardConsumer(
                builder: (context, ref, child) {
                  final state = ref.watch(onBoardStateProvider);
                  return InkWell(
                    onTap: () => _onNextTap(state),
                    child: Container(
                      width: 230,
                      height: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              offset: const Offset(0, 2),
                              color: const Color(0xff000000).withOpacity(0.25),
                              blurRadius: 2.0,
                              spreadRadius: 2),
                        ],
                        borderRadius: BorderRadius.circular(30),
                        gradient: LinearGradient(
                          colors: [
                            ColorConfig.white.withOpacity(0.5),
                            ColorConfig.white,
                            ColorConfig.white,
                            ColorConfig.white.withOpacity(0.5)
                          ],
                        ),
                      ),
                      child: Text(
                        state.isLastPage ? TextConfig.done : TextConfig.next,
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onNextTap(OnBoardState onBoardState) async {
    if (!onBoardState.isLastPage) {
      _pageController.animateToPage(
        onBoardState.page + 1,
        duration: const Duration(milliseconds: 250),
        curve: Curves.easeInOutSine,
      );
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool("intro", true);
      Get.off(const HomeScreen(), transition: Transition.fadeIn);
    }
  }
}

final List<OnBoardModel> onBoardData = [
  OnBoardModel(
    title: TextConfig.splashScreen1Note1,
    description: TextConfig.splashScreen1Note2,
    imgUrl: AssetsConfig.splashScreenIcon1,
  ),
  OnBoardModel(
    title: TextConfig.splashScreen2Note1,
    description: TextConfig.splashScreen2Note2,
    imgUrl: AssetsConfig.splashScreenIcon2,
  ),
  OnBoardModel(
    title: TextConfig.splashScreen3Note1,
    description: TextConfig.splashScreen3Note2,
    imgUrl: AssetsConfig.splashScreenIcon3,
  ),
];
