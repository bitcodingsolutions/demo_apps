import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:live_score_ipl/app/modules/home_screen.dart';
import 'package:live_score_ipl/app/modules/intro_module/intro_screen.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constant/assets_config.dart';
import '../controller/controller.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 3),
      () {
        restoreSharedPreferenceValue();
      },
    );
    super.initState();
  }

  restoreSharedPreferenceValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Controller.to.introScreenStatus.value = prefs.getBool('intro') ?? false;
    if(Controller.to.introScreenStatus.value == false){
      Get.off(const IntroScreen(), transition: Transition.fadeIn);
    }else{
      Get.off(const HomeScreen(), transition: Transition.fadeIn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        body: Stack(
          children: [
            SizedBox(
              height: 1000.h,
              width: 1000.w,
              child: Image.asset(AssetsConfig.splashScreen,
                  alignment: Alignment.topCenter, fit: BoxFit.cover),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(bottom: 8.h),
                child: Lottie.asset(
                  AssetsConfig.loaderAnimation,
                  height: 35.sp,
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
