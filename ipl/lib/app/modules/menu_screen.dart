import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:live_score_ipl/app/constant/assets_config.dart';
import 'package:live_score_ipl/app/constant/common_widget.dart';
import 'package:live_score_ipl/app/modules/menu_screen_list/auction.dart';
import 'package:live_score_ipl/app/modules/menu_screen_list/venue.dart';
import '../constant/color_config.dart';
import '../constant/font_config.dart';
import '../constant/text_config.dart';
import 'menu_screen_list/highlight.dart';
import 'menu_screen_list/point_table.dart';
import 'menu_screen_list/recode.dart';
import 'menu_screen_list/schedule.dart';
import 'menu_screen_list/team_squad.dart';
import 'menu_screen_list/winner.dart';

class MenuList extends StatefulWidget {
  const MenuList({Key? key}) : super(key: key);

  @override
  State<MenuList> createState() => _MenuListState();
}

class _MenuListState extends State<MenuList> {
  List title = [
    TextConfig.auction,
    TextConfig.schedule,
    TextConfig.teamSquad,
    TextConfig.highlight,
    TextConfig.pointTable,
    TextConfig.winner,
    TextConfig.recode,
    TextConfig.venue
  ];
  List titleImage = [
    AssetsConfig.item1,
    AssetsConfig.item2,
    AssetsConfig.item3,
    AssetsConfig.item4,
    AssetsConfig.item5,
    AssetsConfig.item6,
    AssetsConfig.item7,
    AssetsConfig.item8,
  ];
  List titleColor = [
    ColorConfig.color1,
    ColorConfig.color2,
    ColorConfig.color3,
    ColorConfig.color4,
    ColorConfig.color5,
    ColorConfig.color6,
    ColorConfig.color7,
    ColorConfig.color8,
  ];

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: const Icon(Icons.arrow_back_ios_new)),
            centerTitle: true,
            title: Text(TextConfig.ipl2023),
          ),
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 0.05.sw),
              child: GridView.builder(
                itemCount: title.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2, crossAxisSpacing: 25.r),
                itemBuilder: (context, index) {
                  List pageList = [
                    AuctionScreen(appbarTitle: title[index]),
                    ScheduleScreen(appbarTitle: title[index]),
                    TeamSquadScreen(appbarTitle: title[index]),
                    HighlightScreen(appbarTitle: title[index]),
                    PointTableScreen(appbarTitle: title[index]),
                    WinnerScreen(appbarTitle: title[index]),
                    RecodeScreen(appbarTitle: title[index]),
                    VanueScreen(appbarTitle: title[index]),
                  ];
                  return GestureDetector(
                    onTap: () {
                      Get.to(pageList[index], transition: Transition.fadeIn);
                    },
                    child: Stack(alignment: Alignment.bottomCenter, children:  [
                      Container(
                        height: 80.h,
                        padding: EdgeInsets.only(bottom: 0.03.sh),
                        alignment: Alignment.bottomCenter,
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.15),
                            borderRadius: BorderRadius.circular(10.r)),
                        child: Text(
                          title[index],
                          style: TextStyle(
                              color: ColorConfig.white,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w400,
                              fontFamily: FontConfig.jost),
                        ),
                      ),
                      Positioned(
                        top: 0.04.sh,
                        right: 0,
                        left: 0,
                        child: Container(
                          height: 55.h,
                          width: 55.h,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: titleColor[index]),
                          child: Image.asset(titleImage[index], height: 20.h),
                        ),
                      )
                    ]),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
