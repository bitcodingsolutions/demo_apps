// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_score_ipl/app/constant/common_widget.dart';
import 'package:live_score_ipl/app/dialog/loader_dialog.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AuctionScreen extends StatefulWidget {
  String appbarTitle;

  AuctionScreen({required this.appbarTitle});

  @override
  State<AuctionScreen> createState() => _AuctionScreenState();
}

class _AuctionScreenState extends State<AuctionScreen> {
  // Web View
  WebViewController controller = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..setBackgroundColor(const Color(0x00000000))
    ..setNavigationDelegate(
      NavigationDelegate(
        onProgress: (int progress) {},
        onPageStarted: (String url) {},
        onPageFinished: (String url) {},
        onWebResourceError: (WebResourceError error) {},
        onNavigationRequest: (NavigationRequest request) {
          if (request.url.startsWith('https://www.iplt20.com/video/all')) {
            return NavigationDecision.prevent;
          }
          return NavigationDecision.navigate;
        },
      ),
    )
    ..loadRequest(Uri.parse('https://www.iplt20.com/video/all'));

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leading: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Icon(Icons.arrow_back_ios_new)),
            title: Text(widget.appbarTitle),
          ),
          body: WebViewWidget(controller: controller),
        ),
      ],
    );
  }
}
