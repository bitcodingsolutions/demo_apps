import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constant/common_widget.dart';

class WinnerScreen extends StatefulWidget {
  String appbarTitle;
  WinnerScreen({required this.appbarTitle});

  @override
  State<WinnerScreen> createState() => _WinnerScreenState();
}

class _WinnerScreenState extends State<WinnerScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leading: GestureDetector(onTap: () {
              Get.back();
            },child: const Icon(Icons.arrow_back_ios_new)),
            title: Text(widget.appbarTitle),
          ),
        ),
      ],
    );
  }
}
