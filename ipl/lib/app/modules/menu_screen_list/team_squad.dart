// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../constant/common_widget.dart';

class TeamSquadScreen extends StatefulWidget {
  String appbarTitle;
  TeamSquadScreen({required this.appbarTitle});

  @override
  State<TeamSquadScreen> createState() => _TeamSquadScreenState();
}

class _TeamSquadScreenState extends State<TeamSquadScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leading: GestureDetector(onTap: () {
              Get.back();
            },child: const Icon(Icons.arrow_back_ios_new)),
            title: Text(widget.appbarTitle),
          ),
        ),
      ],
    );
  }
}
