// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constant/common_widget.dart';

class RecodeScreen extends StatefulWidget {
  String appbarTitle;
  RecodeScreen({required this.appbarTitle});
  @override
  State<RecodeScreen> createState() => _RecodeScreenState();
}

class _RecodeScreenState extends State<RecodeScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leading: GestureDetector(onTap: () {
              Get.back();
            },child: const Icon(Icons.arrow_back_ios_new)),
            title: Text(widget.appbarTitle),
          ),
        ),
      ],
    );
  }
}
