import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constant/common_widget.dart';

class ScheduleScreen extends StatefulWidget {
  String appbarTitle;

  ScheduleScreen({required this.appbarTitle});

  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        gradientContainer(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            centerTitle: true,
            elevation: 0,
            leading: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Icon(Icons.arrow_back_ios_new)),
            title: Text(widget.appbarTitle),
          ),
        ),
      ],
    );
  }
}
