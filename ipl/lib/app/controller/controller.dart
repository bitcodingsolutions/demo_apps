import 'package:get/get.dart';
import '../utilities/firebase_database_util.dart';

class Controller extends GetxController{
  static Controller to = Controller();

  RxBool introScreenStatus = false.obs;
  RxString setting_url = "".obs;

  Future loadData() async {
    try {
      await FirebaseDatabaseUtil.instance.initState();
      dynamic settingsData =
      await FirebaseDatabaseUtil.instance.readData('');
      if (settingsData != null) {
        setting_url.value = settingsData[''];
        print("Images Setting Url=====>> ${setting_url.value}");
      } else {
        setting_url.value = "";
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future loadAPiData(){

  }
}