import 'dart:ui';

class ColorConfig{
  static Color backGroundColor = const Color(0xff111833);
  static Color drawerBackground = const Color(0xff1b2964);
  static Color blue = const Color(0xff5b70cc);
  static Color white = const Color(0xffffffff);


  // todo menu list screen
  static Color color1 = const Color(0xff4760F0);
  static Color color2 = const Color(0xffF32525);
  static Color color3 = const Color(0xff00B76B);
  static Color color4 = const Color(0xffCD2DFF);
  static Color color5 = const Color(0xffC8094E);
  static Color color6 = const Color(0xff09C8BC);
  static Color color7 = const Color(0xff7806D2);
  static Color color8 = const Color(0xffF1C40F);
}