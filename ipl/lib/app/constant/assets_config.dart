class AssetsConfig{
  static String appLogo = "";
  static String splashScreen = "assets/splash.png";
  static String menuIcon = "assets/menu.png";
  static String loaderAnimation = "assets/Animation/loader.json";


  static String ipl2023 = "assets/ipl2023.png";

  // todo splash screen icon image
  static String splashScreenIcon1 = 'assets/splash/s1.png';
  static String splashScreenIcon2 = 'assets/splash/s2.png';
  static String splashScreenIcon3 = 'assets/splash/s3.png';

  // todo manu list screen
 static String item1 = 'assets/menu_list/auction.png';
 static String item2 = 'assets/menu_list/schedule.png';
 static String item3 = 'assets/menu_list/team_squad.png';
 static String item4 = 'assets/menu_list/highlight.png';
 static String item5 = 'assets/menu_list/point_table.png';
 static String item6 = 'assets/menu_list/recode.png';
 static String item7 = 'assets/menu_list/winner.png';
 static String item8 = 'assets/menu_list/venue.png';











  // todo drawer elements
  static String privacyPolicy = "assets/privacy_policy.png";
  static String aboutAs = "assets/about_as.png";
  static String feedback = "assets/feedback.png";
}
