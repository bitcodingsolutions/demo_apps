import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'color_config.dart';
import 'font_config.dart';

// todo for background Gradient Color
Widget gradientContainer() {
  return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
              colors: [
        ColorConfig.backGroundColor,
        ColorConfig.drawerBackground
      ])));
}

// todo for Drawer Item
Widget drawerItem(
    {required String imagePath,
    // required Color color,
    required String title,
    required GestureTapCallback onTap}) {
  return GestureDetector(
    onTap: onTap,
    child: Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 20.w),
          height: 40.h,
          width: 40.h,
          padding: EdgeInsets.all(10.r),
          decoration: BoxDecoration(
              color: ColorConfig.backGroundColor.withOpacity(0.3), borderRadius: BorderRadius.circular(5.r)),
          child: Image.asset(imagePath, color: Colors.white),
        ),
        SizedBox(
          height: 40.h,
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: FontConfig.jost,
                  fontSize: 18.sp),
            ),
          ),
        )
      ],
    ),
  );
}


