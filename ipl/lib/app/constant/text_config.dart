// ignore_for_file: non_constant_identifier_names

class TextConfig {
  static String appTitle = "IPL Live Score";
  static String cricket = "Cricket";
  static String privacyPolicy = "Privacy Policy";
  static String feedback = "Feedback";
  static String aboutAs = "About As";
  static String next = "Next";
  static String done = "Done";
  static String ipl2023 = "IPL 2023";
  static String splashScreen1Note1 = 'LIVE SCORE\nCRICKET';
  static String splashScreen1Note2 =
      "Provide you the fastest Live cricket Score\nupdated of every cricket matches";
  static String splashScreen2Note1 = 'VANUE\nDETAILS';
  static String splashScreen2Note2 = "Upcoming Series and match details";
  static String splashScreen3Note1 = 'ACCURATE MATCH\nODDS AND SERIES';
  static String splashScreen3Note2 = "Give polls and see other's option";

  // todo menu screen
  static String auction = 'Auction';
  static String schedule = 'Schedule';
  static String teamSquad = 'Team & Squad';
  static String highlight = 'HighLight';
  static String pointTable = 'Point Table';
  static String winner = 'Winner';
  static String recode = 'Recode';
  static String venue = 'Venue';
}
