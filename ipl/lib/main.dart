import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:live_score_ipl/app/modules/splash_screen.dart';
import 'app/modules/intro_module/intro_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(ScreenUtilInit(builder: (context, child) => NotificationListener<OverscrollIndicatorNotification>(onNotification: (overscroll) {
    overscroll.disallowIndicator();
    return true;
  },child: const GetMaterialApp(
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
  )),));
}
